#define _GLIBCXX_HAVE_SYS_SDT_H 1
#define _BITS_FLOATN_COMMON_H 
#define __SSP_STRONG__ 3
#define __DBL_MIN_EXP__ (-1021)
#define __FLT32X_MAX_EXP__ 1024
#define ____sigset_t_defined 
#define __cpp_attributes 200809
#define _GLIBCXX98_USE_C99_MATH 1
#define __CFLOAT32 _Complex float
#define __UINT_LEAST16_MAX__ 0xffff
#define __ATOMIC_ACQUIRE 2
#define __FLT128_MAX_10_EXP__ 4932
#define __FLT_MIN__ 1.17549435082228750796873653722224568e-38F
#define __GCC_IEC_559_COMPLEX 2
#define _GLIBCXX_USE_FCHMOD 1
#define __cpp_aggregate_nsdmi 201304
#define __bswap_16(x) (__extension__ ({ unsigned short int __v, __x = (unsigned short int) (x); if (__builtin_constant_p (__x)) __v = __bswap_constant_16 (__x); else __asm__ ("rorw $8, %w0" : "=r" (__v) : "0" (__x) : "cc"); __v; }))
#define __UINT_LEAST8_TYPE__ unsigned char
#define __SIZEOF_FLOAT80__ 16
#define _T_WCHAR_ 
#define __CFLOAT64 _Complex double
#define __flexarr []
#define __S64_TYPE long int
#define __stub_fchflags 
#define __SQUAD_TYPE long int
#define __INTMAX_C(c) c ## L
#define _BSD_SIZE_T_DEFINED_ 
#define _GLIBCXX_TXN_SAFE_DYN 
#define __TIME_T_TYPE __SYSCALL_SLONG_TYPE
#define __CHAR_BIT__ 8
#define __FSWORD_T_TYPE __SYSCALL_SLONG_TYPE
#define makedev(maj,min) __SYSMACROS_DM (makedev) gnu_dev_makedev (maj, min)
#define _GLIBCXX_HAVE_ETIME 1
#define _GLIBCXX_USE_RANDOM_TR1 1
#define __UINT8_MAX__ 0xff
#define __WINT_MAX__ 0xffffffffU
#define __sigset_t_defined 1
#define __SIZEOF_PTHREAD_ATTR_T 56
#define _GLIBCXX_HAVE_WRITEV 1
#define __FLT32_MIN_EXP__ (-125)
#define _GLIBCXX_END_NAMESPACE_LDBL 
#define __GLIBC_PREREQ(maj,min) ((__GLIBC__ << 16) + __GLIBC_MINOR__ >= ((maj) << 16) + (min))
#define _XOPEN_SOURCE 700
#define __cpp_static_assert 200410
#define __WEXITSTATUS(status) (((status) & 0xff00) >> 8)
#define _GLIBCXX_USE_WEAK_REF __GXX_WEAK__
#define __OFF_T_MATCHES_OFF64_T 1
#define __ORDER_LITTLE_ENDIAN__ 1234
#define __SIZE_MAX__ 0xffffffffffffffffUL
#define __stub_putmsg 
#define __WCHAR_MAX__ 0x7fffffff
#define __BLKCNT_T_TYPE __SYSCALL_SLONG_TYPE
#define _GLIBCXX_USE_LONG_LONG 1
#define WIFEXITED(status) __WIFEXITED (status)
#define __GCC_HAVE_SYNC_COMPARE_AND_SWAP_1 1
#define __GCC_HAVE_SYNC_COMPARE_AND_SWAP_2 1
#define __GCC_HAVE_SYNC_COMPARE_AND_SWAP_4 1
#define __DBL_DENORM_MIN__ double(4.94065645841246544176568792868221372e-324L)
#define __GCC_HAVE_SYNC_COMPARE_AND_SWAP_8 1
#define __GCC_ATOMIC_CHAR_LOCK_FREE 2
#define __GCC_IEC_559 2
#define __FLT32X_DECIMAL_DIG__ 17
#define __FLT_EVAL_METHOD__ 0
#define _GLIBCXX_HAVE_FMODF 1
#define _GLIBCXX_PSEUDO_VISIBILITY(V) 
#define _GLIBCXX_HAVE_FMODL 1
#define __ASMNAME2(prefix,cname) __STRING (prefix) cname
#define __unix__ 1
#define _GLIBCXX_HAVE_SYS_STAT_H 1
#define __cpp_binary_literals 201304
#define _GLIBCXX_HAVE_ENOTSUP 1
#define __FLT64_DECIMAL_DIG__ 17
#define _GLIBCXX_SYNCHRONIZATION_HAPPENS_AFTER(A) 
#define _GLIBCXX_CXX_CONFIG_H 1
#define __stub_setlogin 
#define _GLIBCXX_PACKAGE_NAME "package-unused"
#define __GCC_ATOMIC_CHAR32_T_LOCK_FREE 2
#define __SYSCALL_WORDSIZE 64
#define __FLOAT_WORD_ORDER __BYTE_ORDER
#define __x86_64 1
#define __cpp_variadic_templates 200704
#define __attribute_nonstring__ 
#define _GLIBCXX_USE_C99_COMPLEX_TR1 1
#define __CFLOAT128 __cfloat128
#define __UINT_FAST64_MAX__ 0xffffffffffffffffUL
#define __SIG_ATOMIC_TYPE__ int
#define __COMPAR_FN_T 
#define __GID_T_TYPE __U32_TYPE
#define __DBL_MIN_10_EXP__ (-307)
#define __FINITE_MATH_ONLY__ 0
#define __id_t_defined 
#define __cpp_variable_templates 201304
#define __attribute_alloc_size__(params) __attribute__ ((__alloc_size__ params))
#define __U64_TYPE unsigned long int
#define _GLIBCXX_USE_WCHAR_T 1
#define __u_char_defined 
#define __pid_t_defined 
#define __GNUC_PATCHLEVEL__ 0
#define _GLIBCXX_STD_A std
#define __FLT32_HAS_DENORM__ 1
#define _GLIBCXX_BEGIN_NAMESPACE_VERSION 
#define __UINT_FAST8_MAX__ 0xff
#define __LEAF , __leaf__
#define __LDBL_REDIR1(name,proto,alias) name proto
#define _BITS_TYPES_LOCALE_T_H 1
#define __has_include(STR) __has_include__(STR)
#define __size_t 
#define _GLIBCXX_HAVE_FREXPF 1
#define _GLIBCXX_HAVE_FREXPL 1
#define __DEC64_MAX_EXP__ 385
#define _WCHAR_T_DEFINED 
#define _GLIBCXX_PURE __attribute__ ((__pure__))
#define _GLIBCXX_HAVE_STDINT_H 1
#define __SIZEOF_PTHREAD_CONDATTR_T 4
#define __INT8_C(c) c
#define _BITS_SYSMACROS_H 1
#define _GLIBCXX_HAVE_COSHF 1
#define _GLIBCXX_HAVE_COSHL 1
#define __INT_LEAST8_WIDTH__ 8
#define __UINT_LEAST64_MAX__ 0xffffffffffffffffUL
#define htobe16(x) __bswap_16 (x)
#define _GLIBCXX_HAVE_ENDIAN_H 1
#define __always_inline __inline __attribute__ ((__always_inline__))
#define NFDBITS __NFDBITS
#define __SHRT_MAX__ 0x7fff
#define __LDBL_MAX__ 1.18973149535723176502126385303097021e+4932L
#define _GLIBCXX_USE_C99_COMPLEX _GLIBCXX11_USE_C99_COMPLEX
#define __FLT64X_MAX_10_EXP__ 4932
#define _GLIBCXX_CPU_DEFINES 1
#define _GLIBCXX_USE_C99_STDIO _GLIBCXX11_USE_C99_STDIO
#define __daddr_t_defined 
#define _GLIBCXX_HAVE_STRUCT_DIRENT_D_TYPE 1
#define __fortify_function __extern_always_inline __attribute_artificial__
#define __UINT_LEAST8_MAX__ 0xff
#define __GCC_ATOMIC_BOOL_LOCK_FREE 2
#define _GLIBCXX_HAVE_EOWNERDEAD 1
#define __FLT128_DENORM_MIN__ 6.47517511943802511092443895822764655e-4966F128
#define __ptr_t void *
#define _GLIBCXX_HAVE_MODF 1
#define _GLIBCXX_HAVE_ATANF 1
#define __USE_ISOCXX11 1
#define _GLIBCXX_HAVE_ATANL 1
#define __UINTMAX_TYPE__ long unsigned int
#define __HAVE_DISTINCT_FLOAT32X 0
#define _GLIBCXX_OS_DEFINES 1
#define __linux 1
#define __DEC32_EPSILON__ 1E-6DF
#define __FLT_EVAL_METHOD_TS_18661_3__ 0
#define _BITS_TYPES_H 1
#define _GLIBCXX_HAVE_SYS_TIME_H 1
#define _GLIBCXX_HAVE_LIBINTL_H 1
#define __unix 1
#define _BITS_PTHREADTYPES_ARCH_H 1
#define _GLIBCXX_USE_FCHMODAT 1
#define __UINT32_MAX__ 0xffffffffU
#define __GXX_EXPERIMENTAL_CXX0X__ 1
#define __UID_T_TYPE __U32_TYPE
#define _GLIBCXX98_USE_C99_COMPLEX 1
#define _GLIBCXX_SYMVER 1
#define __SIZE_T 
#define _GLIBCXX_FULLY_DYNAMIC_STRING 0
#define FD_SETSIZE __FD_SETSIZE
#define __LDBL_MAX_EXP__ 16384
#define _GLIBCXX_HAVE_STRTOLD 1
#define _ATFILE_SOURCE 1
#define __glibcxx_assert(_Condition) 
#define __FLT128_MIN_EXP__ (-16381)
#define __WINT_MIN__ 0U
#define WUNTRACED 2
#define __SIZEOF_PTHREAD_RWLOCKATTR_T 8
#define __linux__ 1
#define _GLIBCXX_HAVE_MEMALIGN 1
#define __FLT128_MIN_10_EXP__ (-4931)
#define EXIT_FAILURE 1
#define __INT_LEAST16_WIDTH__ 16
#define _SIZE_T_DEFINED_ 
#define __LDBL_REDIR_NTH(name,proto) name proto __THROW
#define __SCHAR_MAX__ 0x7f
#define __FLT128_MANT_DIG__ 113
#define __WCHAR_MIN__ (-__WCHAR_MAX__ - 1)
#define __KERNEL_STRICT_NAMES 
#define __INT64_C(c) c ## L
#define __NTH(fct) __LEAF_ATTR fct throw ()
#define _GLIBCXX_CONST __attribute__ ((__const__))
#define __DBL_DIG__ 15
#define __GCC_ATOMIC_POINTER_LOCK_FREE 2
#define WSTOPPED 2
#define __FLT64X_MANT_DIG__ 64
#define _GLIBCXX_HAVE_AT_QUICK_EXIT 1
#define __GLIBC_USE_IEC_60559_TYPES_EXT 1
#define _GLIBCXX_HAVE_LIMIT_FSIZE 1
#define _GLIBCXX_HAVE_ATAN2F 1
#define _GLIBCXX_HAVE_ATAN2L 1
#define _GLIBCXX_USE_SENDFILE 1
#define _POSIX_SOURCE 1
#define __SIZEOF_INT__ 4
#define __SIZEOF_POINTER__ 8
#define _DEFAULT_SOURCE 1
#define __attribute_used__ __attribute__ ((__used__))
#define __LOCK_ALIGNMENT 
#define __GCC_ATOMIC_CHAR16_T_LOCK_FREE 2
#define _GLIBCXX_HAVE_EBADMSG 1
#define __USER_LABEL_PREFIX__ 
#define _GLIBCXX_NAMESPACE_CXX11 __cxx11::
#define __glibc_macro_warning(message) __glibc_macro_warning1 (GCC warning message)
#define _GLIBCXX_HAVE_TANL 1
#define _GLIBCXX_USE_PTHREAD_RWLOCK_T 1
#define _GLIBCXX_USE_C99_FENV_TR1 1
#define __GLIBC__ 2
#define _GLIBCXX_USE_C11_UCHAR_CXX11 1
#define __END_DECLS }
#define __FLT64X_EPSILON__ 1.08420217248550443400745280086994171e-19F64x
#define __CONCAT(x,y) x ## y
#define WCONTINUED 8
#define __STDC_HOSTED__ 1
#define _ALLOCA_H 1
#define __LDBL_HAS_INFINITY__ 1
#define __SLONG32_TYPE int
#define _BITS_UINTN_IDENTITY_H 1
#define _SYS_SELECT_H 1
#define __SYSMACROS_DM1(...) __glibc_macro_warning (#__VA_ARGS__)
#define _GLIBCXX_HAVE_LIMIT_VMEM 0
#define __GNU_LIBRARY__ 6
#define __FLT32_DIG__ 6
#define __RLIM_T_MATCHES_RLIM64_T 1
#define __FLT_EPSILON__ 1.19209289550781250000000000000000000e-7F
#define __GXX_WEAK__ 1
#define _GLIBCXX_HAVE_ISNANF 1
#define __SHRT_WIDTH__ 16
#define _GLIBCXX_HAVE_ISNANL 1
#define __f64x(x) x ##l
#define __SSIZE_T_TYPE __SWORD_TYPE
#define __DEV_T_TYPE __UQUAD_TYPE
#define _GLIBCXX_BEGIN_NAMESPACE_ALGO _GLIBCXX_BEGIN_NAMESPACE_VERSION
#define __LDBL_MIN__ 3.36210314311209350626267781732175260e-4932L
#define __nonnull(params) __attribute__ ((__nonnull__ params))
#define le32toh(x) __uint32_identity (x)
#define __DEC32_MAX__ 9.999999E96DF
#define __GLIBC_USE_IEC_60559_BFP_EXT 1
#define __WIFSIGNALED(status) (((signed char) (((status) & 0x7f) + 1) >> 1) > 0)
#define _GLIBCXX_HAVE_POLL 1
#define __cpp_threadsafe_static_init 200806
#define _GLIBCXX_USE_CLOCK_REALTIME 1
#define __WCOREDUMP(status) ((status) & __WCOREFLAG)
#define _WCHAR_T_ 
#define __FLT64X_DENORM_MIN__ 3.64519953188247460252840593361941982e-4951F64x
#define _GLIBCXX_STDLIB_H 1
#define _GLIBCXX_USE_C99_STDLIB _GLIBCXX11_USE_C99_STDLIB
#define __FLT32X_HAS_INFINITY__ 1
#define __INT32_MAX__ 0x7fffffff
#define __GLIBC_USE_DEPRECATED_GETS 0
#define __SIZEOF_PTHREAD_COND_T 48
#define __INT_WIDTH__ 32
#define __SIZEOF_LONG__ 8
#define __STDC_IEC_559__ 1
#define __STDC_ISO_10646__ 201706L
#define __UINT16_C(c) c
#define __PTRDIFF_WIDTH__ 64
#define __SIZEOF_PTHREAD_BARRIER_T 32
#define _GLIBCXX_HAVE_MODFF 1
#define _GLIBCXX_HAVE_MODFL 1
#define __DECIMAL_DIG__ 21
#define __NTHNL(fct) fct throw ()
#define __USE_FORTIFY_LEVEL 0
#define __PTHREAD_MUTEX_NUSERS_AFTER_KIND 0
#define __FLT64_EPSILON__ 2.22044604925031308084726333618164062e-16F64
#define _GLIBCXX_HAVE_HYPOTF 1
#define _GLIBCXX_HAVE_HYPOTL 1
#define _GLIBCXX_HAVE_SYS_UIO_H 1
#define __gnu_linux__ 1
#define __INTMAX_WIDTH__ 64
#define _LARGEFILE_SOURCE 1
#define _ENDIAN_H 1
#define __FLT64_MIN_EXP__ (-1021)
#define __attribute_warn_unused_result__ __attribute__ ((__warn_unused_result__))
#define __has_include_next(STR) __has_include_next__(STR)
#define _GLIBCXX_USE_LFS 1
#define __HAVE_FLOAT64X_LONG_DOUBLE 1
#define _GLIBCXX_HAVE_SYMVER_SYMBOL_RENAMING_RUNTIME_SUPPORT 1
#define __UQUAD_TYPE unsigned long int
#define __FLT64X_MIN_10_EXP__ (-4931)
#define _GLIBCXX_HAVE_LOCALE_H 1
#define __LDBL_HAS_QUIET_NAN__ 1
#define __USE_ISOC11 1
#define __HAVE_FLOAT64X 1
#define _GLIBCXX_HAVE_FABSF 1
#define _GLIBCXX_HAVE_FABSL 1
#define __FLT64_MANT_DIG__ 53
#define __attribute_const__ __attribute__ ((__const__))
#define __THROW throw ()
#define _GLIBCXX_HAVE_POWF 1
#define _GLIBCXX_HAVE_POWL 1
#define ___int_wchar_t_h 
#define WIFCONTINUED(status) __WIFCONTINUED (status)
#define __GNUC__ 7
#define __SYSCALL_ULONG_TYPE __ULONGWORD_TYPE
#define __GXX_RTTI 1
#define __pie__ 2
#define __MMX__ 1
#define __cpp_delegating_constructors 200604
#define __timespec_defined 1
#define _GLIBCXX_USE_GET_NPROCS 1
#define __OFF64_T_TYPE __SQUAD_TYPE
#define _GLIBCXX98_USE_C99_STDLIB 1
#define _GLIBCXX_HAVE_STRERROR_R 1
#define FD_SET(fd,fdsetp) __FD_SET (fd, fdsetp)
#define __FLT_HAS_DENORM__ 1
#define __SIZEOF_LONG_DOUBLE__ 16
#define _GLIBCXX_HAVE_EWOULDBLOCK 1
#define __timeval_defined 1
#define _GLIBCXX_HAVE_SYS_SYSINFO_H 1
#define _GLIBCXX_HAVE_POSIX_MEMALIGN 1
#define _GLIBCXX_RES_LIMITS 1
#define __PTHREAD_SPINS_DATA short __spins; short __elision
#define __LDBL_REDIR1_NTH(name,proto,alias) name proto __THROW
#define __BIGGEST_ALIGNMENT__ 16
#define __STDC_UTF_16__ 1
#define __ONCE_ALIGNMENT 
#define __FLT64_MAX_10_EXP__ 308
#define __GLIBC_USE_LIB_EXT2 1
#define __USE_ISOC95 1
#define __USE_ISOC99 1
#define __ASMNAME(cname) __ASMNAME2 (__USER_LABEL_PREFIX__, cname)
#define _GLIBCXX_HAVE_WCHAR_H 1
#define WTERMSIG(status) __WTERMSIG (status)
#define _GLIBCXX_HAVE_S_ISREG 1
#define __FLT32_HAS_INFINITY__ 1
#define EXIT_SUCCESS 0
#define __DBL_MAX__ double(1.79769313486231570814527423731704357e+308L)
#define _GLIBCXX_HAVE_GETS 1
#define _GLIBCXX_STDIO_SEEK_CUR 1
#define __cpp_raw_strings 200710
#define __INT_FAST32_MAX__ 0x7fffffffffffffffL
#define _GLIBCXX_HAVE_EPROTO 1
#define __DBL_HAS_INFINITY__ 1
#define _GLIBCXX_STDIO_EOF -1
#define __SIZEOF_PTHREAD_MUTEX_T 40
#define __GLIBC_USE_IEC_60559_FUNCS_EXT 1
#define _GLIBCXX_NAMESPACE_LDBL_OR_CXX11 _GLIBCXX_NAMESPACE_CXX11
#define __USE_LARGEFILE 1
#define _GLIBCXX_HAVE_ETXTBSY 1
#define __INT64_MAX__ 0x7fffffffffffffffL
#define _GLIBCXX_END_NAMESPACE_ALGO _GLIBCXX_END_NAMESPACE_VERSION
#define __USE_XOPEN 1
#define __USE_XOPEN2K 1
#define _GLIBCXX_HAVE_COSF 1
#define __lldiv_t_defined 1
#define __DEC32_MIN_EXP__ (-94)
#define __DADDR_T_TYPE __S32_TYPE
#define _GLIBCXX_HAVE_UCHAR_H 1
#define __INTPTR_WIDTH__ 64
#define _ISOC11_SOURCE 1
#define _GLIBCXX_HAVE_LINUX_FUTEX 1
#define _GLIBCXX_HAVE_QUICK_EXIT 1
#define _GLIBCXX_HAVE_SINCOSF 1
#define __GLIBC_USE(F) __GLIBC_USE_ ## F
#define _GLIBCXX_HAVE_SINCOSL 1
#define __FD_SETSIZE 1024
#define be32toh(x) __bswap_32 (x)
#define __attribute_format_strfmon__(a,b) __attribute__ ((__format__ (__strfmon__, a, b)))
#define _GLIBCXX_PACKAGE__GLIBCXX_VERSION "version-unused"
#define __FLT32X_HAS_DENORM__ 1
#define __INT_FAST16_TYPE__ long int
#define __HAVE_FLOAT128X 0
#define _SIZE_T_DEFINED 
#define _GLIBCXX_HAVE_GETIPINFO 1
#define _WCHAR_T_DEFINED_ 
#define __USE_POSIX199506 1
#define _FEATURES_H 1
#define __LDBL_HAS_DENORM__ 1
#define __stub_getmsg 
#define _GLIBCXX11_USE_C99_STDIO 1
#define __stub_fattach 
#define __cplusplus 201402L
#define __cpp_ref_qualifiers 200710
#define __DEC128_MAX__ 9.999999999999999999999999999999999E6144DL
#define __INT_LEAST32_MAX__ 0x7fffffff
#define __DEC32_MIN__ 1E-95DF
#define _GLIBCXX_HAVE_SYS_IPC_H 1
#define _GLIBCXX_HAVE_EXPL 1
#define __dev_t_defined 
#define __DEPRECATED 1
#define __S32_TYPE int
#define __glibc_unlikely(cond) __builtin_expect ((cond), 0)
#define __cpp_rvalue_references 200610
#define __DBL_MAX_EXP__ 1024
#define __WCHAR_WIDTH__ 32
#define __BIT_TYPES_DEFINED__ 1
#define __FLT32_MAX__ 3.40282346638528859811704183484516925e+38F32
#define _GLIBCXX_EXTERN_TEMPLATE 1
#define _GLIBCXX_HAVE_STRERROR_L 1
#define __DEC128_EPSILON__ 1E-33DL
#define __SSE2_MATH__ 1
#define __ATOMIC_HLE_RELEASE 131072
#define __FSFILCNT_T_TYPE __SYSCALL_ULONG_TYPE
#define _GLIBCXX_HAVE_ECANCELED 1
#define __HAVE_FLOAT16 0
#define __PTRDIFF_MAX__ 0x7fffffffffffffffL
#define __CPU_MASK_TYPE __SYSCALL_ULONG_TYPE
#define __amd64 1
#define _GLIBCXX_HAVE_ATTRIBUTE_VISIBILITY 1
#define __STDC_NO_THREADS__ 1
#define __HAVE_FLOAT32 1
#define __USECONDS_T_TYPE __U32_TYPE
#define __OFF_T_TYPE __SYSCALL_SLONG_TYPE
#define __ATOMIC_HLE_ACQUIRE 65536
#define __FLT32_HAS_QUIET_NAN__ 1
#define __GNUG__ 7
#define _T_WCHAR 
#define _GLIBCXX_HAVE___CXA_THREAD_ATEXIT_IMPL 1
#define __LONG_LONG_MAX__ 0x7fffffffffffffffLL
#define __SIZEOF_SIZE_T__ 8
#define __GLIBCXX__ 20180720
#define _WCHAR_T 
#define __HAVE_FLOAT64 1
#define __cpp_rvalue_reference 200610
#define __FD_ZERO(fdsp) do { int __d0, __d1; __asm__ __volatile__ ("cld; rep; " __FD_ZERO_STOS : "=c" (__d0), "=D" (__d1) : "a" (0), "0" (sizeof (fd_set) / sizeof (__fd_mask)), "1" (&__FDS_BITS (fdsp)[0]) : "memory"); } while (0)
#define __cpp_nsdmi 200809
#define _GLIBCXX_HAVE_INT64_T_LONG 1
#define __FLT64X_MIN_EXP__ (-16381)
#define __SIZEOF_WINT_T__ 4
#define __NO_CTYPE 1
#define __f32x(x) x
#define __CORRECT_ISO_CPP_STRING_H_PROTO 
#define __u_intN_t(N,MODE) typedef unsigned int u_int ##N ##_t __attribute__ ((__mode__ (MODE)))
#define __LONG_LONG_WIDTH__ 64
#define __cpp_initializer_lists 200806
#define __U16_TYPE unsigned short int
#define _GLIBCXX_HAVE_SYS_PARAM_H 1
#define _GCC_WCHAR_T 
#define __FLT32_MAX_EXP__ 128
#define _SIGSET_NWORDS (1024 / (8 * sizeof (unsigned long int)))
#define __HAVE_FLOATN_NOT_TYPEDEF 0
#define __PTHREAD_MUTEX_LOCK_ELISION 1
#define __cpp_hex_float 201603
#define __GCC_HAVE_DWARF2_CFI_ASM 1
#define _GLIBCXX_HOSTED 1
#define __GXX_ABI_VERSION 1011
#define __WTERMSIG(status) ((status) & 0x7f)
#define _GLIBCXX_HAS_GTHREADS 1
#define __USE_GNU 1
#define __FLT128_HAS_INFINITY__ 1
#define __FLT_MIN_EXP__ (-125)
#define __FD_CLR(d,set) ((void) (__FDS_BITS (set)[__FD_ELT (d)] &= ~__FD_MASK (d)))
#define WEXITED 4
#define _GLIBCXX_HAVE_ENODATA 1
#define __glibc_clang_has_extension(ext) 0
#define __FD_ELT(d) ((d) / __NFDBITS)
#define WNOHANG 1
#define alloca(size) __builtin_alloca (size)
#define _GLIBCXX_BEGIN_NAMESPACE_CXX11 namespace __cxx11 {
#define __HAVE_DISTINCT_FLOAT16 __HAVE_FLOAT16
#define __extern_always_inline extern __always_inline __attribute__ ((__gnu_inline__))
#define _GLIBCXX_HAVE_FINITEF 1
#define _GLIBCXX_HAVE_FINITEL 1
#define __cpp_lambdas 200907
#define __HAVE_GENERIC_SELECTION 0
#define _GLIBCXX_HAVE_SINHF 1
#define _GLIBCXX_HAVE_SINHL 1
#define __FLT64X_HAS_QUIET_NAN__ 1
#define __INT_FAST64_TYPE__ long int
#define _GLIBCXX_HAVE_SQRTF 1
#define _GLIBCXX_HAVE_SQRTL 1
#define _GLIBCXX_HAVE_STDALIGN_H 1
#define __FLT64_DENORM_MIN__ 4.94065645841246544176568792868221372e-324F64
#define __DBL_MIN__ double(2.22507385850720138309023271733240406e-308L)
#define _GLIBCXX_HAVE_SYS_SEM_H 1
#define __PIE__ 2
#define _GLIBCXX_HAVE_ENOTRECOVERABLE 1
#define _GLIBCXX_HAVE_ENOLINK 1
#define _GLIBCXX14_CONSTEXPR constexpr
#define __USE_XOPEN2KXSI 1
#define __WCOREFLAG 0x80
#define __REDIRECT(name,proto,alias) name proto __asm__ (__ASMNAME (#alias))
#define __HAVE_DISTINCT_FLOAT64 0
#define htobe32(x) __bswap_32 (x)
#define __LP64__ 1
#define __HAVE_FLOAT32X 1
#define __FLT32X_EPSILON__ 2.22044604925031308084726333618164062e-16F32x
#define __USE_UNIX98 1
#define __MODE_T_TYPE __U32_TYPE
#define _GLIBCXX_HAVE_STRINGS_H 1
#define __LEAF_ATTR __attribute__ ((__leaf__))
#define __DECIMAL_BID_FORMAT__ 1
#define __RLIM64_T_TYPE __UQUAD_TYPE
#define _GLIBCXX_USE_C99_INTTYPES_WCHAR_T_TR1 1
#define _GLIBCXX_USE_DEPRECATED 1
#define _GLIBCXX_HAVE_SETENV 1
#define __CFLOAT64X _Complex long double
#define _GLIBCXX_PACKAGE_URL ""
#define __FLT64_MIN_10_EXP__ (-307)
#define __FDS_BITS(set) ((set)->fds_bits)
#define _GLIBCXX_HAVE_ETIMEDOUT 1
#define __FLT64X_DECIMAL_DIG__ 21
#define __DEC128_MIN__ 1E-6143DL
#define _GLIBCXX_ATOMIC_BUILTINS 1
#define __REGISTER_PREFIX__ 
#define __UINT16_MAX__ 0xffff
#define __DBL_HAS_DENORM__ 1
#define __attribute_pure__ __attribute__ ((__pure__))
#define __HAVE_DISTINCT_FLOAT128X __HAVE_FLOAT128X
#define _GLIBCXX_HAVE_SINCOS 1
#define __USE_POSIX2 1
#define __FLT32_MIN__ 1.17549435082228750796873653722224568e-38F32
#define __UINT8_TYPE__ unsigned char
#define __SLONGWORD_TYPE long int
#define _GLIBCXX_ABI_TAG_CXX11 __attribute ((__abi_tag__ ("cxx11")))
#define _GLIBCXX_HAVE_ISWBLANK 1
#define __REDIRECT_LDBL(name,proto,alias) __REDIRECT (name, proto, alias)
#define __NO_INLINE__ 1
#define __warndecl(name,msg) extern void name (void) __attribute__((__warning__ (msg)))
#define _GLIBCXX_HAVE_ALIGNED_ALLOC 1
#define __FLT_MANT_DIG__ 24
#define __LDBL_DECIMAL_DIG__ 21
#define _GLIBCXX_HAVE_COMPLEX_H 1
#define __VERSION__ "7.3.0"
#define _GLIBCXX11_USE_C99_STDLIB 1
#define _GLIBCXX_HAVE_INT64_T 1
#define _GLIBCXX_USE_CONSTEXPR constexpr
#define FD_ZERO(fdsetp) __FD_ZERO (fdsetp)
#define __UINT64_C(c) c ## UL
#define _GLIBCXX_HAVE_TANHF 1
#define _SYS_CDEFS_H 1
#define _GLIBCXX_HAVE_TANHL 1
#define __cpp_unicode_characters 200704
#define _GLIBCXX_USE_DECIMAL_FLOAT 1
#define _GLIBCXX_HAVE_ENOSPC 1
#define _STDC_PREDEF_H 1
#define __PTHREAD_MUTEX_HAVE_PREV 1
#define _GLIBCXX_PACKAGE_BUGREPORT ""
#define __INT_WCHAR_T_H 
#define __USE_XOPEN2K8 1
#define __cpp_decltype_auto 201304
#define WEXITSTATUS(status) __WEXITSTATUS (status)
#define __W_EXITCODE(ret,sig) ((ret) << 8 | (sig))
#define _GLIBCXX_THROW(_EXC) 
#define __STRING(x) #x
#define __GCC_ATOMIC_INT_LOCK_FREE 2
#define _GLIBCXX_USE_NOEXCEPT noexcept
#define __FLT128_MAX_EXP__ 16384
#define __INO_T_TYPE __SYSCALL_ULONG_TYPE
#define _GLIBCXX_HAVE_ENOSTR 1
#define _GLIBCXX_BEGIN_NAMESPACE_CONTAINER _GLIBCXX_BEGIN_NAMESPACE_VERSION
#define _GLIBCXX_HAVE_HYPOT 1
#define __ssize_t_defined 
#define __GNUC_PREREQ(maj,min) ((__GNUC__ << 16) + __GNUC_MINOR__ >= ((maj) << 16) + (min))
#define __FLT32_MANT_DIG__ 24
#define _SYS_SIZE_T_H 
#define _GLIBCXX_HAVE_EPERM 1
#define __FLOAT_WORD_ORDER__ __ORDER_LITTLE_ENDIAN__
#define __FSBLKCNT64_T_TYPE __UQUAD_TYPE
#define __SUSECONDS_T_TYPE __SYSCALL_SLONG_TYPE
#define __SIZE_T__ 
#define __stub_gtty 
#define __NLINK_T_TYPE __SYSCALL_ULONG_TYPE
#define __nlink_t_defined 
#define __attribute_deprecated_msg__(msg) __attribute__ ((__deprecated__ (msg)))
#define _GLIBCXX_HAVE_TGMATH_H 1
#define _GLIBCXX11_USE_C99_COMPLEX 1
#define __stub_sstk 
#define __glibc_macro_warning1(message) _Pragma (#message)
#define __wur 
#define __STDC_IEC_559_COMPLEX__ 1
#define __FLT128_HAS_DENORM__ 1
#define _GLIBCXX_NOEXCEPT noexcept
#define _GLIBCXX_HAVE_WCSTOF 1
#define __off64_t_defined 
#define _GLIBCXX_WEAK_DEFINITION 
#define __FLT128_DIG__ 33
#define __SCHAR_WIDTH__ 8
#define _GLIBCXX_USE_C99_INTTYPES_TR1 1
#define __INT32_C(c) c
#define __DEC64_EPSILON__ 1E-15DD
#define __ORDER_PDP_ENDIAN__ 3412
#define __DEC128_MIN_EXP__ (-6142)
#define __PDP_ENDIAN 3412
#define _ISOC95_SOURCE 1
#define _GLIBCXX_HAVE_ENOSR 1
#define __FLT32_MAX_10_EXP__ 38
#define BYTE_ORDER __BYTE_ORDER
#define __INT_FAST32_TYPE__ long int
#define __have_pthread_attr_t 1
#define _GLIBCXX_HAVE_LIMIT_DATA 1
#define _GLIBCXX_NOEXCEPT_QUAL 
#define _GLIBCXX_BEGIN_NAMESPACE_LDBL_OR_CXX11 _GLIBCXX_BEGIN_NAMESPACE_CXX11
#define _BITS_TYPESIZES_H 1
#define htole32(x) __uint32_identity (x)
#define __SYSCALL_SLONG_TYPE __SLONGWORD_TYPE
#define _GLIBCXX_USE_C99_MATH_TR1 1
#define WSTOPSIG(status) __WSTOPSIG (status)
#define _GLIBCXX_USE_C99_MATH _GLIBCXX11_USE_C99_MATH
#define __UINT_LEAST16_TYPE__ short unsigned int
#define __WIFEXITED(status) (__WTERMSIG(status) == 0)
#define STDC_HEADERS 1
#define RAND_MAX 2147483647
#define __FLT64X_HAS_INFINITY__ 1
#define unix 1
#define __USE_POSIX 1
#define __FD_ZERO_STOS "stosq"
#define _GLIBCXX_HAVE_STRING_H 1
#define __INT16_MAX__ 0x7fff
#define __THROWNL throw ()
#define _BSD_SIZE_T_ 
#define __cpp_rtti 199711
#define __SIZE_TYPE__ long unsigned int
#define __PMT(args) args
#define __UINT64_MAX__ 0xffffffffffffffffUL
#define __va_arg_pack_len() __builtin_va_arg_pack_len ()
#define __ULONGWORD_TYPE unsigned long int
#define _SIZE_T_DECLARED 
#define _GLIBCXX_HAVE_LIMIT_AS 1
#define __FLT64X_DIG__ 18
#define __PTHREAD_COMPAT_PADDING_END 
#define __INT8_TYPE__ signed char
#define __cpp_digit_separators 201309
#define __HAVE_DISTINCT_FLOAT128 1
#define __ELF__ 1
#define __GCC_ASM_FLAG_OUTPUTS__ 1
#define _BITS_BYTESWAP_H 1
#define __ID_T_TYPE __U32_TYPE
#define _GLIBCXX_HAVE_MBSTATE_T 1
#define __warnattr(msg) __attribute__((__warning__ (msg)))
#define __FLT_RADIX__ 2
#define __INT_LEAST16_TYPE__ short int
#define __LDBL_EPSILON__ 1.08420217248550443400745280086994171e-19L
#define _GLIBCXX98_USE_C99_WCHAR 1
#define __UINTMAX_C(c) c ## UL
#define minor(dev) __SYSMACROS_DM (minor) gnu_dev_minor (dev)
#define _POSIX_C_SOURCE 200809L
#define _GLIBCXX_HAVE_DIRENT_H 1
#define _GLIBCXX_HAVE_SYS_STATVFS_H 1
#define __GLIBCXX_BITSIZE_INT_N_0 128
#define _GLIBCXX17_CONSTEXPR 
#define __uid_t_defined 
#define __k8 1
#define _GLIBCXX_NO_OBSOLETE_ISINF_ISNAN_DYNAMIC __GLIBC_PREREQ(2,23)
#define __SIZEOF_PTHREAD_BARRIERATTR_T 4
#define __LDBL_REDIR(name,proto) name proto
#define __SIG_ATOMIC_MAX__ 0x7fffffff
#define __f32(x) x ##f
#define __blksize_t_defined 
#define _GLIBCXX_HAVE_LIMIT_RSS 1
#define _GLIBCXX_X86_RDRAND 1
#define __GCC_ATOMIC_WCHAR_T_LOCK_FREE 2
#define __cpp_sized_deallocation 201309
#define _GLIBCXX_USE_DUAL_ABI 1
#define __bswap_constant_16(x) ((unsigned short int) ((((x) >> 8) & 0xff) | (((x) & 0xff) << 8)))
#define __SIZEOF_PTRDIFF_T__ 8
#define __NFDBITS (8 * (int) sizeof (__fd_mask))
#define _GLIBCXX_VISIBILITY(V) __attribute__ ((__visibility__ (#V)))
#define _GLIBCXX_HAVE_COSL 1
#define __f64(x) x
#define __bswap_constant_32(x) ((((x) & 0xff000000) >> 24) | (((x) & 0x00ff0000) >> 8) | (((x) & 0x0000ff00) << 8) | (((x) & 0x000000ff) << 24))
#define _GLIBCXX_USE_FLOAT128 1
#define __stub_sigreturn 
#define __errordecl(name,msg) extern void name (void) __attribute__((__error__ (msg)))
#define _GLIBCXX_HAVE_UTIME_H 1
#define _XOPEN_SOURCE_EXTENDED 1
#define __FLT32X_MANT_DIG__ 53
#define _GLIBCXX_END_NAMESPACE_VERSION 
#define __restrict_arr 
#define _GLIBCXX_USE_UTIMENSAT 1
#define __CFLOAT32X _Complex double
#define __attribute_artificial__ __attribute__ ((__artificial__))
#define __USE_MISC 1
#define __UWORD_TYPE unsigned long int
#define __x86_64__ 1
#define _SIZE_T_ 
#define __bswap_constant_64(x) (__extension__ ((((x) & 0xff00000000000000ull) >> 56) | (((x) & 0x00ff000000000000ull) >> 40) | (((x) & 0x0000ff0000000000ull) >> 24) | (((x) & 0x000000ff00000000ull) >> 8) | (((x) & 0x00000000ff000000ull) << 8) | (((x) & 0x0000000000ff0000ull) << 24) | (((x) & 0x000000000000ff00ull) << 40) | (((x) & 0x00000000000000ffull) << 56)))
#define __FLT32X_MIN_EXP__ (-1021)
#define __PTHREAD_RWLOCK_INT_FLAGS_SHARED 1
#define __DEC32_SUBNORMAL_MIN__ 0.000001E-95DF
#define _GLIBCXX_SYNCHRONIZATION_HAPPENS_BEFORE(A) 
#define _GLIBCXX_HAVE_DLFCN_H 1
#define strdupa(s) (__extension__ ({ const char *__old = (s); size_t __len = strlen (__old) + 1; char *__new = (char *) __builtin_alloca (__len); (char *) memcpy (__new, __old, __len); }))
#define _WCHAR_T_H 
#define _GLIBCXX_END_EXTERN_C }
#define __INT_FAST16_MAX__ 0x7fffffffffffffffL
#define __stub_revoke 
#define __timer_t_defined 1
#define __WCLONE 0x80000000
#define _GLIBCXX_HAVE_VSWSCANF 1
#define _GLIBCXX_END_NAMESPACE_CXX11 }
#define _GLIBCXX_ICONV_CONST 
#define __FLT64_DIG__ 15
#define _GLIBCXX_NAMESPACE_LDBL 
#define __UINT_FAST32_MAX__ 0xffffffffffffffffUL
#define __UINT_LEAST64_TYPE__ long unsigned int
#define __FLT_HAS_QUIET_NAN__ 1
#define __FLT_MAX_10_EXP__ 38
#define _GLIBCXX_HAVE_FLOORF 1
#define __FD_MASK(d) ((__fd_mask) (1UL << ((d) % __NFDBITS)))
#define _GLIBCXX_HAVE_FLOORL 1
#define __glibc_c99_flexarr_available 1
#define __LONG_MAX__ 0x7fffffffffffffffL
#define __WCHAR_T__ 
#define __FLT64X_HAS_DENORM__ 1
#define __DEC128_SUBNORMAL_MIN__ 0.000000000000000000000000000000001E-6143DL
#define _GLIBCXX_NORETURN __attribute__ ((__noreturn__))
#define __FLT_HAS_INFINITY__ 1
#define __WSTOPSIG(status) __WEXITSTATUS(status)
#define _BITS_PTHREADTYPES_COMMON_H 1
#define __cpp_unicode_literals 200710
#define __LONG_LONG_PAIR(HI,LO) LO, HI
#define __UINT_FAST16_TYPE__ long unsigned int
#define __bos0(ptr) __builtin_object_size (ptr, 0)
#define __DEC64_MAX__ 9.999999999999999E384DD
#define __ENUM_IDTYPE_T 1
#define __PTHREAD_MUTEX_USE_UNION 0
#define WIFSTOPPED(status) __WIFSTOPPED (status)
#define __INT_FAST32_WIDTH__ 64
#define NULL __null
#define _GLIBCXX_BITS_STD_ABS_H 
#define __CHAR16_TYPE__ short unsigned int
#define LT_OBJDIR ".libs/"
#define __PRAGMA_REDEFINE_EXTNAME 1
#define BIG_ENDIAN __BIG_ENDIAN
#define _GLIBCXX_HAVE_SINF 1
#define _GLIBCXX_HAVE_SINL 1
#define _STRINGS_H 1
#define __SIZE_WIDTH__ 64
#define __BLKSIZE_T_TYPE __SYSCALL_SLONG_TYPE
#define __SEG_FS 1
#define __INT_LEAST16_MAX__ 0x7fff
#define __stub_lchmod 
#define __DEC64_MANT_DIG__ 16
#define __UINT_LEAST32_MAX__ 0xffffffffU
#define _GLIBCXX_MANGLE_SIZE_T m
#define __SEG_GS 1
#define __CORRECT_ISO_CPP_STRINGS_H_PROTO 
#define __FLT32_DENORM_MIN__ 1.40129846432481707092372958328991613e-45F32
#define __GCC_ATOMIC_LONG_LOCK_FREE 2
#define __SIG_ATOMIC_WIDTH__ 32
#define _GLIBCXX_RELEASE 7
#define __HAVE_DISTINCT_FLOAT32 0
#define __INT_LEAST64_TYPE__ long int
#define htole16(x) __uint16_identity (x)
#define _GLIBCXX_HAVE_FCNTL_H 1
#define _GLIBCXX_SYMVER_GNU 1
#define __INT16_TYPE__ short int
#define __INT_LEAST8_TYPE__ signed char
#define __DEC32_MAX_EXP__ 97
#define __INT_FAST8_MAX__ 0x7f
#define __FLT128_MAX__ 1.18973149535723176508575932662800702e+4932F128
#define __INTPTR_MAX__ 0x7fffffffffffffffL
#define _SYS_TYPES_H 1
#define linux 1
#define __cpp_range_based_for 200907
#define __FLT64_HAS_QUIET_NAN__ 1
#define _GLIBCXX_HAVE_WCTYPE_H 1
#define PDP_ENDIAN __PDP_ENDIAN
#define htole64(x) __uint64_identity (x)
#define WIFSIGNALED(status) __WIFSIGNALED (status)
#define _GLIBCXX_HAVE_LDEXPF 1
#define _GLIBCXX_USE_TMPNAM 1
#define __FLT32_MIN_10_EXP__ (-37)
#define __SSE2__ 1
#define __KEY_T_TYPE __S32_TYPE
#define __EXCEPTIONS 1
#define __WORDSIZE 64
#define __BEGIN_DECLS extern "C" {
#define __LDBL_MANT_DIG__ 64
#define __SIZEOF_PTHREAD_MUTEXATTR_T 4
#define _GLIBCXX_HAVE_INTTYPES_H 1
#define __DBL_HAS_QUIET_NAN__ 1
#define __FLT64_HAS_INFINITY__ 1
#define _GLIBCXX_CSTDLIB 1
#define __FLT64X_MAX__ 1.18973149535723176502126385303097021e+4932F64x
#define _STRING_H 1
#define __SIG_ATOMIC_MIN__ (-__SIG_ATOMIC_MAX__ - 1)
#define __code_model_small__ 1
#define __RLIM_T_TYPE __SYSCALL_ULONG_TYPE
#define __SYSMACROS_DM(symbol) __SYSMACROS_DM1 (In the GNU C Library, #symbol is defined\n by <sys/sysmacros.h>. For historical compatibility, it is\n currently defined by <sys/types.h> as well, but we plan to\n remove this soon. To use #symbol, include <sys/sysmacros.h>\n directly. If you did not intend to use a system-defined macro\n #symbol, you should undefine it after including <sys/types.h>.)
#define le64toh(x) __uint64_identity (x)
#define __cpp_return_type_deduction 201304
#define __ino_t_defined 
#define __k8__ 1
#define __INTPTR_TYPE__ long int
#define __UINT16_TYPE__ short unsigned int
#define __WCHAR_TYPE__ int
#define __CLOCKID_T_TYPE __S32_TYPE
#define __SIZEOF_FLOAT__ 4
#define _GLIBCXX_PACKAGE_TARNAME "libstdc++"
#define __stub_fdetach 
#define __pic__ 2
#define __PTHREAD_RWLOCK_ELISION_EXTRA 0, { 0, 0, 0, 0, 0, 0, 0 }
#define __UINTPTR_MAX__ 0xffffffffffffffffUL
#define __INT_FAST64_WIDTH__ 64
#define __DEC64_MIN_EXP__ (-382)
#define __stub_chflags 
#define _GLIBCXX_USE_CXX11_ABI 1
#define __cpp_decltype 200707
#define __BYTE_ORDER __LITTLE_ENDIAN
#define _GLIBCXX_USE_C99 1
#define __FLT32_DECIMAL_DIG__ 9
#define _GLIBCXX_DEFAULT_ABI_TAG _GLIBCXX_ABI_TAG_CXX11
#define __INT_FAST64_MAX__ 0x7fffffffffffffffL
#define __GCC_ATOMIC_TEST_AND_SET_TRUEVAL 1
#define __LITTLE_ENDIAN 1234
#define major(dev) __SYSMACROS_DM (major) gnu_dev_major (dev)
#define __FLT_DIG__ 6
#define __FSID_T_TYPE struct { int __val[2]; }
#define _GLIBCXX_HAVE_EIDRM 1
#define _GLIBCXX_HAVE_ICONV 1
#define _GLIBCXX_CONSTEXPR constexpr
#define __PTHREAD_COMPAT_PADDING_MID 
#define __FLT64X_MAX_EXP__ 16384
#define _WCHAR_T_DECLARED 
#define __UINT_FAST64_TYPE__ long unsigned int
#define WNOWAIT 0x01000000
#define __LDBL_REDIR_DECL(name) 
#define _GLIBCXX_HAVE_STRTOF 1
#define _GLIBCXX_HAVE_MEMORY_H 1
#define __INT_MAX__ 0x7fffffff
#define __amd64__ 1
#define __S16_TYPE short int
#define __bos(ptr) __builtin_object_size (ptr, __USE_FORTIFY_LEVEL > 1)
#define _T_SIZE_ 
#define __FD_SET(d,set) ((void) (__FDS_BITS (set)[__FD_ELT (d)] |= __FD_MASK (d)))
#define __WNOTHREAD 0x20000000
#define __USE_LARGEFILE64 1
#define __INT64_TYPE__ long int
#define __FLT_MAX_EXP__ 128
#define _GLIBCXX_HAVE_STRXFRM_L 1
#define __gid_t_defined 
#define _GLIBCXX_USE_SC_NPROCESSORS_ONLN 1
#define __ORDER_BIG_ENDIAN__ 4321
#define _GLIBCXX17_INLINE 
#define __DBL_MANT_DIG__ 53
#define ___int_size_t_h 
#define __TIMER_T_TYPE void *
#define _GLIBCXX_END_NAMESPACE_LDBL_OR_CXX11 _GLIBCXX_END_NAMESPACE_CXX11
#define __cpp_inheriting_constructors 201511
#define __WIFCONTINUED(status) ((status) == __W_CONTINUED)
#define _GLIBCXX_HAVE_FENV_H 1
#define _GLIBCXX_HAVE_STDBOOL_H 1
#define __SIZEOF_FLOAT128__ 16
#define __INT_LEAST64_MAX__ 0x7fffffffffffffffL
#define __clock_t_defined 1
#define __GLIBC_MINOR__ 27
#define __DEC64_MIN__ 1E-383DD
#define __WINT_TYPE__ unsigned int
#define __UINT_LEAST32_TYPE__ unsigned int
#define __SIZEOF_SHORT__ 2
#define __SSE__ 1
#define __LDBL_MIN_EXP__ (-16381)
#define _GLIBCXX_HAVE_LOGF 1
#define _GLIBCXX_HAVE_LOGL 1
#define _GLIBCXX_HAVE_EXPF 1
#define __FLT64_MAX__ 1.79769313486231570814527423731704357e+308F64
#define _GLIBCXX_NOEXCEPT_PARM 
#define _GLIBCXX_TXN_SAFE 
#define __WINT_WIDTH__ 32
#define __REDIRECT_NTHNL(name,proto,alias) name proto __THROWNL __asm__ (__ASMNAME (#alias))
#define __INT_LEAST8_MAX__ 0x7f
#define __extern_inline extern __inline __attribute__ ((__gnu_inline__))
#define __USE_POSIX199309 1
#define __FLT32X_MAX_10_EXP__ 308
#define _GLIBCXX_HAVE_LOG10F 1
#define _GLIBCXX_HAVE_LOG10L 1
#define __SIZEOF_INT128__ 16
#define __BLKCNT64_T_TYPE __SQUAD_TYPE
#define __LDBL_MAX_10_EXP__ 4932
#define __W_CONTINUED 0xffff
#define __ATOMIC_RELAXED 0
#define __FSBLKCNT_T_TYPE __SYSCALL_ULONG_TYPE
#define __DBL_EPSILON__ double(2.22044604925031308084726333618164062e-16L)
#define __stub_stty 
#define le16toh(x) __uint16_identity (x)
#define __FLT128_MIN__ 3.36210314311209350626267781732175260e-4932F128
#define _SIZET_ 
#define _LP64 1
#define __REDIRECT_NTH_LDBL(name,proto,alias) __REDIRECT_NTH (name, proto, alias)
#define __UINT8_C(c) c
#define _GLIBCXX_HAVE_CEILF 1
#define _GLIBCXX_HAVE_CEILL 1
#define _BITS_STDINT_INTN_H 1
#define __FLT64_MAX_EXP__ 1024
#define __INT_LEAST32_TYPE__ int
#define __wchar_t__ 
#define __SIZEOF_WCHAR_T__ 4
#define _GLIBCXX_USE_C99_WCHAR _GLIBCXX11_USE_C99_WCHAR
#define _ISOC99_SOURCE 1
#define __REDIRECT_NTH(name,proto,alias) name proto __THROW __asm__ (__ASMNAME (#alias))
#define _GLIBCXX_STD_C std
#define _LARGEFILE64_SOURCE 1
#define _GLIBCXX_USE_C99_STDINT_TR1 1
#define __stub___compat_bdflush 
#define MB_CUR_MAX (__ctype_get_mb_cur_max ())
#define __FLT128_HAS_QUIET_NAN__ 1
#define be64toh(x) __bswap_64 (x)
#define _GLIBCXX_STDIO_SEEK_END 2
#define __INT_FAST8_TYPE__ signed char
#define __PID_T_TYPE __S32_TYPE
#define _GLIBCXX_HAVE_TANF 1
#define _GLIBCXX_USE_ST_MTIM 1
#define __FLT64X_MIN__ 3.36210314311209350626267781732175260e-4932F64x
#define _GLIBCXX_USE_NLS 1
#define _GLIBCXX_HAVE_STDLIB_H 1
#define _THREAD_SHARED_TYPES_H 1
#define __f128(x) x ##q
#define __GNUC_STDC_INLINE__ 1
#define __FLT64_HAS_DENORM__ 1
#define __WORDSIZE_TIME64_COMPAT32 1
#define _GLIBCXX_DEPRECATED __attribute__ ((__deprecated__))
#define __FLT32_EPSILON__ 1.19209289550781250000000000000000000e-7F32
#define _GLIBCXX_HAVE_AS_SYMVER_DIRECTIVE 1
#define _BITS_TYPES___LOCALE_T_H 1
#define __DBL_DECIMAL_DIG__ 17
#define __STDC_UTF_32__ 1
#define __INT_FAST8_WIDTH__ 8
#define __FXSR__ 1
#define __DEC_EVAL_METHOD__ 2
#define _SIZE_T 
#define _GLIBCXX_USE_GETTIMEOFDAY 1
#define __FLT32X_MAX__ 1.79769313486231570814527423731704357e+308F32x
#define __WIFSTOPPED(status) (((status) & 0xff) == 0x7f)
#define __ULONG32_TYPE unsigned int
#define _GLIBCXX_FAST_MATH 0
#define __BIG_ENDIAN 4321
#define __suseconds_t_defined 
#define __W_STOPCODE(sig) ((sig) << 8 | 0x7f)
#define strndupa(s,n) (__extension__ ({ const char *__old = (s); size_t __len = strnlen (__old, (n)); char *__new = (char *) __builtin_alloca (__len + 1); __new[__len] = '\0'; (char *) memcpy (__new, __old, __len); }))
#define __off_t_defined 
#define _GLIBCXX_HAVE_LDEXPL 1
#define _GLIBCXX_NOTHROW _GLIBCXX_USE_NOEXCEPT
#define __mode_t_defined 
#define _GCC_SIZE_T 
#define __INO64_T_TYPE __UQUAD_TYPE
#define __cpp_runtime_arrays 198712
#define __UINT64_TYPE__ long unsigned int
#define __USE_XOPEN2K8XSI 1
#define __UINT32_C(c) c ## U
#define __INTMAX_MAX__ 0x7fffffffffffffffL
#define __cpp_alias_templates 200704
#define __BYTE_ORDER__ __ORDER_LITTLE_ENDIAN__
#define __size_t__ 
#define htobe64(x) __bswap_64 (x)
#define __FLT_DENORM_MIN__ 1.40129846432481707092372958328991613e-45F
#define _GLIBCXX_HAVE_LC_MESSAGES 1
#define _GLIBCXX_HAVE_FINITE 1
#define __INT8_MAX__ 0x7f
#define __LONG_WIDTH__ 64
#define __PIC__ 2
#define _GLIBCXX_HAVE_EOVERFLOW 1
#define __UINT_FAST32_TYPE__ long unsigned int
#define FD_ISSET(fd,fdsetp) __FD_ISSET (fd, fdsetp)
#define _GLIBCXX_HAVE_EXCEPTION_PTR_SINCE_GCC46 1
#define __INO_T_MATCHES_INO64_T 1
#define _GLIBCXX_HAVE_FLOAT_H 1
#define __CHAR32_TYPE__ unsigned int
#define _GLIBCXX_BEGIN_EXTERN_C extern "C" {
#define __FLT_MAX__ 3.40282346638528859811704183484516925e+38F
#define _GLIBCXX_HAVE_VFWSCANF 1
#define _GLIBCXX_USE_ALLOCATOR_NEW 1
#define __fsfilcnt_t_defined 
#define __blkcnt_t_defined 
#define __HAVE_FLOAT128 1
#define __cpp_constexpr 201304
#define _GLIBCXX98_USE_C99_STDIO 1
#define __attribute_format_arg__(x) __attribute__ ((__format_arg__ (x)))
#define _GLIBCXX_HAVE_SYS_TYPES_H 1
#define __INT32_TYPE__ int
#define __SIZEOF_DOUBLE__ 8
#define __cpp_exceptions 199711
#define __FLT_MIN_10_EXP__ (-37)
#define __time_t_defined 1
#define _SYS_SYSMACROS_H 1
#define __FLT64_MIN__ 2.22507385850720138309023271733240406e-308F64
#define __INT_LEAST32_WIDTH__ 32
#define __SWORD_TYPE long int
#define __INTMAX_TYPE__ long int
#define _GLIBCXX11_USE_C99_MATH 1
#define be16toh(x) __bswap_16 (x)
#define __PTHREAD_SPINS 0, 0
#define __DEC128_MAX_EXP__ 6145
#define _T_SIZE 
#define __va_arg_pack() __builtin_va_arg_pack ()
#define _GLIBCXX_HAVE_TLS 1
#define _GLIBCXX_HAVE_ACOSF 1
#define _GLIBCXX_HAVE_ACOSL 1
#define __FLT32X_HAS_QUIET_NAN__ 1
#define __ATOMIC_CONSUME 1
#define __GNUC_MINOR__ 3
#define __GLIBCXX_TYPE_INT_N_0 __int128
#define __INT_FAST16_WIDTH__ 64
#define __UINTMAX_MAX__ 0xffffffffffffffffUL
#define __DEC32_MANT_DIG__ 7
#define LITTLE_ENDIAN __LITTLE_ENDIAN
#define __FLT32X_DENORM_MIN__ 4.94065645841246544176568792868221372e-324F32x
#define __key_t_defined 
#define __glibc_clang_prereq(maj,min) 0
#define _GTHREAD_USE_MUTEX_TIMEDLOCK 1
#define __DBL_MAX_10_EXP__ 308
#define __LDBL_DENORM_MIN__ 3.64519953188247460252840593361941982e-4951L
#define __USE_XOPEN_EXTENDED 1
#define _GLIBCXX_HAVE_SYS_RESOURCE_H 1
#define __INT16_C(c) c
#define _GLIBCXX_USE_REALPATH 1
#define __U32_TYPE unsigned int
#define __SIZEOF_PTHREAD_RWLOCK_T 56
#define __cpp_generic_lambdas 201304
#define _GLIBCXX_HAVE_SYS_IOCTL_H 1
#define FD_CLR(fd,fdsetp) __FD_CLR (fd, fdsetp)
#define __STDC__ 1
#define _GLIBCXX_HAVE_VWSCANF 1
#define __attribute_malloc__ __attribute__ ((__malloc__))
#define __FLT32X_DIG__ 15
#define _GLIBCXX_USE_C99_CTYPE_TR1 1
#define __PTRDIFF_TYPE__ long int
#define _GLIBCXX_END_NAMESPACE_CONTAINER _GLIBCXX_END_NAMESPACE_VERSION
#define __ino64_t_defined 
#define __clockid_t_defined 1
#define _GLIBCXX_NOEXCEPT_IF(_COND) noexcept(_COND)
#define __attribute_noinline__ __attribute__ ((__noinline__))
#define _GLIBCXX_BEGIN_NAMESPACE_LDBL 
#define __ATOMIC_SEQ_CST 5
#define __CLOCK_T_TYPE __SYSCALL_SLONG_TYPE
#define _BITS_FLOATN_H 
#define __UINT32_TYPE__ unsigned int
#define __FLT32X_MIN_10_EXP__ (-307)
#define __UINTPTR_TYPE__ long unsigned int
#define _GLIBCXX_USE_CLOCK_MONOTONIC 1
#define _GLIBCXX_INLINE_VERSION 0
#define __DEC64_SUBNORMAL_MIN__ 0.000000000000001E-383DD
#define __FSFILCNT64_T_TYPE __UQUAD_TYPE
#define __DEC128_MANT_DIG__ 34
#define __LDBL_MIN_10_EXP__ (-4931)
#define _GLIBCXX_THROW_OR_ABORT(_EXC) (throw (_EXC))
#define __useconds_t_defined 
#define _GLIBCXX_USE_SCHED_YIELD 1
#define __attribute_deprecated__ __attribute__ ((__deprecated__))
#define __FLT128_EPSILON__ 1.92592994438723585305597794258492732e-34F128
#define __HAVE_DISTINCT_FLOAT64X 0
#define __SSE_MATH__ 1
#define __SIZEOF_LONG_LONG__ 8
#define _GLIBCXX_VERBOSE 1
#define _GLIBCXX_HAVE_ISINFF 1
#define _GLIBCXX_HAVE_ASINF 1
#define __cpp_user_defined_literals 200809
#define _GLIBCXX_HAVE_ISINFL 1
#define _GLIBCXX_HAVE_ASINL 1
#define __USE_ATFILE 1
#define __FLT128_DECIMAL_DIG__ 36
#define __GCC_ATOMIC_LLONG_LOCK_FREE 2
#define __FLT32X_MIN__ 2.22507385850720138309023271733240406e-308F32x
#define __LDBL_DIG__ 18
#define __FLT_DECIMAL_DIG__ 9
#define __UINT_FAST16_MAX__ 0xffffffffffffffffUL
#define _GLIBCXX_PACKAGE_STRING "package-unused version-unused"
#define __WALL 0x40000000
#define __ldiv_t_defined 1
#define __GCC_ATOMIC_SHORT_LOCK_FREE 2
#define _GLIBCXX_USE_NANOSLEEP 1
#define _GLIBCXX11_USE_C99_WCHAR 1
#define __INT_LEAST64_WIDTH__ 64
#define _GLIBCXX_HAVE_ECHILD 1
#define _GLIBCXX_HAVE_UNISTD_H 1
#define __glibc_likely(cond) __builtin_expect ((cond), 1)
#define __FD_ISSET(d,set) ((__FDS_BITS (set)[__FD_ELT (d)] & __FD_MASK (d)) != 0)
#define __UINT_FAST8_TYPE__ unsigned char
#define _GNU_SOURCE 1
#define __N(msgid) (msgid)
#define __P(args) args
#define __cpp_init_captures 201304
#define __ATOMIC_ACQ_REL 4
#define __WCHAR_T 
#define __ATOMIC_RELEASE 3
#define _GLIBCXX_HAVE_EXECINFO_H 1
#define __fsblkcnt_t_defined 
#define _GLIBCXX_USE_INT128 1
#define _STDLIB_H 1
