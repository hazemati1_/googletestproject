#define __S_IFBLK 0060000
#define _XOPEN_XPG3 1
#define _GLIBCXX_HAVE_SYS_SDT_H 1
#define _BITS_SCHED_H 1
#define _BITS_FLOATN_COMMON_H 
#define _SC_CHAR_MIN _SC_CHAR_MIN
#define RE_SYNTAX_POSIX_MINIMAL_BASIC (_RE_SYNTAX_POSIX_COMMON | RE_LIMITED_OPS)
#define __SSP_STRONG__ 3
#define _CS_POSIX_V6_WIDTH_RESTRICTED_ENVS _CS_V6_WIDTH_RESTRICTED_ENVS
#define _CS_POSIX_V6_LP64_OFF64_CFLAGS _CS_POSIX_V6_LP64_OFF64_CFLAGS
#define __DBL_MIN_EXP__ (-1021)
#define _STL_PAIR_H 1
#define ENOTBLK 15
#define EISDIR 21
#define __FLT32X_MAX_EXP__ 1024
#define EMULTIHOP 72
#define __cpp_attributes 200809
#define _CS_POSIX_V6_ILP32_OFFBIG_LIBS _CS_POSIX_V6_ILP32_OFFBIG_LIBS
#define _BITS_WCTYPE_WCHAR_H 1
#define _PC_VDISABLE _PC_VDISABLE
#define _SC_PII _SC_PII
#define _GLIBCXX98_USE_C99_MATH 1
#define RE_NO_SUB (RE_CONTEXT_INVALID_DUP << 1)
#define __CFLOAT32 _Complex float
#define __UINT_LEAST16_MAX__ 0xffff
#define _SC_RE_DUP_MAX _SC_RE_DUP_MAX
#define FLT_MANT_DIG __FLT_MANT_DIG__
#define __ATOMIC_ACQUIRE 2
#define _GLIBCXX_MOVE_BACKWARD3(_Tp,_Up,_Vp) std::move_backward(_Tp, _Up, _Vp)
#define GTEST_DECLARE_int32_(name) GTEST_API_ extern ::testing::internal::Int32 GTEST_FLAG(name)
#define __FLT128_MAX_10_EXP__ 4932
#define RE_DUP_MAX (0x7fff)
#define _SC_PII_OSI_COTS _SC_PII_OSI_COTS
#define _IOS_OUTPUT 2
#define W_EXITCODE(ret,sig) __W_EXITCODE (ret, sig)
#define GTEST_HAS_EXCEPTIONS 1
#define _SC_TTY_NAME_MAX _SC_TTY_NAME_MAX
#define __FLT_MIN__ 1.17549435082228750796873653722224568e-38F
#define __GCC_IEC_559_COMPLEX 2
#define RE_SYNTAX_POSIX_MINIMAL_EXTENDED (_RE_SYNTAX_POSIX_COMMON | RE_CONTEXT_INDEP_ANCHORS | RE_CONTEXT_INVALID_OPS | RE_NO_BK_BRACES | RE_NO_BK_PARENS | RE_NO_BK_REFS | RE_NO_BK_VBAR | RE_UNMATCHED_RIGHT_PAREN_ORD)
#define _GLIBCXX_USE_FCHMOD 1
#define _SC_SPIN_LOCKS _SC_SPIN_LOCKS
#define __cpp_aggregate_nsdmi 201304
#define __bswap_16(x) (__extension__ ({ unsigned short int __v, __x = (unsigned short int) (x); if (__builtin_constant_p (__x)) __v = __bswap_constant_16 (__x); else __asm__ ("rorw $8, %w0" : "=r" (__v) : "0" (__x) : "cc"); __v; }))
#define si_fd _sifields._sigpoll.si_fd
#define __UINT_LEAST8_TYPE__ unsigned char
#define __SIZEOF_FLOAT80__ 16
#define _T_WCHAR_ 
#define _SC_IPV6 _SC_IPV6
#define EUNATCH 49
#define stdout stdout
#define _EXT_TYPE_TRAITS 1
#define INT_LEAST16_MIN (-32767-1)
#define EXPECT_STRCASEEQ(s1,s2) EXPECT_PRED_FORMAT2(::testing::internal::CmpHelperSTRCASEEQ, s1, s2)
#define __CFLOAT64 _Complex double
#define GTEST_DISALLOW_ASSIGN_(type) void operator=(type const &)
#define __flexarr []
#define _STL_UNINITIALIZED_H 1
#define EIO 5
#define ENOCSI 50
#define _GLIBCXX_HAVE_STDALIGN_H 1
#define _IO_FLAGS2_USER_WBUF 8
#define _GLIBCXX_NESTED_EXCEPTION_H 1
#define si_arch _sifields._sigsys._arch
#define _SCHED_H 1
#define ACCESSPERMS (S_IRWXU|S_IRWXG|S_IRWXO)
#define __S64_TYPE long int
#define _POSIX_PRIORITIZED_IO 200809L
#define __stub_fchflags 
#define GTEST_INCLUDE_GTEST_INTERNAL_GTEST_STRING_H_ 
#define STA_PPSFREQ 0x0002
#define _IO_CURRENTLY_PUTTING 0x800
#define SIGBUS 7
#define __INTMAX_C(c) c ## L
#define _SC_2_C_DEV _SC_2_C_DEV
#define EPROTONOSUPPORT 93
#define _BSD_SIZE_T_DEFINED_ 
#define PTHREAD_SCOPE_PROCESS PTHREAD_SCOPE_PROCESS
#define PTHREAD_CANCELED ((void *) -1)
#define _BITS_TIME_H 1
#define FPE_FLTRES FPE_FLTRES
#define _GLIBCXX_TXN_SAFE_DYN 
#define __socklen_t_defined 
#define CLOCK_THREAD_CPUTIME_ID 3
#define __TIME_T_TYPE __SYSCALL_SLONG_TYPE
#define __CHAR_BIT__ 8
#define __FSWORD_T_TYPE __SYSCALL_SLONG_TYPE
#define _XOPEN_CRYPT 1
#define makedev(maj,min) __SYSMACROS_DM (makedev) gnu_dev_makedev (maj, min)
#define _GLIBCXX_HAVE_ETIME 1
#define _GLIBCXX_USE_RANDOM_TR1 1
#define _STAT_VER_KERNEL 0
#define _BITS_LIBIO_H 1
#define ASSERT_PRED3(pred,v1,v2,v3) GTEST_PRED3_(pred, v1, v2, v3, GTEST_FATAL_FAILURE_)
#define _SYS_STAT_H 1
#define GTEST_TYPED_TEST_CASE_P_STATE_(TestCaseName) gtest_typed_test_case_p_state_ ##TestCaseName ##_
#define EREMCHG 78
#define __CPU_ZERO_S(setsize,cpusetp) do __builtin_memset (cpusetp, '\0', setsize); while (0)
#define RE_NEWLINE_ALT (RE_LIMITED_OPS << 1)
#define SIGTTIN 21
#define EEXIST 17
#define INSTANTIATE_TEST_CASE_P(prefix,test_case_name,generator,...) ::testing::internal::ParamGenerator<test_case_name::ParamType> gtest_ ##prefix ##test_case_name ##_EvalGenerator_() { return generator; } ::std::string gtest_ ##prefix ##test_case_name ##_EvalGenerateName_( const ::testing::TestParamInfo<test_case_name::ParamType>& info) { return ::testing::internal::GetParamNameGen<test_case_name::ParamType> (__VA_ARGS__)(info); } int gtest_ ##prefix ##test_case_name ##_dummy_ GTEST_ATTRIBUTE_UNUSED_ = ::testing::UnitTest::GetInstance()->parameterized_test_registry(). GetTestCasePatternHolder<test_case_name>( #test_case_name, ::testing::internal::CodeLocation( __FILE__, __LINE__))->AddTestCaseInstantiation( #prefix, &gtest_ ##prefix ##test_case_name ##_EvalGenerator_, &gtest_ ##prefix ##test_case_name ##_EvalGenerateName_, __FILE__, __LINE__)
#define __ASSERT_FUNCTION __extension__ __PRETTY_FUNCTION__
#define CLOCK_PROCESS_CPUTIME_ID 2
#define EACCES 13
#define _CS_POSIX_V6_LP64_OFF64_LDFLAGS _CS_POSIX_V6_LP64_OFF64_LDFLAGS
#define __UINT8_MAX__ 0xff
#define _POSIX_CLOCK_SELECTION 200809L
#define __CORRECT_ISO_CPP_STRINGS_H_PROTO 
#define _SC_COLL_WEIGHTS_MAX _SC_COLL_WEIGHTS_MAX
#define _CS_V6_WIDTH_RESTRICTED_ENVS _CS_V6_WIDTH_RESTRICTED_ENVS
#define _STAT_VER_LINUX 1
#define GTEST_INCLUDE_GTEST_INTERNAL_CUSTOM_GTEST_PRINTERS_H_ 
#define _SC_TYPED_MEMORY_OBJECTS _SC_TYPED_MEMORY_OBJECTS
#define _STRINGFWD_H 1
#define __S_ISUID 04000
#define _GLIBCXX_ATOMICITY_H 1
#define RE_NO_GNU_OPS (RE_NO_POSIX_BACKTRACKING << 1)
#define _IOS_BASE_H 1
#define FAIL() GTEST_FAIL()
#define DBL_MAX_10_EXP __DBL_MAX_10_EXP__
#define EDESTADDRREQ 89
#define INT16_C(c) c
#define __WINT_MAX__ 0xffffffffU
#define si_pid _sifields._kill.si_pid
#define GTEST_SUCCEED() GTEST_SUCCESS_("Succeeded")
#define __SIZEOF_PTHREAD_ATTR_T 56
#define _GLIBCXX_HAVE_WRITEV 1
#define _SC_BARRIERS _SC_BARRIERS
#define sa_sigaction __sigaction_handler.sa_sigaction
#define GTEST_ASSERT_NE(val1,val2) ASSERT_PRED_FORMAT2(::testing::internal::CmpHelperNE, val1, val2)
#define __FLT32_MIN_EXP__ (-125)
#define _GLIBCXX_END_NAMESPACE_LDBL 
#define _SC_XOPEN_XPG2 _SC_XOPEN_XPG2
#define F_TEST 3
#define SIGSTKSZ 8192
#define INT8_MAX (127)
#define CLD_TRAPPED CLD_TRAPPED
#define UINT_LEAST64_MAX (__UINT64_C(18446744073709551615))
#define PDP_ENDIAN __PDP_ENDIAN
#define stderr stderr
#define GTEST_DEFINE_STATIC_MUTEX_(mutex) ::testing::internal::MutexBase mutex = { PTHREAD_MUTEX_INITIALIZER, false, pthread_t() }
#define __GLIBC_PREREQ(maj,min) ((__GLIBC__ << 16) + __GLIBC_MINOR__ >= ((maj) << 16) + (min))
#define LC_ALL __LC_ALL
#define _XOPEN_SOURCE 700
#define __cpp_static_assert 200410
#define EILSEQ 84
#define RE_UNMATCHED_RIGHT_PAREN_ORD (RE_NO_EMPTY_RANGES << 1)
#define _SC_PII_SOCKET _SC_PII_SOCKET
#define ESPIPE 29
#define _IO_MAGIC_MASK 0xFFFF0000
#define _GLIBCXX_USE_WEAK_REF __GXX_WEAK__
#define __OFF_T_MATCHES_OFF64_T 1
#define EMLINK 31
#define _GLIBXX_STREAMBUF 1
#define __ORDER_LITTLE_ENDIAN__ 1234
#define __SIZE_MAX__ 0xffffffffffffffffUL
#define __stub_putmsg 
#define UINT_FAST16_WIDTH __WORDSIZE
#define _SC_UINT_MAX _SC_UINT_MAX
#define FLT_MAX_EXP __FLT_MAX_EXP__
#define _GLIBCXX_CLOCALE 1
#define __WCHAR_MAX__ 0x7fffffff
#define REGS_REALLOCATE 1
#define pthread_cleanup_push_defer_np(routine,arg) do { __pthread_cleanup_class __clframe (routine, arg); __clframe.__defer ()
#define EXPECT_STRCASENE(s1,s2) EXPECT_PRED_FORMAT2(::testing::internal::CmpHelperSTRCASENE, s1, s2)
#define _CXXABI_FORCED_H 1
#define ENOSR 63
#define INT16_MAX (32767)
#define _POSIX_MAPPED_FILES 200809L
#define __BLKCNT_T_TYPE __SYSCALL_SLONG_TYPE
#define _GLIBCXX_USE_LONG_LONG 1
#define __GCC_HAVE_SYNC_COMPARE_AND_SWAP_1 1
#define __GCC_HAVE_SYNC_COMPARE_AND_SWAP_2 1
#define __GCC_HAVE_SYNC_COMPARE_AND_SWAP_4 1
#define __DBL_DENORM_MIN__ double(4.94065645841246544176568792868221372e-324L)
#define __GCC_HAVE_SYNC_COMPARE_AND_SWAP_8 1
#define __GCC_ATOMIC_CHAR_LOCK_FREE 2
#define SIGTTOU 22
#define S_ISCHR(mode) __S_ISTYPE((mode), __S_IFCHR)
#define S_TYPEISSHM(buf) __S_TYPEISSHM(buf)
#define _SC_LEVEL3_CACHE_LINESIZE _SC_LEVEL3_CACHE_LINESIZE
#define __GCC_IEC_559 2
#define __cpp_lib_transformation_trait_aliases 201304
#define _IO_flockfile(_fp) 
#define __FLT32X_DECIMAL_DIG__ 17
#define RE_NO_BK_BRACES (RE_NEWLINE_ALT << 1)
#define _IOFBF 0
#define RE_SYNTAX_GNU_AWK ((RE_SYNTAX_POSIX_EXTENDED | RE_BACKSLASH_ESCAPE_IN_LISTS | RE_INVALID_INTERVAL_ORD) & ~(RE_DOT_NOT_NULL | RE_CONTEXT_INDEP_OPS | RE_CONTEXT_INVALID_OPS ))
#define _STL_SET_H 1
#define __FLT_EVAL_METHOD__ 0
#define GTEST_ENV_HAS_TR1_TUPLE_ 1
#define _SC_NPROCESSORS_CONF _SC_NPROCESSORS_CONF
#define _SC_SPORADIC_SERVER _SC_SPORADIC_SERVER
#define _IO_USER_LOCK 0x8000
#define _IO_NO_WRITES 8
#define _GLIBCXX_HAVE_FMODF 1
#define _GLIBCXX_PSEUDO_VISIBILITY(V) 
#define _GLIBCXX_HAVE_FMODL 1
#define __ASMNAME2(prefix,cname) __STRING (prefix) cname
#define _SC_SYSTEM_DATABASE _SC_SYSTEM_DATABASE
#define __unix__ 1
#define _GLIBCXX_HAVE_SYS_STAT_H 1
#define __cpp_binary_literals 201304
#define _CPP_TYPE_TRAITS_H 1
#define _GLIBCXX_HAVE_ENOTSUP 1
#define _SC_THREAD_PRIO_INHERIT _SC_THREAD_PRIO_INHERIT
#define _CS_POSIX_V7_ILP32_OFF32_CFLAGS _CS_POSIX_V7_ILP32_OFF32_CFLAGS
#define __FLT64_DECIMAL_DIG__ 17
#define _WINT_T 1
#define _GLIBCXX_SYNCHRONIZATION_HAPPENS_AFTER(A) 
#define _G_HAVE_ST_BLKSIZE defined (_STATBUF_ST_BLKSIZE)
#define _GLIBCXX_CXX_CONFIG_H 1
#define LC_CTYPE_MASK (1 << __LC_CTYPE)
#define RE_SYNTAX_POSIX_AWK (RE_SYNTAX_POSIX_EXTENDED | RE_BACKSLASH_ESCAPE_IN_LISTS | RE_INTERVALS | RE_NO_GNU_OPS | RE_INVALID_INTERVAL_ORD)
#define SCHED_RR 2
#define PTHREAD_BARRIER_SERIAL_THREAD -1
#define __stub_setlogin 
#define RE_CONTEXT_INDEP_OPS (RE_CONTEXT_INDEP_ANCHORS << 1)
#define _GLIBCXX_PACKAGE_NAME "package-unused"
#define __GCC_ATOMIC_CHAR32_T_LOCK_FREE 2
#define __SYSCALL_WORDSIZE 64
#define _IO_peekc_unlocked(_fp) (_IO_BE ((_fp)->_IO_read_ptr >= (_fp)->_IO_read_end, 0) && __underflow (_fp) == EOF ? EOF : *(unsigned char *) (_fp)->_IO_read_ptr)
#define __PDP_ENDIAN 3412
#define __FLOAT_WORD_ORDER __BYTE_ORDER
#define INT8_MIN (-128)
#define __x86_64 1
#define _GLIBCXX_INVOKE_H 1
#define _SC_LEVEL1_DCACHE_LINESIZE _SC_LEVEL1_DCACHE_LINESIZE
#define __ASSERT_VOID_CAST static_cast<void>
#define _SC_2_CHAR_TERM _SC_2_CHAR_TERM
#define CLONE_NEWNET 0x40000000
#define _CS_LFS_CFLAGS _CS_LFS_CFLAGS
#define _SC_TRACE_USER_EVENT_MAX _SC_TRACE_USER_EVENT_MAX
#define __CPU_COUNT_S(setsize,cpusetp) __sched_cpucount (setsize, cpusetp)
#define PTHREAD_PROCESS_SHARED PTHREAD_PROCESS_SHARED
#define __cpp_variadic_templates 200704
#define SCHED_DEADLINE 6
#define _POSIX_IPV6 200809L
#define _GLIBCXX_HAVE_CDTOR_CALLABI 0
#define RAND_MAX 2147483647
#define __attribute_nonstring__ 
#define EOWNERDEAD 130
#define _GLIBCXX_USE_C99_COMPLEX_TR1 1
#define __CPUMASK(cpu) ((__cpu_mask) 1 << ((cpu) % __NCPUBITS))
#define _SC_XOPEN_UNIX _SC_XOPEN_UNIX
#define RE_CONTEXT_INDEP_ANCHORS (RE_CHAR_CLASSES << 1)
#define __CFLOAT128 __cfloat128
#define GTEST_INCLUDE_GTEST_INTERNAL_CUSTOM_GTEST_PORT_H_ 
#define _SC_CPUTIME _SC_CPUTIME
#define __UINT_FAST64_MAX__ 0xffffffffffffffffUL
#define __SIG_ATOMIC_TYPE__ int
#define _CS_PATH _CS_PATH
#define _SC_TRACE_INHERIT _SC_TRACE_INHERIT
#define si_timerid _sifields._timer.si_tid
#define __COMPAR_FN_T 
#define __GID_T_TYPE __U32_TYPE
#define si_ptr _sifields._rt.si_sigval.sival_ptr
#define REG_RAX REG_RAX
#define _CS_POSIX_V7_LP64_OFF64_LDFLAGS _CS_POSIX_V7_LP64_OFF64_LDFLAGS
#define POLL_MSG POLL_MSG
#define RE_NO_BK_REFS (RE_NO_BK_PARENS << 1)
#define __DBL_MIN_10_EXP__ (-307)
#define INT16_MIN (-32767-1)
#define SCHED_IDLE 5
#define RE_INVALID_INTERVAL_ORD (RE_DEBUG << 1)
#define __FINITE_MATH_ONLY__ 0
#define si_overrun _sifields._timer.si_overrun
#define __id_t_defined 
#define __cpp_variable_templates 201304
#define _SC_PAGE_SIZE _SC_PAGESIZE
#define __cpp_lib_integral_constant_callable 201304
#define UINTMAX_C(c) c ## UL
#define _SC_SCHAR_MAX _SC_SCHAR_MAX
#define MOD_TIMECONST ADJ_TIMECONST
#define __attribute_alloc_size__(params) __attribute__ ((__alloc_size__ params))
#define _BITS_SIGEVENT_CONSTS_H 1
#define __sched_priority sched_priority
#define __cpp_lib_result_of_sfinae 201210
#define L_cuserid 9
#define INTPTR_MIN (-9223372036854775807L-1)
#define __U64_TYPE unsigned long int
#define _GLIBCXX_USE_WCHAR_T 1
#define EBADF 9
#define _SC_XBS5_ILP32_OFFBIG _SC_XBS5_ILP32_OFFBIG
#define __u_char_defined 
#define WIFEXITED(status) __WIFEXITED (status)
#define S_ISREG(mode) __S_ISTYPE((mode), __S_IFREG)
#define STA_PPSERROR 0x0800
#define __pid_t_defined 
#define _IO_UNIFIED_JUMPTABLES 1
#define _GLIBCXX_ALWAYS_INLINE inline __attribute__((__always_inline__))
#define ERANGE 34
#define __GNUC_PATCHLEVEL__ 0
#define _GLIBCXX_STD_A std
#define __FLT32_HAS_DENORM__ 1
#define _POSIX_NO_TRUNC 1
#define ECANCELED 125
#define _GLIBCXX_BEGIN_NAMESPACE_VERSION 
#define REGS_UNALLOCATED 0
#define _IO_pid_t __pid_t
#define __UINT_FAST8_MAX__ 0xff
#define __LEAF , __leaf__
#define GTEST_INIT_GOOGLE_TEST_NAME_ "testing::InitGoogleTest"
#define GTEST_INCLUDE_GTEST_GTEST_DEATH_TEST_H_ 
#define RE_TRANSLATE_TYPE __RE_TRANSLATE_TYPE
#define __LDBL_REDIR1(name,proto,alias) name proto
#define _BITS_TYPES_LOCALE_T_H 1
#define __has_include(STR) __has_include__(STR)
#define __size_t 
#define EOF (-1)
#define _GLIBCXX_HAVE_FREXPF 1
#define __LONG_LONG_PAIR(HI,LO) LO, HI
#define STA_RONLY (STA_PPSSIGNAL | STA_PPSJITTER | STA_PPSWANDER | STA_PPSERROR | STA_CLOCKERR | STA_NANO | STA_MODE | STA_CLK)
#define _GLIBCXX_HAVE_FREXPL 1
#define SI_USER SI_USER
#define INT_FAST8_MAX (127)
#define EXPECT_PRED5(pred,v1,v2,v3,v4,v5) GTEST_PRED5_(pred, v1, v2, v3, v4, v5, GTEST_NONFATAL_FAILURE_)
#define __DEC64_MAX_EXP__ 385
#define _WCHAR_T_DEFINED 
#define GTEST_EXECUTE_STATEMENT_(statement,regex) GTEST_AMBIGUOUS_ELSE_BLOCKER_ if (::testing::internal::AlwaysTrue()) { GTEST_SUPPRESS_UNREACHABLE_CODE_WARNING_BELOW_(statement); } else ::testing::Message()
#define _POSIX_V7_LP64_OFF64 1
#define _SC_XOPEN_CRYPT _SC_XOPEN_CRYPT
#define MOD_STATUS ADJ_STATUS
#define __ino64_t_defined 
#define ASSERT_STREQ(s1,s2) ASSERT_PRED_FORMAT2(::testing::internal::CmpHelperSTREQ, s1, s2)
#define _GLIBCXX_PURE __attribute__ ((__pure__))
#define ETXTBSY 26
#define ASSERT_GT(val1,val2) GTEST_ASSERT_GT(val1, val2)
#define __SI_SIGFAULT_ADDL 
#define _GLIBCXX_HAVE_STDINT_H 1
#define ENOMEM 12
#define __SIZEOF_PTHREAD_CONDATTR_T 4
#define __STDC_CONSTANT_MACROS 
#define _GLIBCXX_HAVE_ENOTRECOVERABLE 1
#define GTEST_NO_INLINE_ __attribute__((noinline))
#define __INT8_C(c) c
#define WCHAR_MAX __WCHAR_MAX
#define _MKNOD_VER 0
#define _GLIBCXX_HAVE_COSHF 1
#define SIGCONT 18
#define DBL_EPSILON __DBL_EPSILON__
#define _GLIBCXX_HAVE_COSHL 1
#define NSIG _NSIG
#define ENOLINK 67
#define __INT_LEAST8_WIDTH__ 8
#define __UINT_LEAST64_MAX__ 0xffffffffffffffffUL
#define _ERRNO_H 1
#define _SC_DEVICE_IO _SC_DEVICE_IO
#define _GLIBCXX_GUARD_WAITING_BIT __guard_test_bit (2, 1)
#define _IO_FLAGS2_MMAP 1
#define SI_TIMER SI_TIMER
#define ADJ_ESTERROR 0x0008
#define htobe16(x) __bswap_16 (x)
#define _XBS5_LPBIG_OFFBIG -1
#define _GLIBCXX_HAVE_ENDIAN_H 1
#define _SC_UIO_MAXIOV _SC_UIO_MAXIOV
#define __always_inline __inline __attribute__ ((__always_inline__))
#define _SC_PII_OSI_M _SC_PII_OSI_M
#define _CXXABI_H 1
#define NFDBITS __NFDBITS
#define __SHRT_MAX__ 0x7fff
#define __LDBL_MAX__ 1.18973149535723176502126385303097021e+4932L
#define ASSERT_FLOAT_EQ(val1,val2) ASSERT_PRED_FORMAT2(::testing::internal::CmpHelperFloatingPointEQ<float>, val1, val2)
#define _SC_PII_INTERNET_DGRAM _SC_PII_INTERNET_DGRAM
#define _GLIBCXX_USE_C99_COMPLEX _GLIBCXX11_USE_C99_COMPLEX
#define ASSERT_NEAR(val1,val2,abs_error) ASSERT_PRED_FORMAT3(::testing::internal::DoubleNearPredFormat, val1, val2, abs_error)
#define ILL_ILLOPC ILL_ILLOPC
#define CLONE_NEWUTS 0x04000000
#define REGISTER_TYPED_TEST_CASE_P(CaseName,...) namespace GTEST_CASE_NAMESPACE_(CaseName) { typedef ::testing::internal::Templates<__VA_ARGS__>::type gtest_AllTests_; } static const char* const GTEST_REGISTERED_TEST_NAMES_(CaseName) GTEST_ATTRIBUTE_UNUSED_ = GTEST_TYPED_TEST_CASE_P_STATE_(CaseName).VerifyRegisteredTestNames( __FILE__, __LINE__, #__VA_ARGS__)
#define _STREAMBUF_TCC 1
#define _SC_CHARCLASS_NAME_MAX _SC_CHARCLASS_NAME_MAX
#define _BASIC_IOS_H 1
#define __FLT64X_MAX_10_EXP__ 4932
#define S_IXOTH (S_IXGRP >> 3)
#define _TYPEINFO 
#define __cpp_lib_string_udls 201304
#define __ILP32_OFFBIG_CFLAGS "-m32 -D_LARGEFILE_SOURCE -D_FILE_OFFSET_BITS=64"
#define _GLIBCXX_CPU_DEFINES 1
#define _IO_need_lock(_fp) (((_fp)->_flags2 & _IO_FLAGS2_NEED_LOCK) != 0)
#define LDBL_MIN_EXP __LDBL_MIN_EXP__
#define WCHAR_WIDTH 32
#define _GLIBCXX_USE_C99_STDIO _GLIBCXX11_USE_C99_STDIO
#define __daddr_t_defined 
#define __CFLOAT32X _Complex double
#define __cpp_lib_is_final 201402L
#define STDERR_FILENO 2
#define _GLIBCXX_HAVE_STRUCT_DIRENT_D_TYPE 1
#define _GLIBCXX_SYSTEM_ERROR 1
#define _IO_EOF_SEEN 0x10
#define _IOS_BIN 128
#define __fortify_function __extern_always_inline __attribute_artificial__
#define _BASIC_IOS_TCC 1
#define ALLPERMS (S_ISUID|S_ISGID|S_ISVTX|S_IRWXU|S_IRWXG|S_IRWXO)
#define _LFS_ASYNCHRONOUS_IO 1
#define __mbstate_t_defined 1
#define _POSIX_JOB_CONTROL 1
#define __UINT_LEAST8_MAX__ 0xff
#define __GCC_ATOMIC_BOOL_LOCK_FREE 2
#define DBL_MANT_DIG __DBL_MANT_DIG__
#define _GLIBCXX_HAVE_EOWNERDEAD 1
#define _CS_XBS5_LPBIG_OFFBIG_LIBS _CS_XBS5_LPBIG_OFFBIG_LIBS
#define REG_NOTBOL 1
#define __FLT128_DENORM_MIN__ 6.47517511943802511092443895822764655e-4966F128
#define __ptr_t void *
#define _SC_V6_LP64_OFF64 _SC_V6_LP64_OFF64
#define _SC_JOB_CONTROL _SC_JOB_CONTROL
#define GTEST_ASSERT_LE(val1,val2) ASSERT_PRED_FORMAT2(::testing::internal::CmpHelperLE, val1, val2)
#define LDBL_MIN_10_EXP __LDBL_MIN_10_EXP__
#define _GLIBCXX_HAVE_MODF 1
#define _GLIBCXX_HAVE_ATANF 1
#define __USE_ISOCXX11 1
#define _SC_SYMLOOP_MAX _SC_SYMLOOP_MAX
#define _GLIBCXX_HAVE_ATANL 1
#define __UINTMAX_TYPE__ long unsigned int
#define EINPROGRESS 115
#define _CS_POSIX_V6_ILP32_OFFBIG_LINTFLAGS _CS_POSIX_V6_ILP32_OFFBIG_LINTFLAGS
#define __HAVE_DISTINCT_FLOAT32X 0
#define UINTPTR_WIDTH __WORDSIZE
#define _CS_GNU_LIBC_VERSION _CS_GNU_LIBC_VERSION
#define INT_FAST8_MIN (-128)
#define CLOCK_MONOTONIC_COARSE 6
#define _GLIBCXX_OS_DEFINES 1
#define _POSIX_THREAD_CPUTIME 0
#define __linux 1
#define __DEC32_EPSILON__ 1E-6DF
#define FLT_ROUNDS 1
#define CPU_OR_S(setsize,destset,srcset1,srcset2) __CPU_OP_S (setsize, destset, srcset1, srcset2, |)
#define WCHAR_MIN __WCHAR_MIN
#define _NSIG (__SIGRTMAX + 1)
#define __FLT_EVAL_METHOD_TS_18661_3__ 0
#define _BITS_TYPES_H 1
#define _IO_cleanup_region_end(_Doit) 
#define _STDIO_H 1
#define _GLIBCXX_HAVE_SYS_TIME_H 1
#define si_int _sifields._rt.si_sigval.sival_int
#define __blksize_t_defined 
#define _GLIBCXX_HAVE_LIBINTL_H 1
#define __unix 1
#define _OSTREAM_INSERT_H 1
#define RE_NO_EMPTY_RANGES (RE_NO_BK_VBAR << 1)
#define _BITS_PTHREADTYPES_ARCH_H 1
#define _GLIBCXX_USE_FCHMODAT 1
#define __UINT32_MAX__ 0xffffffffU
#define __GXX_EXPERIMENTAL_CXX0X__ 1
#define __UID_T_TYPE __U32_TYPE
#define _POSIX_TRACE_EVENT_FILTER -1
#define _POSIX_READER_WRITER_LOCKS 200809L
#define __sigstack_defined 1
#define CLONE_SIGHAND 0x00000800
#define _BITS_SIGINFO_CONSTS_ARCH_H 1
#define _GLIBCXX98_USE_C99_COMPLEX 1
#define __cpp_lib_void_t 201411
#define _GLIBCXX_SYMVER 1
#define __SIZE_T 
#define _GLIBCXX_FULLY_DYNAMIC_STRING 0
#define FD_SETSIZE __FD_SETSIZE
#define __S_ISVTX 01000
#define __LDBL_MAX_EXP__ 16384
#define _BITS_POSIX_OPT_H 1
#define GTEST_ATTRIBUTE_NO_SANITIZE_MEMORY_ 
#define _GLIBCXX_HAVE_STRTOLD 1
#define GTEST_EXECUTE_DEATH_TEST_STATEMENT_(statement,death_test) try { GTEST_SUPPRESS_UNREACHABLE_CODE_WARNING_BELOW_(statement); } catch (const ::std::exception& gtest_exception) { fprintf( stderr, "\n%s: Caught std::exception-derived exception escaping the " "death test statement. Exception message: %s\n", ::testing::internal::FormatFileLocation(__FILE__, __LINE__).c_str(), gtest_exception.what()); fflush(stderr); death_test->Abort(::testing::internal::DeathTest::TEST_THREW_EXCEPTION); } catch (...) { death_test->Abort(::testing::internal::DeathTest::TEST_THREW_EXCEPTION); }
#define _CS_V5_WIDTH_RESTRICTED_ENVS _CS_V5_WIDTH_RESTRICTED_ENVS
#define _SC_USHRT_MAX _SC_USHRT_MAX
#define _SC_DEVICE_SPECIFIC_R _SC_DEVICE_SPECIFIC_R
#define _ATFILE_SOURCE 1
#define CPU_ISSET(cpu,cpusetp) __CPU_ISSET_S (cpu, sizeof (cpu_set_t), cpusetp)
#define __glibcxx_assert(_Condition) 
#define __glibcxx_function_requires(...) 
#define UINT_FAST16_MAX (18446744073709551615UL)
#define _IO_ssize_t __ssize_t
#define _SC_NETWORKING _SC_NETWORKING
#define __FLT128_MIN_EXP__ (-16381)
#define GTEST_FLAG_PREFIX_DASH_ "gtest-"
#define RE_CARET_ANCHORS_HERE (RE_ICASE << 1)
#define _GLIBCXX_HAVE_LC_MESSAGES 1
#define __WINT_MIN__ 0U
#define UINT_FAST64_WIDTH 64
#define INT_FAST8_WIDTH 8
#define WUNTRACED 2
#define EPROTOTYPE 91
#define ERESTART 85
#define _SC_SHELL _SC_SHELL
#define _GLIBCXX_GUARD_PENDING_BIT __guard_test_bit (1, 1)
#define _HASH_BYTES_H 1
#define __SI_ASYNCIO_AFTER_SIGIO 1
#define EISNAM 120
#define _STREAMBUF_ITERATOR_H 1
#define __linux__ 1
#define UINT16_C(c) c
#define _GLIBCXX_HAVE_MEMALIGN 1
#define CLONE_SETTLS 0x00080000
#define MOD_CLKA ADJ_OFFSET_SINGLESHOT
#define MOD_CLKB ADJ_TICK
#define __FLT128_MIN_10_EXP__ (-4931)
#define _SC_MESSAGE_PASSING _SC_MESSAGE_PASSING
#define GTEST_HAS_PTHREAD (GTEST_OS_LINUX || GTEST_OS_MAC || GTEST_OS_HPUX || GTEST_OS_QNX || GTEST_OS_FREEBSD || GTEST_OS_NACL)
#define __S_IREAD 0400
#define _POSIX_RAW_SOCKETS 200809L
#define GTEST_INCLUDE_GTEST_INTERNAL_GTEST_TYPE_UTIL_H_ 
#define ENOMSG 42
#define _PC_PRIO_IO _PC_PRIO_IO
#define EXIT_FAILURE 1
#define WAIT_ANY (-1)
#define ADJ_MAXERROR 0x0004
#define __INT_LEAST16_WIDTH__ 16
#define _SIZE_T_DEFINED_ 
#define __LC_TIME 2
#define GTEST_ASSERT_GT(val1,val2) ASSERT_PRED_FORMAT2(::testing::internal::CmpHelperGT, val1, val2)
#define __glibcxx_requires_sorted_pred(_First,_Last,_Pred) 
#define __LDBL_REDIR_NTH(name,proto) name proto __THROW
#define __SCHAR_MAX__ 0x7f
#define EXPECT_STREQ(s1,s2) EXPECT_PRED_FORMAT2(::testing::internal::CmpHelperSTREQ, s1, s2)
#define _POSIX_SPIN_LOCKS 200809L
#define SIG_HOLD ((__sighandler_t) 2)
#define __FLT128_MANT_DIG__ 113
#define EALREADY 114
#define INT_LEAST64_MAX (__INT64_C(9223372036854775807))
#define CLONE_SYSVSEM 0x00040000
#define CLONE_THREAD 0x00010000
#define __WCHAR_MIN__ (-__WCHAR_MAX__ - 1)
#define GTEST_HAS_DEATH_TEST 1
#define SA_NOMASK SA_NODEFER
#define _SC_MB_LEN_MAX _SC_MB_LEN_MAX
#define SIGILL 4
#define _POSIX_SYNCHRONIZED_IO 200809L
#define __KERNEL_STRICT_NAMES 
#define _IO_stderr ((_IO_FILE*)(&_IO_2_1_stderr_))
#define _IO_ferror_unlocked(__fp) (((__fp)->_flags & _IO_ERR_SEEN) != 0)
#define GTEST_HAS_ALT_PATH_SEP_ 0
#define PTHREAD_COND_INITIALIZER { { {0}, {0}, {0, 0}, {0, 0}, 0, 0, {0, 0} } }
#define _FUNCTEXCEPT_H 1
#define __INT64_C(c) c ## L
#define ADJ_OFFSET_SINGLESHOT 0x8001
#define _SC_TIMERS _SC_TIMERS
#define __NTH(fct) __LEAF_ATTR fct throw ()
#define _GLIBCXX_CONST __attribute__ ((__const__))
#define __DBL_DIG__ 15
#define _GLIBCXX_IOS 1
#define _CS_XBS5_LP64_OFF64_LDFLAGS _CS_XBS5_LP64_OFF64_LDFLAGS
#define _EXT_NUMERIC_TRAITS 1
#define _SC_V7_LP64_OFF64 _SC_V7_LP64_OFF64
#define __glibcxx_digits10_b(T,B) (__glibcxx_digits_b (T,B) * 643L / 2136)
#define __GCC_ATOMIC_POINTER_LOCK_FREE 2
#define STA_PPSWANDER 0x0400
#define WSTOPPED 2
#define __FLT64X_MANT_DIG__ 64
#define _POSIX_TIMEOUTS 200809L
#define GTEST_INCLUDE_GTEST_INTERNAL_GTEST_LINKED_PTR_H_ 
#define _CS_LFS_LINTFLAGS _CS_LFS_LINTFLAGS
#define _IO_file_flags _flags
#define _GLIBCXX_HAVE_AT_QUICK_EXIT 1
#define _SC_BASE _SC_BASE
#define _BITS_TIMEX_H 1
#define GTEST_LANG_CXX11 1
#define __GLIBC_USE_IEC_60559_TYPES_EXT 1
#define _GLIBCXX_HAVE_LIMIT_FSIZE 1
#define _GLIBCXX_HAVE_ATAN2F 1
#define _GLIBCXX_HAVE_ATAN2L 1
#define DEFFILEMODE (S_IRUSR|S_IWUSR|S_IRGRP|S_IWGRP|S_IROTH|S_IWOTH)
#define __isascii(c) (((c) & ~0x7f) == 0)
#define __toascii(c) ((c) & 0x7f)
#define GTEST_HAS_STD_STRING 1
#define _STREAM_ITERATOR_H 1
#define _GLIBCXX_USE_SENDFILE 1
#define _POSIX_SOURCE 1
#define __SIZEOF_INT__ 4
#define S_IRWXO (S_IRWXG >> 3)
#define __SIZEOF_POINTER__ 8
#define S_IRWXU (__S_IREAD|__S_IWRITE|__S_IEXEC)
#define _SC_PII_OSI_CLTS _SC_PII_OSI_CLTS
#define _BITS_SIGINFO_ARCH_H 1
#define _DEFAULT_SOURCE 1
#define GTEST_FLAG(name) FLAGS_gtest_ ##name
#define MOD_OFFSET ADJ_OFFSET
#define GTEST_ATTRIBUTE_NO_SANITIZE_THREAD_ 
#define L_INCR SEEK_CUR
#define _SC_XOPEN_SHM _SC_XOPEN_SHM
#define __attribute_used__ __attribute__ ((__used__))
#define _STDIO_USES_IOSTREAM 
#define CLOCK_REALTIME 0
#define __LOCK_ALIGNMENT 
#define __GCC_ATOMIC_CHAR16_T_LOCK_FREE 2
#define ASSERT_DEBUG_DEATH(statement,regex) ASSERT_DEATH(statement, regex)
#define _XOPEN_REALTIME_THREADS 1
#define _GLIBCXX_HAVE_EBADMSG 1
#define ETIMEDOUT 110
#define _IO_IS_FILEBUF 0x2000
#define _SC_VERSION _SC_VERSION
#define _SC_AIO_LISTIO_MAX _SC_AIO_LISTIO_MAX
#define __wint_t_defined 1
#define RE_SYNTAX_POSIX_BASIC (_RE_SYNTAX_POSIX_COMMON | RE_BK_PLUS_QM | RE_CONTEXT_INVALID_DUP)
#define __USER_LABEL_PREFIX__ 
#define _GLIBCXX_NAMESPACE_CXX11 __cxx11::
#define SCHED_OTHER 0
#define __SI_ERRNO_THEN_CODE 1
#define CLOCK_REALTIME_ALARM 8
#define _GLIBCXX_NUM_FACETS 28
#define __glibc_macro_warning(message) __glibc_macro_warning1 (GCC warning message)
#define ENODATA 61
#define _GLIBCXX_HAVE_TANL 1
#define _GLIBCXX_USE_PTHREAD_RWLOCK_T 1
#define SIG_SETMASK 2
#define _GLIBCXX_USE_C99_FENV_TR1 1
#define __LC_MEASUREMENT 11
#define _GLIBCXX_ITERATOR 1
#define _POSIX_TRACE -1
#define GTEST_REMOVE_REFERENCE_AND_CONST_(T) GTEST_REMOVE_CONST_(GTEST_REMOVE_REFERENCE_(T))
#define _LFS64_LARGEFILE 1
#define _SC_BC_SCALE_MAX _SC_BC_SCALE_MAX
#define __GLIBC__ 2
#define _SC_POLL _SC_POLL
#define PTHREAD_CANCEL_DEFERRED PTHREAD_CANCEL_DEFERRED
#define _GLIBCXX_USE_C11_UCHAR_CXX11 1
#define __END_DECLS }
#define _POSIX_ASYNC_IO 1
#define __FLT64X_EPSILON__ 1.08420217248550443400745280086994171e-19F64x
#define _POSIX_V6_LPBIG_OFFBIG -1
#define __CONCAT(x,y) x ## y
#define _SC_RTSIG_MAX _SC_RTSIG_MAX
#define WCONTINUED 8
#define UINT8_MAX (255)
#define _MKNOD_VER_LINUX 0
#define __STDC_HOSTED__ 1
#define _SC_GETPW_R_SIZE_MAX _SC_GETPW_R_SIZE_MAX
#define _ALLOCA_H 1
#define TYPED_TEST_P(CaseName,TestName) namespace GTEST_CASE_NAMESPACE_(CaseName) { template <typename gtest_TypeParam_> class TestName : public CaseName<gtest_TypeParam_> { private: typedef CaseName<gtest_TypeParam_> TestFixture; typedef gtest_TypeParam_ TypeParam; virtual void TestBody(); }; static bool gtest_ ##TestName ##_defined_ GTEST_ATTRIBUTE_UNUSED_ = GTEST_TYPED_TEST_CASE_P_STATE_(CaseName).AddTestName( __FILE__, __LINE__, #CaseName, #TestName); } template <typename gtest_TypeParam_> void GTEST_CASE_NAMESPACE_(CaseName)::TestName<gtest_TypeParam_>::TestBody()
#define __LDBL_HAS_INFINITY__ 1
#define __SLONG32_TYPE int
#define _BITS_UINTN_IDENTITY_H 1
#define _GLIBCXX_DEBUG_ASSERTIONS_H 1
#define S_IRUSR __S_IREAD
#define _IO_LINE_BUF 0x200
#define __SYSMACROS_DM1(...) __glibc_macro_warning (#__VA_ARGS__)
#define GTEST_REGISTERED_TEST_NAMES_(TestCaseName) gtest_registered_test_names_ ##TestCaseName ##_
#define _IOS_NOCREATE 32
#define _XOPEN_ENH_I18N 1
#define GTEST_ENV_HAS_STD_TUPLE_ 1
#define GTEST_FLAG_PREFIX_UPPER_ "GTEST_"
#define si_lower _sifields._sigfault._bounds._addr_bnd._lower
#define _SC_LONG_BIT _SC_LONG_BIT
#define __EXCEPTION_H 1
#define _PC_PIPE_BUF _PC_PIPE_BUF
#define UINT64_MAX (__UINT64_C(18446744073709551615))
#define GTEST_REFERENCE_TO_CONST_(T) GTEST_ADD_REFERENCE_(const GTEST_REMOVE_REFERENCE_(T))
#define TMP_MAX 238328
#define _GLIBCXX_HAVE_LIMIT_VMEM 0
#define __EXCEPTION__ 
#define SIGURG 23
#define _SC_TZNAME_MAX _SC_TZNAME_MAX
#define _CS_POSIX_V6_LPBIG_OFFBIG_LDFLAGS _CS_POSIX_V6_LPBIG_OFFBIG_LDFLAGS
#define S_TYPEISMQ(buf) __S_TYPEISMQ(buf)
#define ENOTTY 25
#define __GNU_LIBRARY__ 6
#define TIME_UTC 1
#define __FLT32_DIG__ 6
#define EINTR 4
#define _CS_POSIX_V7_LPBIG_OFFBIG_CFLAGS _CS_POSIX_V7_LPBIG_OFFBIG_CFLAGS
#define __RLIM_T_MATCHES_RLIM64_T 1
#define _SC_TRACE_EVENT_FILTER _SC_TRACE_EVENT_FILTER
#define _G_BUFSIZ 8192
#define __FLT_EPSILON__ 1.19209289550781250000000000000000000e-7F
#define INT32_MIN (-2147483647-1)
#define __CPUELT(cpu) ((cpu) / __NCPUBITS)
#define SS_DISABLE SS_DISABLE
#define RE_CONTEXT_INVALID_OPS (RE_CONTEXT_INDEP_OPS << 1)
#define _GLIBCXX_C_LOCALE_GNU 1
#define STA_CLOCKERR 0x1000
#define REG_CR2 REG_CR2
#define __GXX_WEAK__ 1
#define _SC_TIMER_MAX _SC_TIMER_MAX
#define LC_CTYPE __LC_CTYPE
#define _GLIBCXX_HAVE_ISNANF 1
#define __SHRT_WIDTH__ 16
#define _GLIBCXX_HAVE_ISNANL 1
#define __f64x(x) x ##l
#define EXPECT_ANY_THROW(statement) GTEST_TEST_ANY_THROW_(statement, GTEST_NONFATAL_FAILURE_)
#define ILL_COPROC ILL_COPROC
#define GTEST_DECLARE_STATIC_MUTEX_(mutex) extern ::testing::internal::MutexBase mutex
#define si_status _sifields._sigchld.si_status
#define __SSIZE_T_TYPE __SWORD_TYPE
#define PTHREAD_CANCEL_ASYNCHRONOUS PTHREAD_CANCEL_ASYNCHRONOUS
#define __DEV_T_TYPE __UQUAD_TYPE
#define S_IWGRP (S_IWUSR >> 3)
#define _GLIBCXX_ALGORITHMFWD_H 1
#define _GLIBCXX_BEGIN_NAMESPACE_ALGO _GLIBCXX_BEGIN_NAMESPACE_VERSION
#define _GLIBCXX_CXA_VEC_CTOR_RETURN(x) return
#define __SI_MAX_SIZE 128
#define SIGIO SIGPOLL
#define ENOTCONN 107
#define _IO_SCIENTIFIC 04000
#define ILL_ILLADR ILL_ILLADR
#define __LDBL_MIN__ 3.36210314311209350626267781732175260e-4932L
#define GTEST_CHECK_POSIX_SUCCESS_(posix_call) if (const int gtest_error = (posix_call)) GTEST_LOG_(FATAL) << #posix_call << "failed with error " << gtest_error
#define TEST(test_case_name,test_name) GTEST_TEST(test_case_name, test_name)
#define __nonnull(params) __attribute__ ((__nonnull__ params))
#define __GTHREADS_CXX0X 1
#define le32toh(x) __uint32_identity (x)
#define _PC_REC_INCR_XFER_SIZE _PC_REC_INCR_XFER_SIZE
#define __DEC32_MAX__ 9.999999E96DF
#define __GLIBC_USE_IEC_60559_BFP_EXT 1
#define INT_FAST64_MAX (__INT64_C(9223372036854775807))
#define _SC_THREAD_SAFE_FUNCTIONS _SC_THREAD_SAFE_FUNCTIONS
#define _GLIBCXX_HAVE_POLL 1
#define CPU_SETSIZE __CPU_SETSIZE
#define _SC_UCHAR_MAX _SC_UCHAR_MAX
#define _POSIX_FSYNC 200809L
#define __cpp_threadsafe_static_init 200806
#define _IO_SHOWPOS 02000
#define __u_intN_t(N,MODE) typedef unsigned int u_int ##N ##_t __attribute__ ((__mode__ (MODE)))
#define _GLIBCXX_USE_CLOCK_REALTIME 1
#define SI_MESGQ SI_MESGQ
#define _SC_THREAD_PROCESS_SHARED _SC_THREAD_PROCESS_SHARED
#define _GLIBCXX_ISTREAM 1
#define __WCOREDUMP(status) ((status) & __WCOREFLAG)
#define _WCHAR_T_ 
#define _POSIX_THREAD_ATTR_STACKSIZE 200809L
#define __FLT64X_DENORM_MIN__ 3.64519953188247460252840593361941982e-4951F64x
#define _SC_REALTIME_SIGNALS _SC_REALTIME_SIGNALS
#define _GLIBCXX_CXX_ALLOCATOR_H 1
#define LC_ADDRESS_MASK (1 << __LC_ADDRESS)
#define _GLIBCXX_STDLIB_H 1
#define _STDDEF_H 
#define _SC_MEMLOCK _SC_MEMLOCK
#define _SC_PII_OSI _SC_PII_OSI
#define EL2NSYNC 45
#define _GLIBCXX_USE_C99_STDLIB _GLIBCXX11_USE_C99_STDLIB
#define __FLT32X_HAS_INFINITY__ 1
#define UINT32_WIDTH 32
#define __INT32_MAX__ 0x7fffffff
#define _GLIBCXX_BEGIN_EXTERN_C extern "C" {
#define _GLIBCXX_ATOMIC_WORD_H 1
#define REG_EFL REG_EFL
#define __glibcxx_digits_b(T,B) (B - __glibcxx_signed_b (T,B))
#define _CS_POSIX_V6_ILP32_OFFBIG_LDFLAGS _CS_POSIX_V6_ILP32_OFFBIG_LDFLAGS
#define __GLIBC_USE_DEPRECATED_GETS 0
#define __SIZEOF_PTHREAD_COND_T 48
#define htobe64(x) __bswap_64 (x)
#define _POSIX_THREAD_SAFE_FUNCTIONS 200809L
#define _POSIX_V7_LPBIG_OFFBIG -1
#define __INT_WIDTH__ 32
#define __SIZEOF_LONG__ 8
#define _IONBF 2
#define __S_IFCHR 0020000
#define LDBL_DIG __LDBL_DIG__
#define _IO_getc_unlocked(_fp) (_IO_BE ((_fp)->_IO_read_ptr >= (_fp)->_IO_read_end, 0) ? __uflow (_fp) : *(unsigned char *) (_fp)->_IO_read_ptr++)
#define _SYS_UCONTEXT_H 1
#define GTEST_AMBIGUOUS_ELSE_BLOCKER_ switch (0) case 0: default:
#define __STDC_IEC_559__ 1
#define SIGEV_NONE SIGEV_NONE
#define GTEST_CHECK_(condition) GTEST_AMBIGUOUS_ELSE_BLOCKER_ if (::testing::internal::IsTrue(condition)) ; else GTEST_LOG_(FATAL) << "Condition " #condition " failed. "
#define __STDC_ISO_10646__ 201706L
#define MOD_MICRO ADJ_MICRO
#define _CS_POSIX_V6_LP64_OFF64_LIBS _CS_POSIX_V6_LP64_OFF64_LIBS
#define _SC_NL_NMAX _SC_NL_NMAX
#define EMFILE 24
#define __UINT16_C(c) c
#define __PTRDIFF_WIDTH__ 64
#define _SC_C_LANG_SUPPORT_R _SC_C_LANG_SUPPORT_R
#define STA_DEL 0x0020
#define __SIZEOF_PTHREAD_BARRIER_T 32
#define _GLIBCXX_HAVE_MODFF 1
#define _GLIBCXX_HAVE_MODFL 1
#define __DECIMAL_DIG__ 21
#define _BITS_ERRNO_H 1
#define __NTHNL(fct) fct throw ()
#define __USE_FORTIFY_LEVEL 0
#define __PTHREAD_MUTEX_NUSERS_AFTER_KIND 0
#define EXPECT_EXIT(statement,predicate,regex) GTEST_DEATH_TEST_(statement, predicate, regex, GTEST_NONFATAL_FAILURE_)
#define TYPED_TEST_CASE_P(CaseName) static ::testing::internal::TypedTestCasePState GTEST_TYPED_TEST_CASE_P_STATE_(CaseName)
#define CPU_EQUAL(cpusetp1,cpusetp2) __CPU_EQUAL_S (sizeof (cpu_set_t), cpusetp1, cpusetp2)
#define STA_FREQHOLD 0x0080
#define __FLT64_EPSILON__ 2.22044604925031308084726333618164062e-16F64
#define _GLIBCXX_HAVE_HYPOTF 1
#define _GLIBCXX_HAVE_HYPOTL 1
#define _GLIBCXX_HAVE_SYS_UIO_H 1
#define __gnu_linux__ 1
#define _SC_T_IOV_MAX _SC_T_IOV_MAX
#define __INTMAX_WIDTH__ 64
#define _SC_TIMEOUTS _SC_TIMEOUTS
#define _LARGEFILE_SOURCE 1
#define _GLIBCXX_GCC_GTHR_POSIX_H 
#define GTEST_INCLUDE_GTEST_INTERNAL_GTEST_PARAM_UTIL_GENERATED_H_ 
#define __FLT64_MIN_EXP__ (-1021)
#define INT_FAST16_MAX (9223372036854775807L)
#define __glibcxx_requires_heap(_First,_Last) 
#define ENETDOWN 100
#define __HAVE_DISTINCT_FLOAT32 0
#define ESTALE 116
#define GTEST_DEFINE_int32_(name,default_val,doc) GTEST_API_ ::testing::internal::Int32 GTEST_FLAG(name) = (default_val)
#define __attribute_warn_unused_result__ __attribute__ ((__warn_unused_result__))
#define __has_include_next(STR) __has_include_next__(STR)
#define _GLIBCXX_USE_LFS 1
#define LC_TIME __LC_TIME
#define _PC_FILESIZEBITS _PC_FILESIZEBITS
#define __HAVE_FLOAT64X_LONG_DOUBLE 1
#define GTEST_PROJECT_URL_ "https://github.com/google/googletest/"
#define DBL_MIN_EXP __DBL_MIN_EXP__
#define __S_IFIFO 0010000
#define _GLIBCXX_HAVE_SYMVER_SYMBOL_RENAMING_RUNTIME_SUPPORT 1
#define __UQUAD_TYPE unsigned long int
#define _CS_POSIX_V7_LP64_OFF64_LINTFLAGS _CS_POSIX_V7_LP64_OFF64_LINTFLAGS
#define ____sigset_t_defined 
#define _IO_LEFT 02
#define __glibcxx_requires_cond(_Cond,_Msg) 
#define TYPED_TEST(CaseName,TestName) template <typename gtest_TypeParam_> class GTEST_TEST_CLASS_NAME_(CaseName, TestName) : public CaseName<gtest_TypeParam_> { private: typedef CaseName<gtest_TypeParam_> TestFixture; typedef gtest_TypeParam_ TypeParam; virtual void TestBody(); }; bool gtest_ ##CaseName ##_ ##TestName ##_registered_ GTEST_ATTRIBUTE_UNUSED_ = ::testing::internal::TypeParameterizedTest< CaseName, ::testing::internal::TemplateSel< GTEST_TEST_CLASS_NAME_(CaseName, TestName)>, GTEST_TYPE_PARAMS_(CaseName)>::Register( "", ::testing::internal::CodeLocation(__FILE__, __LINE__), #CaseName, #TestName, 0); template <typename gtest_TypeParam_> void GTEST_TEST_CLASS_NAME_(CaseName, TestName)<gtest_TypeParam_>::TestBody()
#define _BITS_SS_FLAGS_H 1
#define _NEW_ALLOCATOR_H 1
#define _CS_V6_ENV _CS_V6_ENV
#define __FLT64X_MIN_10_EXP__ (-4931)
#define _POSIX_THREAD_ROBUST_PRIO_PROTECT -1
#define F_OK 0
#define _GLIBCXX_HAVE_LOCALE_H 1
#define __glibcxx_max_b(T,B) (__glibcxx_signed_b (T,B) ? (((((T)1 << (__glibcxx_digits_b (T,B) - 1)) - 1) << 1) + 1) : ~(T)0)
#define __LDBL_HAS_QUIET_NAN__ 1
#define INT64_C(c) c ## L
#define __USE_ISOC11 1
#define S_ISDIR(mode) __S_ISTYPE((mode), __S_IFDIR)
#define __HAVE_FLOAT64X 1
#define _GLIBCXX_CSTDIO 1
#define ADJ_MICRO 0x1000
#define _GLIBCXX_HAVE_FABSF 1
#define _GLIBCXX_HAVE_FABSL 1
#define SIGRTMAX (__libc_current_sigrtmax ())
#define _CS_POSIX_V7_LPBIG_OFFBIG_LDFLAGS _CS_POSIX_V7_LPBIG_OFFBIG_LDFLAGS
#define __FLT64_MANT_DIG__ 53
#define _GLIBCXX_STDEXCEPT 1
#define __attribute_const__ __attribute__ ((__const__))
#define _SC_SCHAR_MIN _SC_SCHAR_MIN
#define REG_ERR REG_ERR
#define L_XTND SEEK_END
#define _PC_PATH_MAX _PC_PATH_MAX
#define __THROW throw ()
#define _SC_XOPEN_VERSION _SC_XOPEN_VERSION
#define _POSIX_REGEXP 1
#define __glibcxx_requires_heap_pred(_First,_Last,_Pred) 
#define EXPECT_NO_THROW(statement) GTEST_TEST_NO_THROW_(statement, GTEST_NONFATAL_FAILURE_)
#define _GLIBCXX_HAVE_POWF 1
#define _GLIBCXX_HAVE_POWL 1
#define __USE_GNU_GETTEXT 1
#define SIGIOT SIGABRT
#define _POSIX_CPUTIME 0
#define _RE_SYNTAX_POSIX_COMMON (RE_CHAR_CLASSES | RE_DOT_NEWLINE | RE_DOT_NOT_NULL | RE_INTERVALS | RE_NO_EMPTY_RANGES)
#define L_tmpnam 20
#define ___int_wchar_t_h 
#define UINT8_C(c) c
#define WIFCONTINUED(status) __WIFCONTINUED (status)
#define _T_PTRDIFF 
#define ASSERT_THROW(statement,expected_exception) GTEST_TEST_THROW_(statement, expected_exception, GTEST_FATAL_FAILURE_)
#define __GNUC__ 7
#define __SYSCALL_ULONG_TYPE __ULONGWORD_TYPE
#define __cpp_lib_is_null_pointer 201309
#define SIGWINCH 28
#define _CS_XBS5_LP64_OFF64_CFLAGS _CS_XBS5_LP64_OFF64_CFLAGS
#define __GXX_RTTI 1
#define CLD_EXITED CLD_EXITED
#define __pie__ 2
#define __MMX__ 1
#define CLONE_FILES 0x00000400
#define GTEST_DISABLE_MSC_WARNINGS_POP_() 
#define SUCCEED() GTEST_SUCCEED()
#define __cpp_delegating_constructors 200604
#define __timespec_defined 1
#define GTEST_INCLUDE_GTEST_GTEST_MESSAGE_H_ 
#define _GLIBCXX_USE_GET_NPROCS 1
#define __OFF64_T_TYPE __SQUAD_TYPE
#define offsetof(TYPE,MEMBER) __builtin_offsetof (TYPE, MEMBER)
#define _GLIBCXX98_USE_C99_STDLIB 1
#define _GLIBCXX_HAVE_STRERROR_R 1
#define FD_SET(fd,fdsetp) __FD_SET (fd, fdsetp)
#define __FLT_HAS_DENORM__ 1
#define __SIZEOF_LONG_DOUBLE__ 16
#define EXPECT_EQ(val1,val2) EXPECT_PRED_FORMAT2(::testing::internal:: EqHelper<GTEST_IS_NULL_LITERAL_(val1)>::Compare, val1, val2)
#define GTEST_TEST_NO_THROW_(statement,fail) GTEST_AMBIGUOUS_ELSE_BLOCKER_ if (::testing::internal::AlwaysTrue()) { try { GTEST_SUPPRESS_UNREACHABLE_CODE_WARNING_BELOW_(statement); } catch (...) { goto GTEST_CONCAT_TOKEN_(gtest_label_testnothrow_, __LINE__); } } else GTEST_CONCAT_TOKEN_(gtest_label_testnothrow_, __LINE__): fail("Expected: " #statement " doesn't throw an exception.\n" "  Actual: it throws.")
#define REG_RCX REG_RCX
#define _GLIBCXX_HAVE_EWOULDBLOCK 1
#define _SC_ARG_MAX _SC_ARG_MAX
#define INT8_WIDTH 8
#define EXPECT_DEATH_IF_SUPPORTED(statement,regex) EXPECT_DEATH(statement, regex)
#define ESOCKTNOSUPPORT 94
#define __timeval_defined 1
#define GTEST_CAN_STREAM_RESULTS_ 1
#define INT_FAST16_MIN (-9223372036854775807L-1)
#define _GLIBCXX_HAVE_SYS_SYSINFO_H 1
#define GTEST_TEST_ANY_THROW_(statement,fail) GTEST_AMBIGUOUS_ELSE_BLOCKER_ if (::testing::internal::AlwaysTrue()) { bool gtest_caught_any = false; try { GTEST_SUPPRESS_UNREACHABLE_CODE_WARNING_BELOW_(statement); } catch (...) { gtest_caught_any = true; } if (!gtest_caught_any) { goto GTEST_CONCAT_TOKEN_(gtest_label_testanythrow_, __LINE__); } } else GTEST_CONCAT_TOKEN_(gtest_label_testanythrow_, __LINE__): fail("Expected: " #statement " throws an exception.\n" "  Actual: it doesn't.")
#define _GLIBCXX_GTHREAD_USE_WEAK 1
#define _SC_TRACE_NAME_MAX _SC_TRACE_NAME_MAX
#define _GLIBCXX_HAVE_POSIX_MEMALIGN 1
#define GTEST_REMOVE_CONST_(T) typename ::testing::internal::RemoveConst<T>::type
#define _GLIBCXX_RES_LIMITS 1
#define _SC_USER_GROUPS_R _SC_USER_GROUPS_R
#define __PTHREAD_SPINS_DATA short __spins; short __elision
#define __LDBL_REDIR1_NTH(name,proto,alias) name proto __THROW
#define ELNRNG 48
#define __BIGGEST_ALIGNMENT__ 16
#define _SC_BC_STRING_MAX _SC_BC_STRING_MAX
#define PTRDIFF_MIN (-9223372036854775807L-1)
#define __STDC_UTF_16__ 1
#define RE_DEBUG (RE_NO_GNU_OPS << 1)
#define sa_handler __sigaction_handler.sa_handler
#define _POSIX_THREAD_PRIORITY_SCHEDULING 200809L
#define __ONCE_ALIGNMENT 
#define _GLIBCXX_POSTYPES_H 1
#define _SC_THREAD_DESTRUCTOR_ITERATIONS _SC_THREAD_DESTRUCTOR_ITERATIONS
#define __SI_ALIGNMENT 
#define __FLT64_MAX_10_EXP__ 308
#define _STL_CONSTRUCT_H 1
#define _POSIX_CHOWN_RESTRICTED 0
#define _GLIBCXX_NAMESPACE_LDBL 
#define __USE_ISOC95 1
#define _TIME_H 1
#define __USE_ISOC99 1
#define REG_OLDMASK REG_OLDMASK
#define __ASMNAME(cname) __ASMNAME2 (__USER_LABEL_PREFIX__, cname)
#define __sigevent_t_defined 1
#define _GLIBCXX_HAVE_WCHAR_H 1
#define EXPECT_DEATH(statement,regex) EXPECT_EXIT(statement, ::testing::internal::ExitedUnsuccessfully, regex)
#define WTERMSIG(status) __WTERMSIG (status)
#define _GLIBCXX_HAVE_S_ISREG 1
#define __CPU_SET_S(cpu,setsize,cpusetp) (__extension__ ({ size_t __cpu = (cpu); __cpu / 8 < (setsize) ? (((__cpu_mask *) ((cpusetp)->__bits))[__CPUELT (__cpu)] |= __CPUMASK (__cpu)) : 0; }))
#define __GTHREAD_RECURSIVE_MUTEX_INIT PTHREAD_RECURSIVE_MUTEX_INITIALIZER_NP
#define _GLIBCXX_LOCALE 1
#define __CLOCKID_T_TYPE __S32_TYPE
#define ASSERT_DOUBLE_EQ(val1,val2) ASSERT_PRED_FORMAT2(::testing::internal::CmpHelperFloatingPointEQ<double>, val1, val2)
#define __FLT32_HAS_INFINITY__ 1
#define _GCC_WCHAR_T 
#define __isctype_l(c,type,locale) ((locale)->__ctype_b[(int) (c)] & (unsigned short int) type)
#define GTEST_FLAG_PREFIX_ "gtest_"
#define _GLIBCXX_PREDEFINED_OPS_H 1
#define __DBL_MAX__ double(1.79769313486231570814527423731704357e+308L)
#define ASSERT_LT(val1,val2) GTEST_ASSERT_LT(val1, val2)
#define _BITS_STDINT_UINTN_H 1
#define GTEST_DISALLOW_COPY_AND_ASSIGN_(type) type(type const &); GTEST_DISALLOW_ASSIGN_(type)
#define _CS_XBS5_LPBIG_OFFBIG_CFLAGS _CS_XBS5_LPBIG_OFFBIG_CFLAGS
#define SI_QUEUE SI_QUEUE
#define UINTMAX_WIDTH 64
#define sched_priority sched_priority
#define _CS_LFS64_CFLAGS _CS_LFS64_CFLAGS
#define _GLIBCXX_HAVE_GETS 1
#define _GLIBCXX_STDIO_SEEK_CUR 1
#define INT32_WIDTH 32
#define __glibcxx_requires_irreflexive2(_First,_Last) 
#define __cpp_raw_strings 200710
#define __INT_FAST32_MAX__ 0x7fffffffffffffffL
#define _GLIBCXX_HAVE_EPROTO 1
#define _CS_XBS5_ILP32_OFFBIG_CFLAGS _CS_XBS5_ILP32_OFFBIG_CFLAGS
#define GTEST_FATAL_FAILURE_(message) return GTEST_MESSAGE_(message, ::testing::TestPartResult::kFatalFailure)
#define __DBL_HAS_INFINITY__ 1
#define GTEST_PRED_FORMAT4_(pred_format,v1,v2,v3,v4,on_failure) GTEST_ASSERT_(pred_format(#v1, #v2, #v3, #v4, v1, v2, v3, v4), on_failure)
#define _GLIBCXX_STDIO_EOF -1
#define __SIZEOF_PTHREAD_MUTEX_T 40
#define _PC_SYMLINK_MAX _PC_SYMLINK_MAX
#define WEOF (0xffffffffu)
#define __GLIBC_USE_IEC_60559_FUNCS_EXT 1
#define __NGREG 23
#define _CS_LFS_LDFLAGS _CS_LFS_LDFLAGS
#define _FLOAT_H___ 
#define _CS_XBS5_ILP32_OFF32_CFLAGS _CS_XBS5_ILP32_OFF32_CFLAGS
#define _PC_CHOWN_RESTRICTED _PC_CHOWN_RESTRICTED
#define _SC_NL_TEXTMAX _SC_NL_TEXTMAX
#define _GLIBCXX_NAMESPACE_LDBL_OR_CXX11 _GLIBCXX_NAMESPACE_CXX11
#define __USE_LARGEFILE 1
#define _GLIBCXX_HAVE_ETXTBSY 1
#define __INT64_MAX__ 0x7fffffffffffffffL
#define __glibcxx_requires_subscript(_N) 
#define _GLIBCXX_END_NAMESPACE_ALGO _GLIBCXX_END_NAMESPACE_VERSION
#define __USE_XOPEN 1
#define __SIZEOF_PTHREAD_RWLOCK_T 56
#define __cpp_lib_uncaught_exceptions 201411
#define SIGVTALRM 26
#define CLONE_NEWNS 0x00020000
#define si_syscall _sifields._sigsys._syscall
#define __USE_XOPEN2K 1
#define GTEST_EXCLUSIVE_LOCK_REQUIRED_(locks) 
#define _BITS_SIGCONTEXT_H 1
#define _GLIBCXX_QUOTED_STRING_H 1
#define _GLIBCXX_ERROR_CONSTANTS 1
#define _SC_CLK_TCK _SC_CLK_TCK
#define __lldiv_t_defined 1
#define __DEC32_MIN_EXP__ (-94)
#define __glibcxx_requires_partitioned_upper(_First,_Last,_Value) 
#define SCHED_FIFO 1
#define _CS_LFS64_LIBS _CS_LFS64_LIBS
#define __DADDR_T_TYPE __S32_TYPE
#define _GLIBCXX_HAVE_UCHAR_H 1
#define SIG_BLOCK 0
#define _SC_EQUIV_CLASS_MAX _SC_EQUIV_CLASS_MAX
#define __INTPTR_WIDTH__ 64
#define EPIPE 32
#define _ISOC11_SOURCE 1
#define _POSIX_THREAD_SPORADIC_SERVER -1
#define _SC_SYSTEM_DATABASE_R _SC_SYSTEM_DATABASE_R
#define _GLIBCXX_HAVE_LINUX_FUTEX 1
#define GTEST_PRED5_(pred,v1,v2,v3,v4,v5,on_failure) GTEST_ASSERT_(::testing::AssertPred5Helper(#pred, #v1, #v2, #v3, #v4, #v5, pred, v1, v2, v3, v4, v5), on_failure)
#define _GLIBCXX_HAVE_QUICK_EXIT 1
#define ASSERT_PRED_FORMAT2(pred_format,v1,v2) GTEST_PRED_FORMAT2_(pred_format, v1, v2, GTEST_FATAL_FAILURE_)
#define DBL_MIN_10_EXP __DBL_MIN_10_EXP__
#define __cpp_lib_allocator_traits_is_always_equal 201411
#define _GLIBCXX_HAVE_SINCOSF 1
#define CPU_COUNT_S(setsize,cpusetp) __CPU_COUNT_S (setsize, cpusetp)
#define __GLIBC_USE(F) __GLIBC_USE_ ## F
#define _GLIBCXX_HAVE_SINCOSL 1
#define __FD_SETSIZE 1024
#define GTEST_PRED2_(pred,v1,v2,on_failure) GTEST_ASSERT_(::testing::AssertPred2Helper(#pred, #v1, #v2, pred, v1, v2), on_failure)
#define be32toh(x) __bswap_32 (x)
#define REG_RSI REG_RSI
#define GTEST_COMPILE_ASSERT_(expr,msg) static_assert(expr, #msg)
#define RE_ICASE (RE_INVALID_INTERVAL_ORD << 1)
#define __attribute_format_strfmon__(a,b) __attribute__ ((__format__ (__strfmon__, a, b)))
#define _IO_MAGIC 0xFBAD0000
#define RE_SYNTAX_AWK (RE_BACKSLASH_ESCAPE_IN_LISTS | RE_DOT_NOT_NULL | RE_NO_BK_PARENS | RE_NO_BK_REFS | RE_NO_BK_VBAR | RE_NO_EMPTY_RANGES | RE_DOT_NEWLINE | RE_CONTEXT_INDEP_ANCHORS | RE_CHAR_CLASSES | RE_UNMATCHED_RIGHT_PAREN_ORD | RE_NO_GNU_OPS)
#define _PC_REC_MAX_XFER_SIZE _PC_REC_MAX_XFER_SIZE
#define _GLIBCXX_PACKAGE__GLIBCXX_VERSION "version-unused"
#define __FLT32X_HAS_DENORM__ 1
#define __INT_FAST16_TYPE__ long int
#define __HAVE_FLOAT128X 0
#define _XBS5_LP64_OFF64 1
#define _SIZE_T_DEFINED 
#define _POSIX_PRIORITY_SCHEDULING 200809L
#define _GLIBCXX_HAVE_GETIPINFO 1
#define LC_IDENTIFICATION_MASK (1 << __LC_IDENTIFICATION)
#define _WCHAR_T_DEFINED_ 
#define ECHILD 10
#define EBADMSG 74
#define _SC_V6_ILP32_OFF32 _SC_V6_ILP32_OFF32
#define SA_ONSTACK 0x08000000
#define FLT_MIN_10_EXP __FLT_MIN_10_EXP__
#define UINT_LEAST8_MAX (255)
#define __USE_POSIX199506 1
#define _FEATURES_H 1
#define __LDBL_HAS_DENORM__ 1
#define GTEST_HAS_STD_SHARED_PTR_ 1
#define _STAT_VER _STAT_VER_LINUX
#define _SC_LEVEL2_CACHE_SIZE _SC_LEVEL2_CACHE_SIZE
#define CPU_XOR_S(setsize,destset,srcset1,srcset2) __CPU_OP_S (setsize, destset, srcset1, srcset2, ^)
#define __stub_getmsg 
#define _SC_ASYNCHRONOUS_IO _SC_ASYNCHRONOUS_IO
#define FLT_MIN __FLT_MIN__
#define _GLIBCXX11_USE_C99_STDIO 1
#define _SC_SHRT_MIN _SC_SHRT_MIN
#define LC_TIME_MASK (1 << __LC_TIME)
#define __stub_fattach 
#define _POSIX_SPORADIC_SERVER -1
#define __cplusplus 201402L
#define LC_GLOBAL_LOCALE ((locale_t) -1L)
#define __cpp_ref_qualifiers 200710
#define __DEC128_MAX__ 9.999999999999999999999999999999999E6144DL
#define LC_ADDRESS __LC_ADDRESS
#define __INT_LEAST32_MAX__ 0x7fffffff
#define _STL_ITERATOR_BASE_TYPES_H 1
#define SIGABRT 6
#define _IO_UNBUFFERED 2
#define SS_ONSTACK SS_ONSTACK
#define _GCC_WRAP_STDINT_H 
#define REG_RSP REG_RSP
#define CLD_CONTINUED CLD_CONTINUED
#define GTEST_PRED_FORMAT1_(pred_format,v1,on_failure) GTEST_ASSERT_(pred_format(#v1, v1), on_failure)
#define CLONE_PARENT 0x00008000
#define _IO_INTERNAL 010
#define __DEC32_MIN__ 1E-95DF
#define _GLIBCXX_HAVE_SYS_IPC_H 1
#define S_IFCHR __S_IFCHR
#define SA_RESETHAND 0x80000000
#define __dev_t_defined 
#define __DEPRECATED 1
#define _SC_RAW_SOCKETS _SC_RAW_SOCKETS
#define __S32_TYPE int
#define _STATBUF_ST_RDEV 
#define _SC_PHYS_PAGES _SC_PHYS_PAGES
#define _CS_POSIX_V6_LP64_OFF64_LINTFLAGS _CS_POSIX_V6_LP64_OFF64_LINTFLAGS
#define _SC_2_C_BIND _SC_2_C_BIND
#define __glibc_unlikely(cond) __builtin_expect ((cond), 0)
#define __cpp_rvalue_references 200610
#define __DBL_MAX_EXP__ 1024
#define LC_TELEPHONE_MASK (1 << __LC_TELEPHONE)
#define _IO_fpos_t _G_fpos_t
#define _SC_THREAD_SPORADIC_SERVER _SC_THREAD_SPORADIC_SERVER
#define __WCHAR_WIDTH__ 32
#define __BIT_TYPES_DEFINED__ 1
#define __FLT32_MAX__ 3.40282346638528859811704183484516925e+38F32
#define _GLIBCXX_EXTERN_TEMPLATE 1
#define _GLIBCXX_HAVE_STRERROR_L 1
#define PTHREAD_ADAPTIVE_MUTEX_INITIALIZER_NP { { 0, 0, 0, 0, PTHREAD_MUTEX_ADAPTIVE_NP, __PTHREAD_SPINS, { 0, 0 } } }
#define _BITS_WCHAR_H 1
#define ASSERT_EXIT(statement,predicate,regex) GTEST_DEATH_TEST_(statement, predicate, regex, GTEST_FATAL_FAILURE_)
#define GTEST_DISABLE_MSC_WARNINGS_PUSH_(warnings) 
#define __DEC128_EPSILON__ 1E-33DL
#define ASSERT_EQ(val1,val2) GTEST_ASSERT_EQ(val1, val2)
#define _USES_ALLOCATOR_H 1
#define _POSIX_TIMERS 200809L
#define GTEST_ATTRIBUTE_UNUSED_ __attribute__ ((unused))
#define _LIBINTL_H 1
#define EREMOTEIO 121
#define __SSE2_MATH__ 1
#define __gthrw_(name) __gthrw_ ## name
#define __ATOMIC_HLE_RELEASE 131072
#define __FSFILCNT_T_TYPE __SYSCALL_ULONG_TYPE
#define LC_NAME_MASK (1 << __LC_NAME)
#define _GLIBCXX_HAVE_ECANCELED 1
#define GTEST_PRED_FORMAT3_(pred_format,v1,v2,v3,on_failure) GTEST_ASSERT_(pred_format(#v1, #v2, #v3, v1, v2, v3), on_failure)
#define __HAVE_FLOAT16 0
#define EREMOTE 66
#define _SC_OPEN_MAX _SC_OPEN_MAX
#define S_BLKSIZE 512
#define __PTRDIFF_MAX__ 0x7fffffffffffffffL
#define _SC_STREAM_MAX _SC_STREAM_MAX
#define __CPU_MASK_TYPE __SYSCALL_ULONG_TYPE
#define UINT_LEAST16_WIDTH 16
#define __amd64 1
#define __itimerspec_defined 1
#define BUS_OBJERR BUS_OBJERR
#define GTEST_HAS_TYPED_TEST_P 1
#define _STDBOOL_H 
#define _IO_LINKED 0x80
#define _GLIBCXX_HAVE_ATTRIBUTE_VISIBILITY 1
#define _IO_HAVE_ST_BLKSIZE _G_HAVE_ST_BLKSIZE
#define __STDC_NO_THREADS__ 1
#define EXPECT_DOUBLE_EQ(val1,val2) EXPECT_PRED_FORMAT2(::testing::internal::CmpHelperFloatingPointEQ<double>, val1, val2)
#define GTEST_ASSERT_GE(val1,val2) ASSERT_PRED_FORMAT2(::testing::internal::CmpHelperGE, val1, val2)
#define __HAVE_FLOAT32 1
#define GTEST_TUPLE_NAMESPACE_ ::std
#define __USECONDS_T_TYPE __U32_TYPE
#define _IO_DELETE_DONT_CLOSE 0x40
#define _PTRDIFF_T_DECLARED 
#define _IO_FIXED 010000
#define _CS_POSIX_V6_ILP32_OFFBIG_CFLAGS _CS_POSIX_V6_ILP32_OFFBIG_CFLAGS
#define __OFF_T_TYPE __SYSCALL_SLONG_TYPE
#define __WIFSIGNALED(status) (((signed char) (((status) & 0x7f) + 1) >> 1) > 0)
#define sigmask(sig) ((int)(1u << ((sig) - 1)))
#define PTHREAD_CANCEL_DISABLE PTHREAD_CANCEL_DISABLE
#define GTEST_HAS_COMBINE 1
#define RE_NO_BK_PARENS (RE_NO_BK_BRACES << 1)
#define ENONET 64
#define EXFULL 54
#define _BASIC_STRING_TCC 1
#define __ATOMIC_HLE_ACQUIRE 65536
#define __FLT32_HAS_QUIET_NAN__ 1
#define __GNUG__ 7
#define PTHREAD_CANCEL_ENABLE PTHREAD_CANCEL_ENABLE
#define _GLIBCXX_HAVE___CXA_THREAD_ATEXIT_IMPL 1
#define ASSERT_PRED_FORMAT4(pred_format,v1,v2,v3,v4) GTEST_PRED_FORMAT4_(pred_format, v1, v2, v3, v4, GTEST_FATAL_FAILURE_)
#define __LONG_LONG_MAX__ 0x7fffffffffffffffLL
#define SI_SIGIO SI_SIGIO
#define __cpp_lib_quoted_string_io 201304
#define __SIZEOF_SIZE_T__ 8
#define ASSERT_PRED_FORMAT5(pred_format,v1,v2,v3,v4,v5) GTEST_PRED_FORMAT5_(pred_format, v1, v2, v3, v4, v5, GTEST_FATAL_FAILURE_)
#define GTEST_IS_NULL_LITERAL_(x) (sizeof(::testing::internal::IsNullLiteralHelper(x)) == 1)
#define _SC_XOPEN_LEGACY _SC_XOPEN_LEGACY
#define _WCHAR_H 1
#define __GLIBCXX__ 20180720
#define _WCHAR_T 
#define __HAVE_FLOAT64 1
#define st_atime st_atim.tv_sec
#define PTHREAD_MUTEX_INITIALIZER { { 0, 0, 0, 0, 0, __PTHREAD_SPINS, { 0, 0 } } }
#define __cpp_rvalue_reference 200610
#define EXPECT_STRNE(s1,s2) EXPECT_PRED_FORMAT2(::testing::internal::CmpHelperSTRNE, s1, s2)
#define _CS_POSIX_V7_ILP32_OFFBIG_LIBS _CS_POSIX_V7_ILP32_OFFBIG_LIBS
#define _IO_BOOLALPHA 0200000
#define PTHREAD_CREATE_DETACHED PTHREAD_CREATE_DETACHED
#define GTEST_LOG_(severity) ::testing::internal::GTestLog(::testing::internal::GTEST_ ##severity, __FILE__, __LINE__).GetStream()
#define __FD_ZERO(fdsp) do { int __d0, __d1; __asm__ __volatile__ ("cld; rep; " __FD_ZERO_STOS : "=c" (__d0), "=D" (__d1) : "a" (0), "0" (sizeof (fd_set) / sizeof (__fd_mask)), "1" (&__FDS_BITS (fdsp)[0]) : "memory"); } while (0)
#define __cpp_nsdmi 200809
#define WCOREDUMP(status) __WCOREDUMP (status)
#define __glibcxx_min_b(T,B) (__glibcxx_signed_b (T,B) ? -__glibcxx_max_b (T,B) - 1 : (T)0)
#define _CS_POSIX_V7_ILP32_OFF32_LIBS _CS_POSIX_V7_ILP32_OFF32_LIBS
#define _GLIBCXX_HAVE_INT64_T_LONG 1
#define __FLT64X_MIN_EXP__ (-16381)
#define _STL_RELOPS_H 1
#define SIZE_MAX (18446744073709551615UL)
#define __SIZEOF_WINT_T__ 4
#define _GLIBCXX_UTILITY 1
#define _PTR_TRAITS_H 1
#define __NO_CTYPE 1
#define _Bool bool
#define __f32x(x) x
#define __CORRECT_ISO_CPP_STRING_H_PROTO 
#define errno (*__errno_location ())
#define __cpp_lib_exchange_function 201304
#define _SC_XOPEN_REALTIME _SC_XOPEN_REALTIME
#define __cpp_lib_tuples_by_type 201304
#define ENOTNAM 118
#define UINT64_C(c) c ## UL
#define SIGALRM 14
#define __LONG_LONG_WIDTH__ 64
#define _SC_ADVISORY_INFO _SC_ADVISORY_INFO
#define __cpp_initializer_lists 200806
#define EADV 68
#define __U16_TYPE unsigned short int
#define _POSIX2_C_VERSION __POSIX2_THIS_VERSION
#define _GLIBCXX_HAVE_SYS_PARAM_H 1
#define __FLT32_MAX_EXP__ 128
#define WINT_MAX (4294967295u)
#define ENOSPC 28
#define __LC_ALL 6
#define ETIME 62
#define pthread_cleanup_push(routine,arg) do { __pthread_cleanup_class __clframe (routine, arg)
#define S_IWUSR __S_IWRITE
#define UINTPTR_MAX (18446744073709551615UL)
#define _SIGSET_NWORDS (1024 / (8 * sizeof (unsigned long int)))
#define __HAVE_FLOATN_NOT_TYPEDEF 0
#define ASSERT_PRED_FORMAT1(pred_format,v1) GTEST_PRED_FORMAT1_(pred_format, v1, GTEST_FATAL_FAILURE_)
#define ASSERT_PRED_FORMAT3(pred_format,v1,v2,v3) GTEST_PRED_FORMAT3_(pred_format, v1, v2, v3, GTEST_FATAL_FAILURE_)
#define __GNU_GETTEXT_SUPPORTED_REVISION(major) ((major) == 0 ? 1 : -1)
#define _PTRDIFF_T 
#define _MOVE_H 1
#define __PTHREAD_MUTEX_LOCK_ELISION 1
#define __cpp_hex_float 201603
#define __GCC_HAVE_DWARF2_CFI_ASM 1
#define _GLIBCXX_MOVE(__val) std::move(__val)
#define _GLIBCXX_IOSTREAM 1
#define _GLIBCXX_HOSTED 1
#define __GXX_ABI_VERSION 1011
#define si_uid _sifields._kill.si_uid
#define __WTERMSIG(status) ((status) & 0x7f)
#define _GLIBCXX_HAS_GTHREADS 1
#define _GLIBCXX_DEBUG_ONLY(_Statement) 
#define assert_perror(errnum) (!(errnum) ? __ASSERT_VOID_CAST (0) : __assert_perror_fail ((errnum), __FILE__, __LINE__, __ASSERT_FUNCTION))
#define _SC_PAGESIZE _SC_PAGESIZE
#define _SC_XOPEN_XCU_VERSION _SC_XOPEN_XCU_VERSION
#define _CS_POSIX_V6_LPBIG_OFFBIG_LIBS _CS_POSIX_V6_LPBIG_OFFBIG_LIBS
#define __isleap(year) ((year) % 4 == 0 && ((year) % 100 != 0 || (year) % 400 == 0))
#define __USE_GNU 1
#define __FLT128_HAS_INFINITY__ 1
#define S_ISBLK(mode) __S_ISTYPE((mode), __S_IFBLK)
#define __FLT_MIN_EXP__ (-125)
#define GTEST_PRED_FORMAT2_(pred_format,v1,v2,on_failure) GTEST_ASSERT_(pred_format(#v1, #v2, v1, v2), on_failure)
#define _GLIBCXX_ARRAY 1
#define _SC_READER_WRITER_LOCKS _SC_READER_WRITER_LOCKS
#define __FD_CLR(d,set) ((void) (__FDS_BITS (set)[__FD_ELT (d)] &= ~__FD_MASK (d)))
#define WEXITED 4
#define EBADE 52
#define _GLIBCXX_HAVE_ENODATA 1
#define GTEST_HAS_STD_FUNCTION_ 1
#define _PC_LINK_MAX _PC_LINK_MAX
#define PTHREAD_RWLOCK_INITIALIZER { { 0, 0, 0, 0, 0, 0, 0, 0, __PTHREAD_RWLOCK_ELISION_EXTRA, 0, 0 } }
#define __CPU_EQUAL_S(setsize,cpusetp1,cpusetp2) (__builtin_memcmp (cpusetp1, cpusetp2, setsize) == 0)
#define __S_IFREG 0100000
#define _SC_NL_ARGMAX _SC_NL_ARGMAX
#define __glibc_clang_has_extension(ext) 0
#define EXPECT_NO_FATAL_FAILURE(statement) GTEST_TEST_NO_FATAL_FAILURE_(statement, GTEST_NONFATAL_FAILURE_)
#define ASSERT_STRCASEEQ(s1,s2) ASSERT_PRED_FORMAT2(::testing::internal::CmpHelperSTRCASEEQ, s1, s2)
#define REG_R10 REG_R10
#define LC_IDENTIFICATION __LC_IDENTIFICATION
#define ILL_ILLOPN ILL_ILLOPN
#define _SC_2_UPE _SC_2_UPE
#define _SC_XBS5_LPBIG_OFFBIG _SC_XBS5_LPBIG_OFFBIG
#define WNOHANG 1
#define LC_MEASUREMENT_MASK (1 << __LC_MEASUREMENT)
#define alloca(size) __builtin_alloca (size)
#define LDBL_MIN __LDBL_MIN__
#define REG_NOSUB (REG_NEWLINE << 1)
#define __cpp_lib_transparent_operators 201510
#define REG_R11 REG_R11
#define _SC_2_PBS_LOCATE _SC_2_PBS_LOCATE
#define GTEST_HAS_STD_TUPLE_ 1
#define _SC_LINE_MAX _SC_LINE_MAX
#define SIGCHLD 17
#define EKEYREJECTED 129
#define _GLIBCXX_BEGIN_NAMESPACE_CXX11 namespace __cxx11 {
#define __HAVE_DISTINCT_FLOAT16 __HAVE_FLOAT16
#define __extern_always_inline extern __always_inline __attribute__ ((__gnu_inline__))
#define __PTRDIFF_T 
#define _GLIBCXX_HAVE_FINITEF 1
#define __glibcxx_requires_valid_range(_First,_Last) 
#define _GLIBCXX_HAVE_FINITEL 1
#define _SC_WORD_BIT _SC_WORD_BIT
#define _SC_ULONG_MAX _SC_ULONG_MAX
#define __cpp_lambdas 200907
#define _SC_TRACE_EVENT_NAME_MAX _SC_TRACE_EVENT_NAME_MAX
#define REGS_FIXED 2
#define INT_LEAST32_WIDTH 32
#define GTEST_MESSAGE_AT_(file,line,message,result_type) ::testing::internal::AssertHelper(result_type, file, line, message) = ::testing::Message()
#define __HAVE_GENERIC_SELECTION 0
#define _GLIBCXX_HAVE_SINHF 1
#define _SC_LEVEL2_CACHE_LINESIZE _SC_LEVEL2_CACHE_LINESIZE
#define _POSIX_ADVISORY_INFO 200809L
#define _GLIBCXX_HAVE_SINHL 1
#define _LOCALE_FWD_H 1
#define LC_MEASUREMENT __LC_MEASUREMENT
#define __FLT64X_HAS_QUIET_NAN__ 1
#define __INT_FAST64_TYPE__ long int
#define _ISTREAM_TCC 1
#define ADJ_TIMECONST 0x0020
#define _GLIBCXX_HAVE_SQRTF 1
#define _GLIBCXX_HAVE_SQRTL 1
#define PTHREAD_INHERIT_SCHED PTHREAD_INHERIT_SCHED
#define L_ctermid 9
#define _SC_MQ_OPEN_MAX _SC_MQ_OPEN_MAX
#define GTEST_INCLUDE_GTEST_INTERNAL_GTEST_DEATH_TEST_INTERNAL_H_ 
#define _BITS_SIGINFO_CONSTS_H 1
#define ENOBUFS 105
#define _CS_GNU_LIBPTHREAD_VERSION _CS_GNU_LIBPTHREAD_VERSION
#define __cpp_lib_allocator_is_always_equal 201411
#define _SC_SPAWN _SC_SPAWN
#define CLONE_VFORK 0x00004000
#define ADJ_TAI 0x0080
#define ASSERT_NE(val1,val2) GTEST_ASSERT_NE(val1, val2)
#define __FLT64_DENORM_MIN__ 4.94065645841246544176568792868221372e-324F64
#define __DBL_MIN__ double(2.22507385850720138309023271733240406e-308L)
#define RE_SYNTAX_GREP (RE_BK_PLUS_QM | RE_CHAR_CLASSES | RE_HAT_LISTS_NOT_NEWLINE | RE_INTERVALS | RE_NEWLINE_ALT)
#define EADDRINUSE 98
#define _BITS_STDIO_LIM_H 1
#define GTEST_CASE_NAMESPACE_(TestCaseName) gtest_case_ ##TestCaseName ##_
#define _GLIBCXX_HAVE_SYS_SEM_H 1
#define __CPU_CLR_S(cpu,setsize,cpusetp) (__extension__ ({ size_t __cpu = (cpu); __cpu / 8 < (setsize) ? (((__cpu_mask *) ((cpusetp)->__bits))[__CPUELT (__cpu)] &= ~__CPUMASK (__cpu)) : 0; }))
#define __PIE__ 2
#define REG_R12 REG_R12
#define REG_R14 REG_R14
#define _XOPEN_XPG4 1
#define SIG_ATOMIC_WIDTH 32
#define _GLIBCXX_HAVE_ENOLINK 1
#define __LITTLE_ENDIAN 1234
#define EXPECT_NE(val1,val2) EXPECT_PRED_FORMAT2(::testing::internal::CmpHelperNE, val1, val2)
#define CLOCK_REALTIME_COARSE 5
#define _GLIBCXX14_CONSTEXPR constexpr
#define __USE_XOPEN2KXSI 1
#define __WCOREFLAG 0x80
#define _XOPEN_LEGACY 1
#define _CS_XBS5_LPBIG_OFFBIG_LDFLAGS _CS_XBS5_LPBIG_OFFBIG_LDFLAGS
#define __REDIRECT(name,proto,alias) name proto __asm__ (__ASMNAME (#alias))
#define __HAVE_DISTINCT_FLOAT64 0
#define _IO_size_t size_t
#define htobe32(x) __bswap_32 (x)
#define __LP64__ 1
#define _XOPEN_UNIX 1
#define MINSIGSTKSZ 2048
#define UTIME_NOW ((1l << 30) - 1l)
#define _IO_off64_t __off64_t
#define LC_MONETARY_MASK (1 << __LC_MONETARY)
#define __HAVE_FLOAT32X 1
#define ENETRESET 102
#define SIGUSR1 10
#define FLT_RADIX __FLT_RADIX__
#define __time_t_defined 1
#define __FLT32X_EPSILON__ 2.22044604925031308084726333618164062e-16F32x
#define __USE_UNIX98 1
#define st_ctime st_ctim.tv_sec
#define GTEST_INCLUDE_GTEST_GTEST_TYPED_TEST_H_ 
#define WINT_WIDTH 32
#define __MODE_T_TYPE __U32_TYPE
#define __cpp_lib_is_swappable 201603
#define SIGTRAP 5
#define _GLIBCXX_HAVE_STRINGS_H 1
#define __LEAF_ATTR __attribute__ ((__leaf__))
#define S_IFREG __S_IFREG
#define __cpp_lib_incomplete_container_elements 201505
#define STA_NANO 0x2000
#define __DECIMAL_BID_FORMAT__ 1
#define CPU_ZERO(cpusetp) __CPU_ZERO_S (sizeof (cpu_set_t), cpusetp)
#define __GTHREAD_TIME_INIT {0,0}
#define GTEST_TEST_THROW_(statement,expected_exception,fail) GTEST_AMBIGUOUS_ELSE_BLOCKER_ if (::testing::internal::ConstCharPtr gtest_msg = "") { bool gtest_caught_expected = false; try { GTEST_SUPPRESS_UNREACHABLE_CODE_WARNING_BELOW_(statement); } catch (expected_exception const&) { gtest_caught_expected = true; } catch (...) { gtest_msg.value = "Expected: " #statement " throws an exception of type " #expected_exception ".\n  Actual: it throws a different type."; goto GTEST_CONCAT_TOKEN_(gtest_label_testthrow_, __LINE__); } if (!gtest_caught_expected) { gtest_msg.value = "Expected: " #statement " throws an exception of type " #expected_exception ".\n  Actual: it throws nothing."; goto GTEST_CONCAT_TOKEN_(gtest_label_testthrow_, __LINE__); } } else GTEST_CONCAT_TOKEN_(gtest_label_testthrow_, __LINE__): fail(gtest_msg.value)
#define _SC_MAPPED_FILES _SC_MAPPED_FILES
#define GTEST_INCLUDE_GTEST_GTEST_TEST_PART_H_ 
#define __RLIM64_T_TYPE __UQUAD_TYPE
#define _GLIBCXX_USE_C99_INTTYPES_WCHAR_T_TR1 1
#define _IOS_ATEND 4
#define _GLIBCXX_USE_DEPRECATED 1
#define GTEST_INCLUDE_GTEST_INTERNAL_GTEST_INTERNAL_H_ 
#define GTEST_INCLUDE_GTEST_GTEST_PRED_IMPL_H_ 
#define RE_SYNTAX_POSIX_EXTENDED (_RE_SYNTAX_POSIX_COMMON | RE_CONTEXT_INDEP_ANCHORS | RE_CONTEXT_INDEP_OPS | RE_NO_BK_BRACES | RE_NO_BK_PARENS | RE_NO_BK_VBAR | RE_CONTEXT_INVALID_OPS | RE_UNMATCHED_RIGHT_PAREN_ORD)
#define _GLIBCXX_HAVE_SETENV 1
#define WAIT_MYPGRP 0
#define INT_LEAST8_MAX (127)
#define FPE_INTDIV FPE_INTDIV
#define SEEK_DATA 3
#define LC_MONETARY __LC_MONETARY
#define _BITS_SETJMP_H 1
#define __GXX_TYPEINFO_EQUALITY_INLINE 1
#define SIZE_WIDTH __WORDSIZE
#define __CFLOAT64X _Complex long double
#define _GLIBCXX_PACKAGE_URL ""
#define _GLIBCXX_NUM_UNICODE_FACETS 2
#define __FLT64_MIN_10_EXP__ (-307)
#define _BITS_SIGNUM_H 1
#define _SC_2_PBS_CHECKPOINT _SC_2_PBS_CHECKPOINT
#define __FDS_BITS(set) ((set)->fds_bits)
#define _GLIBCXX_HAVE_ETIMEDOUT 1
#define CPU_CLR(cpu,cpusetp) __CPU_CLR_S (cpu, sizeof (cpu_set_t), cpusetp)
#define GTEST_INCLUDE_GTEST_GTEST_PROD_H_ 
#define CLONE_PARENT_SETTID 0x00100000
#define EBADR 53
#define _SC_FILE_LOCKING _SC_FILE_LOCKING
#define si_pkey _sifields._sigfault._bounds._pkey
#define __FLT64X_DECIMAL_DIG__ 21
#define RE_SYNTAX_ED RE_SYNTAX_POSIX_BASIC
#define __DEC128_MIN__ 1E-6143DL
#define LDBL_MAX_EXP __LDBL_MAX_EXP__
#define _GLIBCXX_ATOMIC_BUILTINS 1
#define SA_SIGINFO 4
#define __REGISTER_PREFIX__ 
#define __UINT16_MAX__ 0xffff
#define __glibcxx_requires_sorted_set(_First1,_Last1,_First2) 
#define _IOS_NOREPLACE 64
#define __DBL_HAS_DENORM__ 1
#define GTEST_TEMPLATE_ template <typename T> class
#define _SSTREAM_TCC 1
#define EIDRM 43
#define _SC_SIGQUEUE_MAX _SC_SIGQUEUE_MAX
#define _ALIGNED_BUFFER_H 1
#define FPE_FLTSUB FPE_FLTSUB
#define INT_FAST16_WIDTH __WORDSIZE
#define _PC_NAME_MAX _PC_NAME_MAX
#define GTEST_HAS_STD_UNIQUE_PTR_ 1
#define REG_STARTEND (1 << 2)
#define _CS_XBS5_ILP32_OFFBIG_LIBS _CS_XBS5_ILP32_OFFBIG_LIBS
#define _IO_TIED_PUT_GET 0x400
#define _CS_XBS5_ILP32_OFFBIG_LDFLAGS _CS_XBS5_ILP32_OFFBIG_LDFLAGS
#define __CORRECT_ISO_CPP_WCHAR_H_PROTO 
#define UINT_LEAST64_WIDTH 64
#define __attribute_pure__ __attribute__ ((__pure__))
#define __glibcxx_requires_non_empty_range(_First,_Last) 
#define GTEST_DEV_EMAIL_ "googletestframework@@googlegroups.com"
#define __HAVE_DISTINCT_FLOAT128X __HAVE_FLOAT128X
#define _LOCALE_FACETS_NONIO_H 1
#define ADJ_NANO 0x2000
#define CLONE_CHILD_SETTID 0x01000000
#define _GLIBCXX_HAVE_SINCOS 1
#define __USE_POSIX2 1
#define _BITS_SYSMACROS_H 1
#define _EXT_ALLOC_TRAITS_H 1
#define __FLT32_MIN__ 1.17549435082228750796873653722224568e-38F32
#define __UINT8_TYPE__ unsigned char
#define si_addr _sifields._sigfault.si_addr
#define _SC_BC_BASE_MAX _SC_BC_BASE_MAX
#define __SLONGWORD_TYPE long int
#define REG_R9 REG_R9
#define GTEST_REMOVE_REFERENCE_(T) typename ::testing::internal::RemoveReference<T>::type
#define _GLIBCXX_ABI_TAG_CXX11 __attribute ((__abi_tag__ ("cxx11")))
#define _SC_LEVEL2_CACHE_ASSOC _SC_LEVEL2_CACHE_ASSOC
#define _GLIBCXX_HAVE_ISWBLANK 1
#define __REDIRECT_LDBL(name,proto,alias) __REDIRECT (name, proto, alias)
#define CLD_DUMPED CLD_DUMPED
#define __NO_INLINE__ 1
#define EXPECT_LT(val1,val2) EXPECT_PRED_FORMAT2(::testing::internal::CmpHelperLT, val1, val2)
#define GTEST_INCLUDE_GTEST_INTERNAL_GTEST_PARAM_UTIL_H_ 
#define EBADFD 77
#define _SC_LEVEL1_ICACHE_ASSOC _SC_LEVEL1_ICACHE_ASSOC
#define __warndecl(name,msg) extern void name (void) __attribute__((__warning__ (msg)))
#define _GLIBCXX_HAVE_ALIGNED_ALLOC 1
#define CPU_SET(cpu,cpusetp) __CPU_SET_S (cpu, sizeof (cpu_set_t), cpusetp)
#define ECONNABORTED 103
#define _SC_TRACE _SC_TRACE
#define ETOOMANYREFS 109
#define __FLT_MANT_DIG__ 24
#define __LDBL_DECIMAL_DIG__ 21
#define _IO_wint_t wint_t
#define _GLIBCXX_HAVE_COMPLEX_H 1
#define INT_LEAST8_MIN (-128)
#define __VERSION__ "7.3.0"
#define _SC_NPROCESSORS_ONLN _SC_NPROCESSORS_ONLN
#define _GLIBCXX11_USE_C99_STDLIB 1
#define _GLIBCXX_HAVE_INT64_T 1
#define _GLIBCXX_USE_CONSTEXPR constexpr
#define S_ISFIFO(mode) __S_ISTYPE((mode), __S_IFIFO)
#define FD_ZERO(fdsetp) __FD_ZERO (fdsetp)
#define UINT_LEAST16_MAX (65535)
#define __UINT64_C(c) c ## UL
#define _PTRDIFF_T_ 
#define GTEST_CONCAT_TOKEN_IMPL_(foo,bar) foo ## bar
#define _GLIBCXX_HAVE_TANHF 1
#define _SYS_CDEFS_H 1
#define _GLIBCXX_HAVE_TANHL 1
#define __cpp_unicode_characters 200704
#define GTEST_PRED1_(pred,v1,on_failure) GTEST_ASSERT_(::testing::AssertPred1Helper(#pred, #v1, pred, v1), on_failure)
#define pthread_cleanup_pop_restore_np(execute) __clframe.__restore (); __clframe.__setdoit (execute); } while (0)
#define _CS_LFS_LIBS _CS_LFS_LIBS
#define S_IFDIR __S_IFDIR
#define _IO_ERR_SEEN 0x20
#define _GLIBCXX_USE_DECIMAL_FLOAT 1
#define RE_BACKSLASH_ESCAPE_IN_LISTS ((unsigned long int) 1)
#define __cpp_lib_make_reverse_iterator 201402
#define __CPU_OP_S(setsize,destset,srcset1,srcset2,op) (__extension__ ({ cpu_set_t *__dest = (destset); const __cpu_mask *__arr1 = (srcset1)->__bits; const __cpu_mask *__arr2 = (srcset2)->__bits; size_t __imax = (setsize) / sizeof (__cpu_mask); size_t __i; for (__i = 0; __i < __imax; ++__i) ((__cpu_mask *) __dest->__bits)[__i] = __arr1[__i] op __arr2[__i]; __dest; }))
#define _ISwbit(bit) ((bit) < 8 ? (int) ((1UL << (bit)) << 24) : ((bit) < 16 ? (int) ((1UL << (bit)) << 8) : ((bit) < 24 ? (int) ((1UL << (bit)) >> 8) : (int) ((1UL << (bit)) >> 24))))
#define GTEST_API_ __attribute__((visibility ("default")))
#define _GLIBCXX_HAVE_ENOSPC 1
#define INT_LEAST64_WIDTH 64
#define _INITIALIZER_LIST 
#define _STDC_PREDEF_H 1
#define INT_LEAST32_MIN (-2147483647-1)
#define _GLIBCXX_PACKAGE_BUGREPORT ""
#define SIGEV_SIGNAL SIGEV_SIGNAL
#define __INT_WCHAR_T_H 
#define GTEST_PRED3_(pred,v1,v2,v3,on_failure) GTEST_ASSERT_(::testing::AssertPred3Helper(#pred, #v1, #v2, #v3, pred, v1, v2, v3), on_failure)
#define _POSIX_SHARED_MEMORY_OBJECTS 200809L
#define __USE_XOPEN2K8 1
#define _SC_NL_MSGMAX _SC_NL_MSGMAX
#define REG_RBP REG_RBP
#define __cpp_decltype_auto 201304
#define REG_RBX REG_RBX
#define __cpp_lib_robust_nonmodifying_seq_ops 201304
#define _SC_EXPR_NEST_MAX _SC_EXPR_NEST_MAX
#define WEXITSTATUS(status) __WEXITSTATUS (status)
#define __W_EXITCODE(ret,sig) ((ret) << 8 | (sig))
#define _GLIBCXX_THROW(_EXC) 
#define _CS_XBS5_LP64_OFF64_LIBS _CS_XBS5_LP64_OFF64_LIBS
#define _SC_NGROUPS_MAX _SC_NGROUPS_MAX
#define _GLIBCXX_HAS_NESTED_TYPE(_NTYPE) template<typename _Tp, typename = __void_t<>> struct __has_ ##_NTYPE : false_type { }; template<typename _Tp> struct __has_ ##_NTYPE<_Tp, __void_t<typename _Tp::_NTYPE>> : true_type { };
#define __catch(X) catch(X)
#define ENOTSOCK 88
#define st_mtime st_mtim.tv_sec
#define __STRING(x) #x
#define __GCC_ATOMIC_INT_LOCK_FREE 2
#define _T_PTRDIFF_ 
#define _GLIBCXX_USE_NOEXCEPT noexcept
#define EPERM 1
#define ENOSTR 60
#define REG_RDI REG_RDI
#define _SC_NL_LANGMAX _SC_NL_LANGMAX
#define REG_RDX REG_RDX
#define __FLT128_MAX_EXP__ 16384
#define __INO_T_TYPE __SYSCALL_ULONG_TYPE
#define GTEST_FAIL() GTEST_FATAL_FAILURE_("Failed")
#define _GLIBCXX_HAVE_ENOSTR 1
#define __SI_HAVE_SIGSYS 1
#define _GLIBCXX_BEGIN_NAMESPACE_CONTAINER _GLIBCXX_BEGIN_NAMESPACE_VERSION
#define TRAP_TRACE TRAP_TRACE
#define F_ULOCK 0
#define SI_ASYNCIO SI_ASYNCIO
#define SCHED_ISO 4
#define TRAP_BRKPT TRAP_BRKPT
#define FLT_MAX_10_EXP __FLT_MAX_10_EXP__
#define __struct_tm_defined 1
#define _BITS_G_CONFIG_H 1
#define _GLIBCXX_HAVE_HYPOT 1
#define __ssize_t_defined 
#define _SC_LEVEL4_CACHE_SIZE _SC_LEVEL4_CACHE_SIZE
#define __GNUC_PREREQ(maj,min) ((__GNUC__ << 16) + __GNUC_MINOR__ >= ((maj) << 16) + (min))
#define _SC_FSYNC _SC_FSYNC
#define __FLT32_MANT_DIG__ 24
#define EL3HLT 46
#define FPE_INTOVF FPE_INTOVF
#define _GLIBCXX_HAVE_EPERM 1
#define ASSERT_GE(val1,val2) GTEST_ASSERT_GE(val1, val2)
#define RE_SYNTAX_EMACS 0
#define _STL_HEAP_H 1
#define GTEST_HAS_POSIX_RE (!GTEST_OS_WINDOWS)
#define STA_PLL 0x0001
#define __FLOAT_WORD_ORDER__ __ORDER_LITTLE_ENDIAN__
#define SIGPROF 27
#define __FSBLKCNT64_T_TYPE __UQUAD_TYPE
#define _IO_feof_unlocked(__fp) (((__fp)->_flags & _IO_EOF_SEEN) != 0)
#define _POSIX_THREAD_PROCESS_SHARED 200809L
#define __SUSECONDS_T_TYPE __SYSCALL_SLONG_TYPE
#define __cpp_lib_generic_associative_lookup 201304
#define __S_IFDIR 0040000
#define _POSIX_TRACE_INHERIT -1
#define __SIZE_T__ 
#define ASSERT_NO_FATAL_FAILURE(statement) GTEST_TEST_NO_FATAL_FAILURE_(statement, GTEST_FATAL_FAILURE_)
#define _CS_POSIX_V6_ILP32_OFF32_LINTFLAGS _CS_POSIX_V6_ILP32_OFF32_LINTFLAGS
#define __stub_gtty 
#define GTEST_GCC_VER_ (__GNUC__*10000 + __GNUC_MINOR__*100 + __GNUC_PATCHLEVEL__)
#define SEEK_CUR 1
#define R_OK 4
#define __NLINK_T_TYPE __SYSCALL_ULONG_TYPE
#define SIG_ATOMIC_MAX (2147483647)
#define S_IFMT __S_IFMT
#define EFAULT 14
#define GTEST_HAS_STREAM_REDIRECTION 1
#define __nlink_t_defined 
#define __attribute_deprecated_msg__(msg) __attribute__ ((__deprecated__ (msg)))
#define GTEST_ADD_REFERENCE_(T) typename ::testing::internal::AddReference<T>::type
#define _SC_SEM_VALUE_MAX _SC_SEM_VALUE_MAX
#define __sigset_t_defined 1
#define __glibcxx_requires_string_len(_String,_Len) 
#define _SC_SHRT_MAX _SC_SHRT_MAX
#define LC_PAPER __LC_PAPER
#define __LC_ADDRESS 9
#define GTEST_STDLIB_CXX11 1
#define _SYS_SIZE_T_H 
#define LC_MESSAGES_MASK (1 << __LC_MESSAGES)
#define _IO_SHOWPOINT 0400
#define _SC_MQ_PRIO_MAX _SC_MQ_PRIO_MAX
#define _GLIBCXX_HAVE_TGMATH_H 1
#define _GLIBCXX11_USE_C99_COMPLEX 1
#define INT32_MAX (2147483647)
#define EOPNOTSUPP 95
#define REG_TRAPNO REG_TRAPNO
#define _CS_POSIX_V7_ILP32_OFF32_LINTFLAGS _CS_POSIX_V7_ILP32_OFF32_LINTFLAGS
#define LC_ALL_MASK (LC_CTYPE_MASK | LC_NUMERIC_MASK | LC_TIME_MASK | LC_COLLATE_MASK | LC_MONETARY_MASK | LC_MESSAGES_MASK | LC_PAPER_MASK | LC_NAME_MASK | LC_ADDRESS_MASK | LC_TELEPHONE_MASK | LC_MEASUREMENT_MASK | LC_IDENTIFICATION_MASK )
#define _CS_POSIX_V6_ILP32_OFF32_LDFLAGS _CS_POSIX_V6_ILP32_OFF32_LDFLAGS
#define _SC_THREAD_ATTR_STACKSIZE _SC_THREAD_ATTR_STACKSIZE
#define GTEST_HAS_STD_INITIALIZER_LIST_ 1
#define __stub_sstk 
#define S_IWRITE S_IWUSR
#define ASSERT_PRED5(pred,v1,v2,v3,v4,v5) GTEST_PRED5_(pred, v1, v2, v3, v4, v5, GTEST_FATAL_FAILURE_)
#define _IO_IN_BACKUP 0x100
#define PTHREAD_RWLOCK_WRITER_NONRECURSIVE_INITIALIZER_NP { { 0, 0, 0, 0, 0, 0, 0, 0, __PTHREAD_RWLOCK_ELISION_EXTRA, 0, PTHREAD_RWLOCK_PREFER_WRITER_NONRECURSIVE_NP } }
#define _GLIBCXX_SET 1
#define __glibc_macro_warning1(message) _Pragma (#message)
#define __wur 
#define SIGHUP 1
#define _SC_THREAD_ROBUST_PRIO_INHERIT _SC_THREAD_ROBUST_PRIO_INHERIT
#define __STDC_IEC_559_COMPLEX__ 1
#define _G_HAVE_MMAP 1
#define _PC_ASYNC_IO _PC_ASYNC_IO
#define _CS_POSIX_V7_ILP32_OFF32_LDFLAGS _CS_POSIX_V7_ILP32_OFF32_LDFLAGS
#define _IO_OCT 040
#define PTHREAD_RECURSIVE_MUTEX_INITIALIZER_NP { { 0, 0, 0, 0, PTHREAD_MUTEX_RECURSIVE_NP, __PTHREAD_SPINS, { 0, 0 } } }
#define __FLT128_HAS_DENORM__ 1
#define FLT_DIG __FLT_DIG__
#define _XOPEN_XPG2 1
#define SIG_UNBLOCK 1
#define _GLIBCXX_NOEXCEPT noexcept
#define __glibcxx_requires_partitioned_lower(_First,_Last,_Value) 
#define _SC_SSIZE_MAX _SC_SSIZE_MAX
#define _GLIBCXX_HAVE_WCSTOF 1
#define _STL_ALGOBASE_H 1
#define _SC_XBS5_ILP32_OFF32 _SC_XBS5_ILP32_OFF32
#define _CXXABI_TWEAKS_H 1
#define __off64_t_defined 
#define _GLIBCXX_WEAK_DEFINITION 
#define __error_t_defined 1
#define __FLT128_DIG__ 33
#define _GLIBCXX_WRITE_MEM_BARRIER __atomic_thread_fence (__ATOMIC_RELEASE)
#define __SCHAR_WIDTH__ 8
#define _GLIBCXX_NUM_CATEGORIES 6
#define _GLIBCXX_USE_C99_INTTYPES_TR1 1
#define __INT32_C(c) c
#define __DEC64_EPSILON__ 1E-15DD
#define TEMP_FAILURE_RETRY(expression) (__extension__ ({ long int __result; do __result = (long int) (expression); while (__result == -1L && errno == EINTR); __result; }))
#define __ORDER_PDP_ENDIAN__ 3412
#define __DEC128_MIN_EXP__ (-6142)
#define _POSIX_MEMLOCK 200809L
#define _ISOC95_SOURCE 1
#define _GLIBCXX_HAVE_ENOSR 1
#define EL2HLT 51
#define CPU_COUNT(cpusetp) __CPU_COUNT_S (sizeof (cpu_set_t), cpusetp)
#define __FLT32_MAX_10_EXP__ 38
#define ILL_ILLTRP ILL_ILLTRP
#define _SC_SINGLE_PROCESS _SC_SINGLE_PROCESS
#define SIGSEGV 11
#define _SC_THREAD_PRIO_PROTECT _SC_THREAD_PRIO_PROTECT
#define W_STOPCODE(sig) __W_STOPCODE (sig)
#define _SC_V6_LPBIG_OFFBIG _SC_V6_LPBIG_OFFBIG
#define _IO_SHOWBASE 0200
#define BYTE_ORDER __BYTE_ORDER
#define ENOKEY 126
#define _UNISTD_H 1
#define __INT_FAST32_TYPE__ long int
#define __have_pthread_attr_t 1
#define _GLIBCXX_HAVE_LIMIT_DATA 1
#define _GLIBCXX_NOEXCEPT_QUAL 
#define _CS_POSIX_V6_LPBIG_OFFBIG_LINTFLAGS _CS_POSIX_V6_LPBIG_OFFBIG_LINTFLAGS
#define _GLIBCXX_BEGIN_NAMESPACE_LDBL_OR_CXX11 _GLIBCXX_BEGIN_NAMESPACE_CXX11
#define GTEST_INCLUDE_GTEST_INTERNAL_GTEST_PORT_ARCH_H_ 
#define _BITS_TYPESIZES_H 1
#define _XOPEN_VERSION 700
#define _SC_C_LANG_SUPPORT _SC_C_LANG_SUPPORT
#define __GTHREAD_MUTEX_INIT PTHREAD_MUTEX_INITIALIZER
#define _GLIBCXX_CTIME 1
#define _IO_USER_BUF 1
#define _SC_MULTI_PROCESS _SC_MULTI_PROCESS
#define htole32(x) __uint32_identity (x)
#define __SYSCALL_SLONG_TYPE __SLONGWORD_TYPE
#define EINVAL 22
#define _GLIBCXX_USE_C99_MATH_TR1 1
#define WSTOPSIG(status) __WSTOPSIG (status)
#define _GLIBCXX_USE_C99_MATH _GLIBCXX11_USE_C99_MATH
#define SIG_ATOMIC_MIN (-2147483647-1)
#define __UINT_LEAST16_TYPE__ short unsigned int
#define _POSIX_THREAD_ATTR_STACKADDR 200809L
#define __WIFEXITED(status) (__WTERMSIG(status) == 0)
#define ADJ_OFFSET 0x0001
#define STDC_HEADERS 1
#define _GLIBCXX_GUARD_BIT __guard_test_bit (0, 1)
#define __glibcxx_requires_irreflexive(_First,_Last) 
#define _STL_TREE_H 1
#define __FLT64X_HAS_INFINITY__ 1
#define GTEST_TEST_CLASS_NAME_(test_case_name,test_name) test_case_name ##_ ##test_name ##_Test
#define unix 1
#define __USE_POSIX 1
#define EDOM 33
#define __FD_ZERO_STOS "stosq"
#define _CS_XBS5_ILP32_OFF32_LIBS _CS_XBS5_ILP32_OFF32_LIBS
#define EADDRNOTAVAIL 99
#define ESHUTDOWN 108
#define _CHAR_TRAITS_H 1
#define _GLIBCXX_HAVE_STRING_H 1
#define _SC_PIPE _SC_PIPE
#define UINT16_WIDTH 16
#define _SC_2_C_VERSION _SC_2_C_VERSION
#define __INT16_MAX__ 0x7fff
#define __THROWNL throw ()
#define _GLIBCXX_CCTYPE 1
#define _BSD_SIZE_T_ 
#define _GLIBCXX_MOVE3(_Tp,_Up,_Vp) std::move(_Tp, _Up, _Vp)
#define __cpp_rtti 199711
#define __SIZE_TYPE__ long unsigned int
#define __PMT(args) args
#define __S_TYPEISSEM(buf) ((buf)->st_mode - (buf)->st_mode)
#define CPU_ALLOC(count) __CPU_ALLOC (count)
#define __UINT64_MAX__ 0xffffffffffffffffUL
#define LC_COLLATE __LC_COLLATE
#define __va_arg_pack_len() __builtin_va_arg_pack_len ()
#define __ULONGWORD_TYPE unsigned long int
#define _SIZE_T_DECLARED 
#define _POSIX_THREADS 200809L
#define _GLIBCXX_HAVE_LIMIT_AS 1
#define _PC_NO_TRUNC _PC_NO_TRUNC
#define INT_FAST64_WIDTH 64
#define EXPECT_LE(val1,val2) EXPECT_PRED_FORMAT2(::testing::internal::CmpHelperLE, val1, val2)
#define __FLT64X_DIG__ 18
#define __PTHREAD_COMPAT_PADDING_END 
#define __INT8_TYPE__ signed char
#define _ALLOCATOR_H 1
#define _CS_XBS5_LP64_OFF64_LINTFLAGS _CS_XBS5_LP64_OFF64_LINTFLAGS
#define _SC_PRIORITIZED_IO _SC_PRIORITIZED_IO
#define EXPECT_PRED1(pred,v1) GTEST_PRED1_(pred, v1, GTEST_NONFATAL_FAILURE_)
#define LC_NUMERIC_MASK (1 << __LC_NUMERIC)
#define __BIG_ENDIAN 4321
#define SA_STACK SA_ONSTACK
#define LC_NUMERIC __LC_NUMERIC
#define __cpp_digit_separators 201309
#define __HAVE_DISTINCT_FLOAT128 1
#define __ELF__ 1
#define ENOMEDIUM 123
#define SI_TKILL SI_TKILL
#define si_addr_lsb _sifields._sigfault.si_addr_lsb
#define __GCC_ASM_FLAG_OUTPUTS__ 1
#define STA_INS 0x0010
#define STA_MODE 0x4000
#define _SC_TRACE_LOG _SC_TRACE_LOG
#define __ILP32_OFFBIG_LDFLAGS "-m32"
#define ASSERT_DEATH(statement,regex) ASSERT_EXIT(statement, ::testing::internal::ExitedUnsuccessfully, regex)
#define _UNIQUE_PTR_H 1
#define _GLIBCXX_DEBUG_MACRO_SWITCH_H 1
#define _BITS_BYTESWAP_H 1
#define __ID_T_TYPE __U32_TYPE
#define CPU_AND(destset,srcset1,srcset2) __CPU_OP_S (sizeof (cpu_set_t), destset, srcset1, srcset2, &)
#define _ASM_GENERIC_ERRNO_BASE_H 
#define EXPECT_PRED3(pred,v1,v2,v3) GTEST_PRED3_(pred, v1, v2, v3, GTEST_NONFATAL_FAILURE_)
#define _STL_FUNCTION_H 1
#define ELIBSCN 81
#define GTEST_HAS_CLONE 1
#define _GLIBCXX_HAVE_MBSTATE_T 1
#define ADJ_OFFSET_SS_READ 0xa001
#define _PC_2_SYMLINKS _PC_2_SYMLINKS
#define __warnattr(msg) __attribute__((__warning__ (msg)))
#define _IO_funlockfile(_fp) 
#define GTEST_TEST_(test_case_name,test_name,parent_class,parent_id) class GTEST_TEST_CLASS_NAME_(test_case_name, test_name) : public parent_class { public: GTEST_TEST_CLASS_NAME_(test_case_name, test_name)() {} private: virtual void TestBody(); static ::testing::TestInfo* const test_info_ GTEST_ATTRIBUTE_UNUSED_; GTEST_DISALLOW_COPY_AND_ASSIGN_( GTEST_TEST_CLASS_NAME_(test_case_name, test_name));};::testing::TestInfo* const GTEST_TEST_CLASS_NAME_(test_case_name, test_name) ::test_info_ = ::testing::internal::MakeAndRegisterTestInfo( #test_case_name, #test_name, NULL, NULL, ::testing::internal::CodeLocation(__FILE__, __LINE__), (parent_id), parent_class::SetUpTestCase, parent_class::TearDownTestCase, new ::testing::internal::TestFactoryImpl< GTEST_TEST_CLASS_NAME_(test_case_name, test_name)>);void GTEST_TEST_CLASS_NAME_(test_case_name, test_name)::TestBody()
#define GTEST_ATTRIBUTE_NO_SANITIZE_ADDRESS_ 
#define SIGQUIT 3
#define __FLT_RADIX__ 2
#define _LOCALE_CLASSES_H 1
#define __INT_LEAST16_TYPE__ short int
#define __LDBL_EPSILON__ 1.08420217248550443400745280086994171e-19L
#define _GLIBCXX98_USE_C99_WCHAR 1
#define INTPTR_MAX (9223372036854775807L)
#define GTEST_HAS_PARAM_TEST 1
#define SCHED_RESET_ON_FORK 0x40000000
#define __UINTMAX_C(c) c ## UL
#define SI_KERNEL SI_KERNEL
#define _POSIX_C_SOURCE 200809L
#define _GLIBCXX_HAVE_DIRENT_H 1
#define _GLIBCXX_HAVE_SYS_STATVFS_H 1
#define __GLIBCXX_BITSIZE_INT_N_0 128
#define __LC_MESSAGES 5
#define _LOCALE_CLASSES_TCC 1
#define GTEST_HAS_STD_TYPE_TRAITS_ 1
#define _SC_LEVEL3_CACHE_ASSOC _SC_LEVEL3_CACHE_ASSOC
#define _T_WCHAR 
#define _BITS_SIGTHREAD_H 1
#define RE_SYNTAX_POSIX_EGREP (RE_SYNTAX_EGREP | RE_INTERVALS | RE_NO_BK_BRACES | RE_INVALID_INTERVAL_ORD)
#define _GLIBCXX17_CONSTEXPR 
#define _EXCEPTION_PTR_H 
#define _SC_THREAD_THREADS_MAX _SC_THREAD_THREADS_MAX
#define ENAVAIL 119
#define __uid_t_defined 
#define __k8 1
#define _GLIBCXX_NO_OBSOLETE_ISINF_ISNAN_DYNAMIC __GLIBC_PREREQ(2,23)
#define __SIZEOF_PTHREAD_BARRIERATTR_T 4
#define _SC_XOPEN_XPG3 _SC_XOPEN_XPG3
#define _SC_XOPEN_XPG4 _SC_XOPEN_XPG4
#define __LDBL_REDIR(name,proto) name proto
#define CLOCK_MONOTONIC_RAW 4
#define ASSERT_TRUE(condition) GTEST_TEST_BOOLEAN_((condition), #condition, false, true, GTEST_FATAL_FAILURE_)
#define __SIG_ATOMIC_MAX__ 0x7fffffff
#define _SC_PRIORITY_SCHEDULING _SC_PRIORITY_SCHEDULING
#define _SC_FD_MGMT _SC_FD_MGMT
#define _G_HAVE_MREMAP 1
#define ADD_FAILURE() GTEST_NONFATAL_FAILURE_("Failed")
#define __f32(x) x ##f
#define SIGEV_THREAD_ID SIGEV_THREAD_ID
#define _GLIBCXX_HAVE_LIMIT_RSS 1
#define _BITS_SIGSTACK_H 1
#define _GLIBCXX_X86_RDRAND 1
#define _LOCALE_FACETS_H 1
#define __GCC_ATOMIC_WCHAR_T_LOCK_FREE 2
#define __cpp_sized_deallocation 201309
#define _GLIBCXX_USE_DUAL_ABI 1
#define EOVERFLOW 75
#define __bswap_constant_16(x) ((unsigned short int) ((((x) >> 8) & 0xff) | (((x) & 0xff) << 8)))
#define __FSID_T_TYPE struct { int __val[2]; }
#define GTEST_IS_THREADSAFE (GTEST_HAS_MUTEX_AND_THREAD_LOCAL_ || (GTEST_OS_WINDOWS && !GTEST_OS_WINDOWS_PHONE && !GTEST_OS_WINDOWS_RT) || GTEST_HAS_PTHREAD)
#define __WCHAR_MAX __WCHAR_MAX__
#define CLONE_NEWCGROUP 0x02000000
#define UINT32_MAX (4294967295U)
#define SIG_DFL ((__sighandler_t) 0)
#define DBL_MAX __DBL_MAX__
#define __SIZEOF_PTRDIFF_T__ 8
#define _LFS64_ASYNCHRONOUS_IO 1
#define SEEK_SET 0
#define _ANSI_STDDEF_H 
#define _CS_V7_ENV _CS_V7_ENV
#define _LFS64_STDIO 1
#define __NFDBITS (8 * (int) sizeof (__fd_mask))
#define _GLIBCXX_VISIBILITY(V) __attribute__ ((__visibility__ (#V)))
#define GTEST_DECLARE_bool_(name) GTEST_API_ extern bool GTEST_FLAG(name)
#define _GLIBCXX_HAVE_COSF 1
#define _GLIBCXX_HAVE_COSL 1
#define GTEST_DEATH_TEST_(statement,predicate,regex,fail) GTEST_AMBIGUOUS_ELSE_BLOCKER_ if (::testing::internal::AlwaysTrue()) { const ::testing::internal::RE& gtest_regex = (regex); ::testing::internal::DeathTest* gtest_dt; if (!::testing::internal::DeathTest::Create(#statement, &gtest_regex, __FILE__, __LINE__, &gtest_dt)) { goto GTEST_CONCAT_TOKEN_(gtest_label_, __LINE__); } if (gtest_dt != NULL) { ::testing::internal::scoped_ptr< ::testing::internal::DeathTest> gtest_dt_ptr(gtest_dt); switch (gtest_dt->AssumeRole()) { case ::testing::internal::DeathTest::OVERSEE_TEST: if (!gtest_dt->Passed(predicate(gtest_dt->Wait()))) { goto GTEST_CONCAT_TOKEN_(gtest_label_, __LINE__); } break; case ::testing::internal::DeathTest::EXECUTE_TEST: { ::testing::internal::DeathTest::ReturnSentinel gtest_sentinel(gtest_dt); GTEST_EXECUTE_DEATH_TEST_STATEMENT_(statement, gtest_dt); gtest_dt->Abort(::testing::internal::DeathTest::TEST_DID_NOT_DIE); break; } default: break; } } } else GTEST_CONCAT_TOKEN_(gtest_label_, __LINE__): fail(::testing::internal::DeathTest::LastMessage())
#define __f64(x) x
#define __bswap_constant_32(x) ((((x) & 0xff000000) >> 24) | (((x) & 0x00ff0000) >> 8) | (((x) & 0x0000ff00) << 8) | (((x) & 0x000000ff) << 24))
#define _GLIBCXX_USE_FLOAT128 1
#define __sig_atomic_t_defined 1
#define _IO_FLAGS2_NOTCANCEL 2
#define __stub_sigreturn 
#define __errordecl(name,msg) extern void name (void) __attribute__((__error__ (msg)))
#define _GLIBCXX_HAVE_UTIME_H 1
#define _XOPEN_SOURCE_EXTENDED 1
#define _VECTOR_TCC 1
#define INT_LEAST8_WIDTH 8
#define GTEST_HAS_STD_FORWARD_LIST_ 1
#define __FLT32X_MANT_DIG__ 53
#define CLOCK_TAI 11
#define _GLIBCXX_END_NAMESPACE_VERSION 
#define EUCLEAN 117
#define __glibcxx_requires_sorted_set_pred(_First1,_Last1,_First2,_Pred) 
#define __restrict_arr 
#define ASSERT_LE(val1,val2) GTEST_ASSERT_LE(val1, val2)
#define _GLIBCXX_USE_UTIMENSAT 1
#define _CS_POSIX_V7_ILP32_OFFBIG_CFLAGS _CS_POSIX_V7_ILP32_OFFBIG_CFLAGS
#define INT8_C(c) c
#define EAFNOSUPPORT 97
#define ESTRPIPE 86
#define RE_NREGS 30
#define _STL_MULTIMAP_H 1
#define _SIGNAL_H 
#define _PC_MAX_INPUT _PC_MAX_INPUT
#define _SC_DELAYTIMER_MAX _SC_DELAYTIMER_MAX
#define _POSIX_SHELL 1
#define strndupa(s,n) (__extension__ ({ const char *__old = (s); size_t __len = strnlen (__old, (n)); char *__new = (char *) __builtin_alloca (__len + 1); __new[__len] = '\0'; (char *) memcpy (__new, __old, __len); }))
#define __attribute_artificial__ __attribute__ ((__artificial__))
#define __USE_MISC 1
#define _BASIC_STRING_H 1
#define EBUSY 16
#define __UWORD_TYPE unsigned long int
#define _EXCEPTION_DEFINES_H 1
#define GTEST_SUPPRESS_UNREACHABLE_CODE_WARNING_BELOW_(statement) if (::testing::internal::AlwaysTrue()) { statement; }
#define __LC_PAPER 7
#define __S_IEXEC 0100
#define _SC_V7_ILP32_OFFBIG _SC_V7_ILP32_OFFBIG
#define W_OK 2
#define ENODEV 19
#define __gthrw2(name,name2,type) static __typeof(type) name __attribute__ ((__weakref__(#name2))); __gthrw_pragma(weak type)
#define _POSIX_ASYNCHRONOUS_IO 200809L
#define __x86_64__ 1
#define _SIZE_T_ 
#define __bswap_constant_64(x) (__extension__ ((((x) & 0xff00000000000000ull) >> 56) | (((x) & 0x00ff000000000000ull) >> 40) | (((x) & 0x0000ff0000000000ull) >> 24) | (((x) & 0x000000ff00000000ull) >> 8) | (((x) & 0x00000000ff000000ull) << 8) | (((x) & 0x0000000000ff0000ull) << 24) | (((x) & 0x000000000000ff00ull) << 40) | (((x) & 0x00000000000000ffull) << 56)))
#define __FLT32X_MIN_EXP__ (-1021)
#define __PTHREAD_RWLOCK_INT_FLAGS_SHARED 1
#define __DEC32_SUBNORMAL_MIN__ 0.000001E-95DF
#define _IO_iconv_t _G_iconv_t
#define CLONE_NEWPID 0x20000000
#define UINT_FAST32_MAX (18446744073709551615UL)
#define _GLIBCXX_SYNCHRONIZATION_HAPPENS_BEFORE(A) 
#define _GLIBCXX_HAVE_DLFCN_H 1
#define __S_IWRITE 0200
#define strdupa(s) (__extension__ ({ const char *__old = (s); size_t __len = strlen (__old) + 1; char *__new = (char *) __builtin_alloca (__len); (char *) memcpy (__new, __old, __len); }))
#define _WCHAR_T_H 
#define _SC_SAVED_IDS _SC_SAVED_IDS
#define EPROTO 71
#define SCHED_BATCH 3
#define STA_FLL 0x0008
#define POLL_OUT POLL_OUT
#define GTEST_INCLUDE_GTEST_GTEST_PRINTERS_H_ 
#define INT16_WIDTH 16
#define _GLIBCXX_END_EXTERN_C }
#define __INT_FAST16_MAX__ 0x7fffffffffffffffL
#define __stub_revoke 
#define __timer_t_defined 1
#define __WCLONE 0x80000000
#define _GLIBCXX_HAVE_VSWSCANF 1
#define EBFONT 59
#define LC_PAPER_MASK (1 << __LC_PAPER)
#define _GLIBCXX_END_NAMESPACE_CXX11 }
#define _GLIBCXX_ICONV_CONST 
#define _POSIX_MONOTONIC_CLOCK 0
#define __off_t_defined 
#define EKEYEXPIRED 127
#define _SC_2_PBS_ACCOUNTING _SC_2_PBS_ACCOUNTING
#define __WCHAR_MIN __WCHAR_MIN__
#define __FLT64_DIG__ 15
#define S_ISVTX __S_ISVTX
#define EROFS 30
#define __GLIBC_USE_LIB_EXT2 1
#define __UINT_FAST32_MAX__ 0xffffffffffffffffUL
#define __CPU_ALLOC_SIZE(count) ((((count) + __NCPUBITS - 1) / __NCPUBITS) * sizeof (__cpu_mask))
#define __UINT_LEAST64_TYPE__ long unsigned int
#define ADJ_FREQUENCY 0x0002
#define _SC_SEMAPHORES _SC_SEMAPHORES
#define _CS_POSIX_V7_LPBIG_OFFBIG_LIBS _CS_POSIX_V7_LPBIG_OFFBIG_LIBS
#define TEST_F(test_fixture,test_name) GTEST_TEST_(test_fixture, test_name, test_fixture, ::testing::internal::GetTypeId<test_fixture>())
#define __FLT_HAS_QUIET_NAN__ 1
#define BUS_MCEERR_AR BUS_MCEERR_AR
#define UINTMAX_MAX (__UINT64_C(18446744073709551615))
#define _SC_THREAD_ATTR_STACKADDR _SC_THREAD_ATTR_STACKADDR
#define _IO_DONT_CLOSE 0100000
#define INT32_C(c) c
#define __FLT_MAX_10_EXP__ 38
#define _GLIBCXX_HAVE_FLOORF 1
#define __FD_MASK(d) ((__fd_mask) (1UL << ((d) % __NFDBITS)))
#define GTEST_LOCK_EXCLUDED_(locks) 
#define __GTHREAD_ONCE_INIT PTHREAD_ONCE_INIT
#define _GLIBCXX_HAVE_FLOORL 1
#define WCOREFLAG __WCOREFLAG
#define _GLIBCXX_CWCTYPE 1
#define _SC_HOST_NAME_MAX _SC_HOST_NAME_MAX
#define S_IXGRP (S_IXUSR >> 3)
#define __glibc_c99_flexarr_available 1
#define __LONG_MAX__ 0x7fffffffffffffffL
#define _SC_ATEXIT_MAX _SC_ATEXIT_MAX
#define __WCHAR_T__ 
#define __FLT64X_HAS_DENORM__ 1
#define __DEC128_SUBNORMAL_MIN__ 0.000000000000000000000000000000001E-6143DL
#define _PC_SYNC_IO _PC_SYNC_IO
#define ELIBACC 79
#define _XOPEN_SHM 1
#define EHWPOISON 133
#define _GLIBCXX_NORETURN __attribute__ ((__noreturn__))
#define _SC_SEM_NSEMS_MAX _SC_SEM_NSEMS_MAX
#define __FLT_HAS_INFINITY__ 1
#define ASSERT_ANY_THROW(statement) GTEST_TEST_ANY_THROW_(statement, GTEST_FATAL_FAILURE_)
#define __WSTOPSIG(status) __WEXITSTATUS(status)
#define EXPECT_FLOAT_EQ(val1,val2) EXPECT_PRED_FORMAT2(::testing::internal::CmpHelperFloatingPointEQ<float>, val1, val2)
#define CPU_ZERO_S(setsize,cpusetp) __CPU_ZERO_S (setsize, cpusetp)
#define __S_TYPEISSHM(buf) ((buf)->st_mode - (buf)->st_mode)
#define __cpp_lib_integer_sequence 201304
#define _BSD_PTRDIFF_T_ 
#define _SYS_WAIT_H 1
#define _BITS_PTHREADTYPES_COMMON_H 1
#define __cpp_unicode_literals 200710
#define _GLIBCXX_NUM_CXX11_FACETS 16
#define SIGXFSZ 25
#define _GLIBCXX_CDTOR_CALLABI 
#define __S_IFMT 0170000
#define GTEST_ASSERT_LT(val1,val2) ASSERT_PRED_FORMAT2(::testing::internal::CmpHelperLT, val1, val2)
#define __UINT_FAST16_TYPE__ long unsigned int
#define __bos0(ptr) __builtin_object_size (ptr, 0)
#define __DEC64_MAX__ 9.999999999999999E384DD
#define _CS_POSIX_V6_LPBIG_OFFBIG_CFLAGS _CS_POSIX_V6_LPBIG_OFFBIG_CFLAGS
#define _MEMORYFWD_H 1
#define __ENUM_IDTYPE_T 1
#define __PTHREAD_MUTEX_USE_UNION 0
#define WIFSTOPPED(status) __WIFSTOPPED (status)
#define _POSIX_MEMORY_PROTECTION 200809L
#define __INT_FAST32_WIDTH__ 64
#define NULL __null
#define GTHR_ACTIVE_PROXY __gthrw_(__pthread_key_create)
#define _GLIBCXX_BITS_STD_ABS_H 
#define GTEST_PRED_FORMAT5_(pred_format,v1,v2,v3,v4,v5,on_failure) GTEST_ASSERT_(pred_format(#v1, #v2, #v3, #v4, #v5, v1, v2, v3, v4, v5), on_failure)
#define __CHAR16_TYPE__ short unsigned int
#define LT_OBJDIR ".libs/"
#define _POSIX2_C_BIND __POSIX2_THIS_VERSION
#define __PRAGMA_REDEFINE_EXTNAME 1
#define _GLIBCXX_GUARD_TEST(x) (*(char *) (x) != 0)
#define INT_LEAST32_MAX (2147483647)
#define E2BIG 7
#define __glibcxx_signed_b(T,B) ((T)(-1) < 0)
#define BIG_ENDIAN __BIG_ENDIAN
#define _GLIBCXX_HAVE_SINF 1
#define FPE_FLTOVF FPE_FLTOVF
#define _GLIBCXX_HAVE_SINL 1
#define TEST_P(test_case_name,test_name) class GTEST_TEST_CLASS_NAME_(test_case_name, test_name) : public test_case_name { public: GTEST_TEST_CLASS_NAME_(test_case_name, test_name)() {} virtual void TestBody(); private: static int AddToRegistry() { ::testing::UnitTest::GetInstance()->parameterized_test_registry(). GetTestCasePatternHolder<test_case_name>( #test_case_name, ::testing::internal::CodeLocation( __FILE__, __LINE__))->AddTestPattern( #test_case_name, #test_name, new ::testing::internal::TestMetaFactory< GTEST_TEST_CLASS_NAME_( test_case_name, test_name)>()); return 0; } static int gtest_registering_dummy_ GTEST_ATTRIBUTE_UNUSED_; GTEST_DISALLOW_COPY_AND_ASSIGN_( GTEST_TEST_CLASS_NAME_(test_case_name, test_name)); }; int GTEST_TEST_CLASS_NAME_(test_case_name, test_name)::gtest_registering_dummy_ = GTEST_TEST_CLASS_NAME_(test_case_name, test_name)::AddToRegistry(); void GTEST_TEST_CLASS_NAME_(test_case_name, test_name)::TestBody()
#define _STRINGS_H 1
#define _VA_LIST_DEFINED 
#define __SIZE_WIDTH__ 64
#define _GLIBCXX_MAP 1
#define _SC_2_LOCALEDEF _SC_2_LOCALEDEF
#define _OSTREAM_TCC 1
#define __BLKSIZE_T_TYPE __SYSCALL_SLONG_TYPE
#define __SEG_FS 1
#define ASSERT_STRCASENE(s1,s2) ASSERT_PRED_FORMAT2(::testing::internal::CmpHelperSTRCASENE, s1, s2)
#define __INT_LEAST16_MAX__ 0x7fff
#define __stub_lchmod 
#define CLONE_NEWUSER 0x10000000
#define pthread_cleanup_pop(execute) __clframe.__setdoit (execute); } while (0)
#define __DEC64_MANT_DIG__ 16
#define UINT64_WIDTH 64
#define _CS_XBS5_ILP32_OFF32_LDFLAGS _CS_XBS5_ILP32_OFF32_LDFLAGS
#define EDEADLK 35
#define __UINT_LEAST32_MAX__ 0xffffffffU
#define _SC_SS_REPL_MAX _SC_SS_REPL_MAX
#define _CS_POSIX_V6_ILP32_OFF32_LIBS _CS_POSIX_V6_ILP32_OFF32_LIBS
#define _GLIBCXX_MANGLE_SIZE_T m
#define __SEG_GS 1
#define _SC_LEVEL4_CACHE_LINESIZE _SC_LEVEL4_CACHE_LINESIZE
#define _PC_REC_XFER_ALIGN _PC_REC_XFER_ALIGN
#define EL3RST 47
#define __FLT32_DENORM_MIN__ 1.40129846432481707092372958328991613e-45F32
#define __GCC_ATOMIC_LONG_LOCK_FREE 2
#define ASSERT_STRNE(s1,s2) ASSERT_PRED_FORMAT2(::testing::internal::CmpHelperSTRNE, s1, s2)
#define __SIG_ATOMIC_WIDTH__ 32
#define _GLIBCXX_RELEASE 7
#define si_stime _sifields._sigchld.si_stime
#define _SC_MEMORY_PROTECTION _SC_MEMORY_PROTECTION
#define _SC_THREAD_KEYS_MAX _SC_THREAD_KEYS_MAX
#define _IO_IS_APPENDING 0x1000
#define __INT_LEAST64_TYPE__ long int
#define INT64_MAX (__INT64_C(9223372036854775807))
#define htole16(x) __uint16_identity (x)
#define _CODECVT_H 1
#define _GLIBCXX_HAVE_FCNTL_H 1
#define _GLIBCXX_SYMVER_GNU 1
#define __INT16_TYPE__ short int
#define __INT_LEAST8_TYPE__ signed char
#define GTEST_HAS_TR1_TUPLE 1
#define GTEST_INTENTIONAL_CONST_COND_PUSH_() GTEST_DISABLE_MSC_WARNINGS_PUSH_(4127)
#define SEGV_BNDERR SEGV_BNDERR
#define _ASSERT_H_DECLS 
#define ENOTDIR 20
#define __DEC32_MAX_EXP__ 97
#define SEEK_HOLE 4
#define ADJ_SETOFFSET 0x0100
#define GTEST_ASSERT_(expression,on_failure) GTEST_AMBIGUOUS_ELSE_BLOCKER_ if (const ::testing::AssertionResult gtest_ar = (expression)) ; else on_failure(gtest_ar.failure_message())
#define TIMER_ABSTIME 1
#define _SC_LEVEL3_CACHE_SIZE _SC_LEVEL3_CACHE_SIZE
#define __INT_FAST8_MAX__ 0x7f
#define ILL_BADSTK ILL_BADSTK
#define FLT_EPSILON __FLT_EPSILON__
#define __FLT128_MAX__ 1.18973149535723176508575932662800702e+4932F128
#define __INTPTR_MAX__ 0x7fffffffffffffffL
#define REG_RIP REG_RIP
#define _SYS_TYPES_H 1
#define FP_XSTATE_MAGIC1 0x46505853U
#define ECONNRESET 104
#define _OLD_STDIO_MAGIC 0xFABC0000
#define _STL_TEMPBUF_H 1
#define _POSIX_TYPED_MEMORY_OBJECTS -1
#define SIGSYS 31
#define CSIGNAL 0x000000ff
#define _GLIBCXX_GCC_GTHR_H 
#define linux 1
#define _SC_2_VERSION _SC_2_VERSION
#define TYPED_TEST_CASE(CaseName,Types) typedef ::testing::internal::TypeList< Types >::type GTEST_TYPE_PARAMS_(CaseName)
#define __S_ISTYPE(mode,mask) (((mode) & __S_IFMT) == (mask))
#define SIGSTOP 19
#define _IOS_APPEND 8
#define _GLIBCXX_DEBUG_ASSERT(_Condition) 
#define GTEST_CAN_COMPARE_NULL 1
#define ENXIO 6
#define _SC_THREAD_CPUTIME _SC_THREAD_CPUTIME
#define SA_NOCLDSTOP 1
#define __GXX_MERGED_TYPEINFO_NAMES 0
#define CLOCK_MONOTONIC 1
#define ASSERT_PRED1(pred,v1) GTEST_PRED1_(pred, v1, GTEST_FATAL_FAILURE_)
#define __cpp_range_based_for 200907
#define __FLT64_HAS_QUIET_NAN__ 1
#define _GLIBCXX_HAVE_WCTYPE_H 1
#define ASSERT_PRED2(pred,v1,v2) GTEST_PRED2_(pred, v1, v2, GTEST_FATAL_FAILURE_)
#define ____FILE_defined 1
#define _IOLBF 1
#define ILL_PRVREG ILL_PRVREG
#define SIGSTKFLT 16
#define MOD_NANO ADJ_NANO
#define htole64(x) __uint64_identity (x)
#define FPE_FLTINV FPE_FLTINV
#define _IO_stdout ((_IO_FILE*)(&_IO_2_1_stdout_))
#define GTEST_DEFINE_string_(name,default_val,doc) GTEST_API_ ::std::string GTEST_FLAG(name) = (default_val)
#define EBADRQC 56
#define _IO_UPPERCASE 01000
#define __WEXITSTATUS(status) (((status) & 0xff00) >> 8)
#define ASSERT_PRED4(pred,v1,v2,v3,v4) GTEST_PRED4_(pred, v1, v2, v3, v4, GTEST_FATAL_FAILURE_)
#define SA_INTERRUPT 0x20000000
#define WIFSIGNALED(status) __WIFSIGNALED (status)
#define MOD_ESTERROR ADJ_ESTERROR
#define _GLIBCXX_ALGORITHM 1
#define __WALL 0x40000000
#define _GLIBCXX_HAVE_LDEXPF 1
#define _SC_LEVEL1_DCACHE_SIZE _SC_LEVEL1_DCACHE_SIZE
#define __try try
#define _SC_MEMLOCK_RANGE _SC_MEMLOCK_RANGE
#define __SIGEV_MAX_SIZE 64
#define _GLIBCXX_USE_TMPNAM 1
#define __FLT32_MIN_10_EXP__ (-37)
#define __SSE2__ 1
#define __KEY_T_TYPE __S32_TYPE
#define sigev_notify_function _sigev_un._sigev_thread._function
#define _POSIX2_C_DEV __POSIX2_THIS_VERSION
#define __ldiv_t_defined 1
#define SEGV_MAPERR SEGV_MAPERR
#define _CS_POSIX_V5_WIDTH_RESTRICTED_ENVS _CS_V5_WIDTH_RESTRICTED_ENVS
#define __EXCEPTIONS 1
#define __WORDSIZE 64
#define INT64_MIN (-__INT64_C(9223372036854775807)-1)
#define __exctype_l(name) extern int name (int, locale_t) __THROW
#define __BEGIN_DECLS extern "C" {
#define _SC_PII_XTI _SC_PII_XTI
#define __LDBL_MANT_DIG__ 64
#define GTEST_USE_OWN_TR1_TUPLE 0
#define GTEST_HAS_GLOBAL_STRING 0
#define _CS_XBS5_ILP32_OFF32_LINTFLAGS _CS_XBS5_ILP32_OFF32_LINTFLAGS
#define SEGV_PKUERR SEGV_PKUERR
#define _PC_REC_MIN_XFER_SIZE _PC_REC_MIN_XFER_SIZE
#define _GETOPT_POSIX_H 1
#define S_IFSOCK __S_IFSOCK
#define GTEST_ARRAY_SIZE_(array) (sizeof(array) / sizeof(array[0]))
#define LC_TELEPHONE __LC_TELEPHONE
#define __SIZEOF_PTHREAD_MUTEXATTR_T 4
#define ENAMETOOLONG 36
#define _GLIBCXX_NUMERIC_LIMITS 1
#define _SC_SELECT _SC_SELECT
#define EFBIG 27
#define _SC_FIFO _SC_FIFO
#define _GLIBCXX_HAVE_INTTYPES_H 1
#define CLD_STOPPED CLD_STOPPED
#define __DBL_HAS_QUIET_NAN__ 1
#define _IO_BAD_SEEN 0x4000
#define __FLT64_HAS_INFINITY__ 1
#define _GLIBCXX_CSTDLIB 1
#define __FLT64X_MAX__ 1.18973149535723176502126385303097021e+4932F64x
#define EXPECT_PRED4(pred,v1,v2,v3,v4) GTEST_PRED4_(pred, v1, v2, v3, v4, GTEST_NONFATAL_FAILURE_)
#define _PC_SOCK_MAXBUF _PC_SOCK_MAXBUF
#define POLL_HUP POLL_HUP
#define _SC_FILE_SYSTEM _SC_FILE_SYSTEM
#define _STRING_H 1
#define UINT_FAST32_WIDTH __WORDSIZE
#define __GNUC_VA_LIST 
#define _GCC_MAX_ALIGN_T 
#define BUS_ADRERR BUS_ADRERR
#define RE_NO_BK_VBAR (RE_NO_BK_REFS << 1)
#define __SIG_ATOMIC_MIN__ (-__SIG_ATOMIC_MAX__ - 1)
#define __code_model_small__ 1
#define _XOPEN_REALTIME 1
#define CLONE_PTRACE 0x00002000
#define PTHREAD_ERRORCHECK_MUTEX_INITIALIZER_NP { { 0, 0, 0, 0, PTHREAD_MUTEX_ERRORCHECK_NP, __PTHREAD_SPINS, { 0, 0 } } }
#define assert(expr) (static_cast <bool> (expr) ? void (0) : __assert_fail (#expr, __FILE__, __LINE__, __ASSERT_FUNCTION))
#define __RLIM_T_TYPE __SYSCALL_ULONG_TYPE
#define __SYSMACROS_DM(symbol) __SYSMACROS_DM1 (In the GNU C Library, #symbol is defined\n by <sys/sysmacros.h>. For historical compatibility, it is\n currently defined by <sys/types.h> as well, but we plan to\n remove this soon. To use #symbol, include <sys/sysmacros.h>\n directly. If you did not intend to use a system-defined macro\n #symbol, you should undefine it after including <sys/types.h>.)
#define le64toh(x) __uint64_identity (x)
#define _GLIBCXX_MAKE_MOVE_IF_NOEXCEPT_ITERATOR(_Iter) std::__make_move_if_noexcept_iterator(_Iter)
#define FILENAME_MAX 4096
#define CLONE_FS 0x00000200
#define GTEST_CONCAT_TOKEN_(foo,bar) GTEST_CONCAT_TOKEN_IMPL_(foo, bar)
#define __cpp_return_type_deduction 201304
#define LC_COLLATE_MASK (1 << __LC_COLLATE)
#define __cleanup_fct_attribute 
#define FLT_MIN_EXP __FLT_MIN_EXP__
#define EXPECT_PRED_FORMAT1(pred_format,v1) GTEST_PRED_FORMAT1_(pred_format, v1, GTEST_NONFATAL_FAILURE_)
#define __ino_t_defined 
#define _SC_XOPEN_REALTIME_THREADS _SC_XOPEN_REALTIME_THREADS
#define __k8__ 1
#define __INTPTR_TYPE__ long int
#define __UINT16_TYPE__ short unsigned int
#define __WCHAR_TYPE__ int
#define __SIZEOF_FLOAT__ 4
#define _IOS_TRUNC 16
#define _GLIBCXX_PACKAGE_TARNAME "libstdc++"
#define _SC_XOPEN_ENH_I18N _SC_XOPEN_ENH_I18N
#define _SC_LEVEL1_ICACHE_SIZE _SC_LEVEL1_ICACHE_SIZE
#define ____sigval_t_defined 
#define __HAVE_COLUMN 
#define _POSIX_MEMLOCK_RANGE 200809L
#define S_IEXEC S_IXUSR
#define __stub_fdetach 
#define ELIBEXEC 83
#define __pic__ 2
#define __PTHREAD_RWLOCK_ELISION_EXTRA 0, { 0, 0, 0, 0, 0, 0, 0 }
#define __W_CONTINUED 0xffff
#define __UINTPTR_MAX__ 0xffffffffffffffffUL
#define __S_IFLNK 0120000
#define _GLIBCXX_IOSFWD 1
#define __INT_FAST64_WIDTH__ 64
#define __DEC64_MIN_EXP__ (-382)
#define RE_HAT_LISTS_NOT_NEWLINE (RE_DOT_NOT_NULL << 1)
#define __stub_chflags 
#define CLONE_IO 0x80000000
#define CLOCK_BOOTTIME 7
#define _GLIBCXX_USE_CXX11_ABI 1
#define INT_LEAST64_MIN (-__INT64_C(9223372036854775807)-1)
#define EXPECT_FALSE(condition) GTEST_TEST_BOOLEAN_(!(condition), #condition, true, false, GTEST_NONFATAL_FAILURE_)
#define __SI_PAD_SIZE ((__SI_MAX_SIZE / sizeof (int)) - 4)
#define EDOTDOT 73
#define __cpp_decltype 200707
#define __BYTE_ORDER __LITTLE_ENDIAN
#define FP_XSTATE_MAGIC2_SIZE sizeof(FP_XSTATE_MAGIC2)
#define _GLIBCXX_USE_C99 1
#define __FLT32_DECIMAL_DIG__ 9
#define _GLIBCXX_DEFAULT_ABI_TAG _GLIBCXX_ABI_TAG_CXX11
#define GTEST_TEST(test_case_name,test_name) GTEST_TEST_(test_case_name, test_name, ::testing::Test, ::testing::internal::GetTestTypeId())
#define INT64_WIDTH 64
#define _GLIBCXX_FORWARD(_Tp,__val) std::forward<_Tp>(__val)
#define __INT_FAST64_MAX__ 0x7fffffffffffffffL
#define __GCC_ATOMIC_TEST_AND_SET_TRUEVAL 1
#define CLONE_NEWIPC 0x08000000
#define INT_LEAST16_WIDTH 16
#define STA_PPSTIME 0x0004
#define BUFSIZ _IO_BUFSIZ
#define major(dev) __SYSMACROS_DM (major) gnu_dev_major (dev)
#define __FLT_DIG__ 6
#define _GLIBCXX_HAVE_EIDRM 1
#define _WCTYPE_H 1
#define _GLIBCXX_HAVE_ICONV 1
#define INT_FAST32_MAX (9223372036854775807L)
#define REG_CSGSFS REG_CSGSFS
#define _GLIBCXX_CONSTEXPR constexpr
#define __PTHREAD_COMPAT_PADDING_MID 
#define RE_INTERVALS (RE_HAT_LISTS_NOT_NEWLINE << 1)
#define _SC_NL_SETMAX _SC_NL_SETMAX
#define __FLT64X_MAX_EXP__ 16384
#define _WCHAR_T_DECLARED 
#define __UINT_FAST64_TYPE__ long unsigned int
#define __glibcxx_class_requires(_a,_b) 
#define CLONE_UNTRACED 0x00800000
#define _IO_putc_unlocked(_ch,_fp) (_IO_BE ((_fp)->_IO_write_ptr >= (_fp)->_IO_write_end, 0) ? __overflow (_fp, (unsigned char) (_ch)) : (unsigned char) (*(_fp)->_IO_write_ptr++ = (_ch)))
#define si_utime _sifields._sigchld.si_utime
#define _POSIX2_CHAR_TERM 200809L
#define WNOWAIT 0x01000000
#define GTEST_HAS_TYPED_TEST 1
#define _POSIX_VERSION 200809L
#define EXIT_SUCCESS 0
#define __LDBL_REDIR_DECL(name) 
#define _GLIBCXX_HAVE_STRTOF 1
#define MOD_FREQUENCY ADJ_FREQUENCY
#define _SC_2_PBS _SC_2_PBS
#define SA_NODEFER 0x40000000
#define UINT_FAST8_MAX (255)
#define __mode_t_defined 
#define _GLIBCXX_HAVE_MEMORY_H 1
#define __INT_MAX__ 0x7fffffff
#define S_IXUSR __S_IEXEC
#define _CS_POSIX_V7_ILP32_OFFBIG_LINTFLAGS _CS_POSIX_V7_ILP32_OFFBIG_LINTFLAGS
#define F_TLOCK 2
#define __amd64__ 1
#define _IO_PENDING_OUTPUT_COUNT(_fp) ((_fp)->_IO_write_ptr - (_fp)->_IO_write_base)
#define __glibcxx_requires_partitioned_upper_pred(_First,_Last,_Value,_Pred) 
#define PTRDIFF_WIDTH __WORDSIZE
#define __CPU_ALLOC(count) __sched_cpualloc (count)
#define RE_BK_PLUS_QM (RE_BACKSLASH_ESCAPE_IN_LISTS << 1)
#define STDIN_FILENO 0
#define S_IWOTH (S_IWGRP >> 3)
#define si_band _sifields._sigpoll.si_band
#define STA_PPSSIGNAL 0x0100
#define DBL_MAX_EXP __DBL_MAX_EXP__
#define S_IREAD S_IRUSR
#define __ILP32_OFF32_LDFLAGS "-m32"
#define __S16_TYPE short int
#define MY_STRINGS_H 
#define FPE_FLTDIV FPE_FLTDIV
#define __bos(ptr) __builtin_object_size (ptr, __USE_FORTIFY_LEVEL > 1)
#define _LOCALE_H 1
#define ADJ_TICK 0x4000
#define EXPECT_GT(val1,val2) EXPECT_PRED_FORMAT2(::testing::internal::CmpHelperGT, val1, val2)
#define _T_SIZE_ 
#define UINT_LEAST8_WIDTH 8
#define ENOTRECOVERABLE 131
#define _POSIX_THREAD_ROBUST_PRIO_INHERIT 200809L
#define _SC_V6_ILP32_OFFBIG _SC_V6_ILP32_OFFBIG
#define __FD_SET(d,set) ((void) (__FDS_BITS (set)[__FD_ELT (d)] |= __FD_MASK (d)))
#define __SIGRTMAX 64
#define __WNOTHREAD 0x20000000
#define _G_va_list __gnuc_va_list
#define __LC_NUMERIC 1
#define _IOS_INPUT 1
#define __USE_LARGEFILE64 1
#define GTEST_PRED4_(pred,v1,v2,v3,v4,on_failure) GTEST_ASSERT_(::testing::AssertPred4Helper(#pred, #v1, #v2, #v3, #v4, pred, v1, v2, v3, v4), on_failure)
#define __INT64_TYPE__ long int
#define _GLIBCXX_STRING 1
#define __FLT_MAX_EXP__ 128
#define LDBL_MAX_10_EXP __LDBL_MAX_10_EXP__
#define _GLIBCXX_HAVE_STRXFRM_L 1
#define ADD_FAILURE_AT(file,line) GTEST_MESSAGE_AT_(file, line, "Failed", ::testing::TestPartResult::kNonFatalFailure)
#define _SC_AIO_PRIO_DELTA_MAX _SC_AIO_PRIO_DELTA_MAX
#define __glibcxx_integral_traps true
#define __gid_t_defined 
#define ENETUNREACH 101
#define EXDEV 18
#define LC_MESSAGES __LC_MESSAGES
#define __gthrw(name) __gthrw2(__gthrw_ ## name,name,name)
#define _GLIBCXX_USE_SC_NPROCESSORS_ONLN 1
#define S_IRGRP (S_IRUSR >> 3)
#define UINT32_C(c) c ## U
#define S_IFLNK __S_IFLNK
#define __ORDER_BIG_ENDIAN__ 4321
#define _SC_THREAD_PRIORITY_SCHEDULING _SC_THREAD_PRIORITY_SCHEDULING
#define EHOSTUNREACH 113
#define _STL_ALGO_H 1
#define _SC_REGEX_VERSION _SC_REGEX_VERSION
#define _GLIBCXX17_INLINE 
#define __DBL_MANT_DIG__ 53
#define ___int_size_t_h 
#define __TIMER_T_TYPE void *
#define _GLIBCXX_END_NAMESPACE_LDBL_OR_CXX11 _GLIBCXX_END_NAMESPACE_CXX11
#define _POSIX_REALTIME_SIGNALS 200809L
#define _POSIX_V6_LP64_OFF64 1
#define __cpp_inheriting_constructors 201511
#define __WIFCONTINUED(status) ((status) == __W_CONTINUED)
#define _GLIBCXX_HAVE_FENV_H 1
#define _GLIBCXX_HAVE_STDBOOL_H 1
#define __SIZEOF_FLOAT128__ 16
#define __INT_LEAST64_MAX__ 0x7fffffffffffffffL
#define _GLIBCXX_DEBUG_PEDASSERT(_Condition) 
#define UINT_FAST64_MAX (__UINT64_C(18446744073709551615))
#define __clock_t_defined 1
#define INT_FAST32_MIN (-9223372036854775807L-1)
#define __GLIBC_MINOR__ 27
#define CLONE_DETACHED 0x00400000
#define __DEC64_MIN__ 1E-383DD
#define __WINT_TYPE__ unsigned int
#define __UINT_LEAST32_TYPE__ unsigned int
#define EDQUOT 122
#define __SIZEOF_SHORT__ 2
#define GTEST_DECLARE_string_(name) GTEST_API_ extern ::std::string GTEST_FLAG(name)
#define STDOUT_FILENO 1
#define __SSE__ 1
#define _GLIBCXX_CXX_LOCALE_H 1
#define _GLIBCXX_OSTREAM 1
#define _CTYPE_H 1
#define _SC_SHARED_MEMORY_OBJECTS _SC_SHARED_MEMORY_OBJECTS
#define SIGKILL 9
#define _GLIBCXX_CERRNO 1
#define __intptr_t_defined 
#define __LDBL_MIN_EXP__ (-16381)
#define _GLIBCXX_HAVE_LOGF 1
#define MOD_TAI ADJ_TAI
#define _IO_va_list __gnuc_va_list
#define _GLIBCXX_HAVE_LOGL 1
#define _CS_XBS5_ILP32_OFFBIG_LINTFLAGS _CS_XBS5_ILP32_OFFBIG_LINTFLAGS
#define SIGPOLL 29
#define LDBL_MANT_DIG __LDBL_MANT_DIG__
#define _GLIBCXX_HAVE_EXPF 1
#define _GLIBCXX_HAVE_EXPL 1
#define __FLT64_MAX__ 1.79769313486231570814527423731704357e+308F64
#define _POSIX_VDISABLE '\0'
#define LDBL_MAX __LDBL_MAX__
#define _GLIBCXX_NOEXCEPT_PARM 
#define UINT16_MAX (65535)
#define SIGCLD SIGCHLD
#define _GLIBCXX_TXN_SAFE 
#define _NEW 
#define ENOEXEC 8
#define _SC_SIGNALS _SC_SIGNALS
#define __WINT_WIDTH__ 32
#define __REDIRECT_NTHNL(name,proto,alias) name proto __THROWNL __asm__ (__ASMNAME (#alias))
#define CLONE_VM 0x00000100
#define INTPTR_WIDTH __WORDSIZE
#define _POSIX_THREAD_PRIO_INHERIT 200809L
#define __INT_LEAST8_MAX__ 0x7f
#define __extern_inline extern __inline __attribute__ ((__gnu_inline__))
#define __USE_POSIX199309 1
#define _SC_CLOCK_SELECTION _SC_CLOCK_SELECTION
#define SIGUSR2 12
#define __FLT32X_MAX_10_EXP__ 308
#define DECIMAL_DIG __DECIMAL_DIG__
#define ESRCH 3
#define ____mbstate_t_defined 1
#define ADJ_STATUS 0x0010
#define CPU_SET_S(cpu,setsize,cpusetp) __CPU_SET_S (cpu, setsize, cpusetp)
#define _GLIBCXX_HAVE_LOG10F 1
#define __SIGRTMIN 32
#define _GLIBCXX_HAVE_LOG10L 1
#define SIGTSTP 20
#define __SIZEOF_INT128__ 16
#define EXPECT_PRED_FORMAT2(pred_format,v1,v2) GTEST_PRED_FORMAT2_(pred_format, v1, v2, GTEST_NONFATAL_FAILURE_)
#define __PTHREAD_MUTEX_HAVE_PREV 1
#define _SC_2_FORT_RUN _SC_2_FORT_RUN
#define _GLIBCXX_TYPE_TRAITS 1
#define _STL_ITERATOR_BASE_FUNCS_H 1
#define __BLKCNT64_T_TYPE __SQUAD_TYPE
#define CPU_ISSET_S(cpu,setsize,cpusetp) __CPU_ISSET_S (cpu, setsize, cpusetp)
#define _GLIBCXX_RANGE_ACCESS_H 1
#define __LDBL_MAX_10_EXP__ 4932
#define _POSIX_SEMAPHORES 200809L
#define __ATOMIC_RELAXED 0
#define _BITS_SIGNUM_GENERIC_H 1
#define _SC_XBS5_LP64_OFF64 _SC_XBS5_LP64_OFF64
#define __FSBLKCNT_T_TYPE __SYSCALL_ULONG_TYPE
#define __DBL_EPSILON__ double(2.22044604925031308084726333618164062e-16L)
#define EMSGSIZE 90
#define PTHREAD_PROCESS_PRIVATE PTHREAD_PROCESS_PRIVATE
#define SA_ONESHOT SA_RESETHAND
#define GTEST_HAS_SEH 0
#define _LOCALE_FACETS_NONIO_TCC 1
#define __stub_stty 
#define _SC_THREAD_STACK_MIN _SC_THREAD_STACK_MIN
#define le16toh(x) __uint16_identity (x)
#define __FLT128_MIN__ 3.36210314311209350626267781732175260e-4932F128
#define __GTHREAD_HAS_COND 1
#define _SIZET_ 
#define __glibcxx_requires_irreflexive_pred2(_First,_Last,_Pred) 
#define _SC_USER_GROUPS _SC_USER_GROUPS
#define _PC_ALLOC_SIZE_MIN _PC_ALLOC_SIZE_MIN
#define SIGPIPE 13
#define _LP64 1
#define _LOCALE_FACETS_TCC 1
#define EXPECT_PRED_FORMAT4(pred_format,v1,v2,v3,v4) GTEST_PRED_FORMAT4_(pred_format, v1, v2, v3, v4, GTEST_NONFATAL_FAILURE_)
#define __REDIRECT_NTH_LDBL(name,proto,alias) __REDIRECT_NTH (name, proto, alias)
#define __UINT8_C(c) c
#define _GLIBCXX_HAVE_CEILF 1
#define REG_R8 REG_R8
#define _GLIBCXX_HAVE_CEILL 1
#define _BITS_STDINT_INTN_H 1
#define __FLT64_MAX_EXP__ 1024
#define MOD_MAXERROR ADJ_MAXERROR
#define _SC_2_FORT_DEV _SC_2_FORT_DEV
#define __S_IFSOCK 0140000
#define __GTHREADS 1
#define __INT_LEAST32_TYPE__ int
#define _CS_POSIX_V7_LPBIG_OFFBIG_LINTFLAGS _CS_POSIX_V7_LPBIG_OFFBIG_LINTFLAGS
#define ECHRNG 44
#define CPU_FREE(cpuset) __CPU_FREE (cpuset)
#define _LFS_LARGEFILE 1
#define DBL_MIN __DBL_MIN__
#define STA_CLK 0x8000
#define __wchar_t__ 
#define RE_LIMITED_OPS (RE_INTERVALS << 1)
#define SEEK_END 2
#define __SIZEOF_WCHAR_T__ 4
#define _GLIBCXX_NOTHROW _GLIBCXX_USE_NOEXCEPT
#define _GLIBCXX_USE_C99_WCHAR _GLIBCXX11_USE_C99_WCHAR
#define _PTHREAD_H 1
#define _GLIBCXX_GUARD_SET(x) *(char *) (x) = 1
#define _ISOC99_SOURCE 1
#define _GLIBCXX_VECTOR 1
#define ENOLCK 37
#define ENFILE 23
#define _SC_REGEXP _SC_REGEXP
#define _STRING_CONVERSIONS_H 1
#define __REDIRECT_NTH(name,proto,alias) name proto __THROW __asm__ (__ASMNAME (#alias))
#define __ILP32_OFF32_CFLAGS "-m32"
#define _ASM_GENERIC_ERRNO_H 
#define __LC_COLLATE 3
#define _IO_RIGHT 04
#define EAGAIN 11
#define _GLIBCXX_STD_C std
#define _SC_IOV_MAX _SC_IOV_MAX
#define _SC_PASS_MAX _SC_PASS_MAX
#define _GLIBCXX_READ_MEM_BARRIER __atomic_thread_fence (__ATOMIC_ACQUIRE)
#define _LARGEFILE64_SOURCE 1
#define _GLIBCXX_USE_C99_STDINT_TR1 1
#define _SC_V7_ILP32_OFF32 _SC_V7_ILP32_OFF32
#define _STDDEF_H_ 
#define __LC_NAME 8
#define __stack_t_defined 1
#define __stub___compat_bdflush 
#define MB_CUR_MAX (__ctype_get_mb_cur_max ())
#define SIGXCPU 24
#define __FLT128_HAS_QUIET_NAN__ 1
#define be64toh(x) __bswap_64 (x)
#define ENOSYS 38
#define __FD_ISSET(d,set) ((__FDS_BITS (set)[__FD_ELT (d)] & __FD_MASK (d)) != 0)
#define EXPECT_TRUE(condition) GTEST_TEST_BOOLEAN_((condition), #condition, false, true, GTEST_NONFATAL_FAILURE_)
#define GTEST_USE_OWN_FLAGFILE_FLAG_ 1
#define RE_SYNTAX_SED RE_SYNTAX_POSIX_BASIC
#define _SC_LOGIN_NAME_MAX _SC_LOGIN_NAME_MAX
#define SA_RESTART 0x10000000
#define __bool_true_false_are_defined 1
#define _GLIBCXX_IOMANIP 1
#define GTEST_NONFATAL_FAILURE_(message) GTEST_MESSAGE_(message, ::testing::TestPartResult::kNonFatalFailure)
#define _GLIBCXX_STDIO_SEEK_END 2
#define __INT_FAST8_TYPE__ signed char
#define LDBL_EPSILON __LDBL_EPSILON__
#define __PID_T_TYPE __S32_TYPE
#define INSTANTIATE_TYPED_TEST_CASE_P(Prefix,CaseName,Types) bool gtest_ ##Prefix ##_ ##CaseName GTEST_ATTRIBUTE_UNUSED_ = ::testing::internal::TypeParameterizedTestCase<CaseName, GTEST_CASE_NAMESPACE_(CaseName)::gtest_AllTests_, ::testing::internal::TypeList< Types >::type>::Register( #Prefix, ::testing::internal::CodeLocation(__FILE__, __LINE__), &GTEST_TYPED_TEST_CASE_P_STATE_(CaseName), #CaseName, GTEST_REGISTERED_TEST_NAMES_(CaseName))
#define _GLIBCXX_HAVE_TANF 1
#define _STATBUF_ST_NSEC 
#define _GLIBCXX_USE_ST_MTIM 1
#define __FLT64X_MIN__ 3.36210314311209350626267781732175260e-4932F64x
#define _BITS_LOCALE_H 1
#define _GLIBCXX_USE_NLS 1
#define _SC_CHILD_MAX _SC_CHILD_MAX
#define _GLIBCXX_HAVE_STDLIB_H 1
#define _STL_BVECTOR_H 1
#define GTEST_USES_POSIX_RE 1
#define S_ISGID __S_ISGID
#define REG_EXTENDED 1
#define _PC_MAX_CANON _PC_MAX_CANON
#define _THREAD_SHARED_TYPES_H 1
#define GTEST_OS_LINUX 1
#define __f128(x) x ##q
#define __glibcxx_requires_sorted(_First,_Last) 
#define GTEST_MESSAGE_(message,result_type) GTEST_MESSAGE_AT_(__FILE__, __LINE__, message, result_type)
#define si_value _sifields._rt.si_sigval
#define PTHREAD_CREATE_JOINABLE PTHREAD_CREATE_JOINABLE
#define GTEST_HAS_CXXABI_H_ 1
#define __GNUC_STDC_INLINE__ 1
#define GTEST_INCLUDE_GTEST_GTEST_PARAM_TEST_H_ 
#define EXPECT_PRED2(pred,v1,v2) GTEST_PRED2_(pred, v1, v2, GTEST_NONFATAL_FAILURE_)
#define P_tmpdir "/tmp"
#define S_IFBLK __S_IFBLK
#define GTEST_HAS_STD_BEGIN_AND_END_ 1
#define __FLT64_HAS_DENORM__ 1
#define _POSIX_MESSAGE_PASSING 200809L
#define __cpp_lib_make_unique 201304
#define __WORDSIZE_TIME64_COMPAT32 1
#define _GLIBCXX_DEPRECATED __attribute__ ((__deprecated__))
#define EPFNOSUPPORT 96
#define ENOTSUP EOPNOTSUPP
#define ESRMNT 69
#define __FLT32_EPSILON__ 1.19209289550781250000000000000000000e-7F32
#define _IO_stdin ((_IO_FILE*)(&_IO_2_1_stdin_))
#define EXPECT_THROW(statement,expected_exception) GTEST_TEST_THROW_(statement, expected_exception, GTEST_NONFATAL_FAILURE_)
#define GTEST_WIDE_STRING_USES_UTF16_ (GTEST_OS_WINDOWS || GTEST_OS_CYGWIN || GTEST_OS_SYMBIAN || GTEST_OS_AIX)
#define INTMAX_MAX (__INT64_C(9223372036854775807))
#define _SC_THREAD_ROBUST_PRIO_PROTECT _SC_THREAD_ROBUST_PRIO_PROTECT
#define EHOSTDOWN 112
#define _IO_cleanup_region_start(_fct,_fp) 
#define __gthrw_pragma(pragma) 
#define _IO_NO_READS 4
#define _GLIBCXX_HAVE_AS_SYMVER_DIRECTIVE 1
#define _CS_LFS64_LDFLAGS _CS_LFS64_LDFLAGS
#define PTHREAD_ONCE_INIT 0
#define _IO_DEC 020
#define _BITS_TYPES___LOCALE_T_H 1
#define __DBL_DECIMAL_DIG__ 17
#define __STDC_UTF_32__ 1
#define _SC_2_PBS_MESSAGE _SC_2_PBS_MESSAGE
#define GTEST_PATH_SEP_ "/"
#define __INT_FAST8_WIDTH__ 8
#define _SC_2_PBS_TRACK _SC_2_PBS_TRACK
#define __FXSR__ 1
#define __GTHREAD_COND_INIT PTHREAD_COND_INITIALIZER
#define EDEADLOCK EDEADLK
#define _GLIBCXX_TUPLE 1
#define CLOCKS_PER_SEC ((__clock_t) 1000000)
#define __DEC_EVAL_METHOD__ 2
#define _SIZE_T 
#define _POSIX2_VERSION __POSIX2_THIS_VERSION
#define ENOANO 55
#define _GLIBCXX_USE_GETTIMEOFDAY 1
#define __FLT32X_MAX__ 1.79769313486231570814527423731704357e+308F32x
#define __WIFSTOPPED(status) (((status) & 0xff) == 0x7f)
#define __ULONG32_TYPE unsigned int
#define _GLIBCXX_BITS_UNIFORM_INT_DIST_H 
#define _LOCALE_CONV_H 1
#define _REGEX_H 1
#define STA_PPSJITTER 0x0200
#define _POSIX2_SW_DEV __POSIX2_THIS_VERSION
#define __siginfo_t_defined 1
#define _BACKWARD_BINDERS_H 1
#define _GLIBCXX_FAST_MATH 0
#define _GXX_NULLPTR_T 
#define UTIME_OMIT ((1l << 30) - 2l)
#define __glibcxx_class_requires2(_a,_b,_c) 
#define __glibcxx_class_requires3(_a,_b,_c,_d) 
#define __glibcxx_class_requires4(_a,_b,_c,_d,_e) 
#define CLONE_CHILD_CLEARTID 0x00200000
#define FOPEN_MAX 16
#define __S_TYPEISMQ(buf) ((buf)->st_mode - (buf)->st_mode)
#define __suseconds_t_defined 
#define SIGFPE 8
#define __W_STOPCODE(sig) ((sig) << 8 | 0x7f)
#define _IO_STDIO 040000
#define _STATBUF_ST_BLKSIZE 
#define __glibcxx_requires_string(_String) 
#define _SC_LEVEL1_DCACHE_ASSOC _SC_LEVEL1_DCACHE_ASSOC
#define GTEST_FLAG_SAVER_ ::testing::internal::GTestFlagSaver
#define _GLIBCXX_HAVE_LDEXPL 1
#define SIGPWR 30
#define _STL_MULTISET_H 1
#define GTEST_HAS_STD_MOVE_ 1
#define GTEST_TEST_NO_FATAL_FAILURE_(statement,fail) GTEST_AMBIGUOUS_ELSE_BLOCKER_ if (::testing::internal::AlwaysTrue()) { ::testing::internal::HasNewFatalFailureHelper gtest_fatal_failure_checker; GTEST_SUPPRESS_UNREACHABLE_CODE_WARNING_BELOW_(statement); if (gtest_fatal_failure_checker.has_new_fatal_failure()) { goto GTEST_CONCAT_TOKEN_(gtest_label_testnofatal_, __LINE__); } } else GTEST_CONCAT_TOKEN_(gtest_label_testnofatal_, __LINE__): fail("Expected: " #statement " doesn't generate new fatal " "failures in the current thread.\n" "  Actual: it does.")
#define _POSIX_REENTRANT_FUNCTIONS 1
#define ASSERT_NO_THROW(statement) GTEST_TEST_NO_THROW_(statement, GTEST_FATAL_FAILURE_)
#define _SC_DEVICE_SPECIFIC _SC_DEVICE_SPECIFIC
#define _GCC_SIZE_T 
#define FLT_EVAL_METHOD __FLT_EVAL_METHOD__
#define _GLIBCXX_CWCHAR 1
#define _POSIX_THREAD_PRIO_PROTECT 200809L
#define __INO64_T_TYPE __UQUAD_TYPE
#define _SC_AVPHYS_PAGES _SC_AVPHYS_PAGES
#define _CS_POSIX_V6_ILP32_OFF32_CFLAGS _CS_POSIX_V6_ILP32_OFF32_CFLAGS
#define __cpp_runtime_arrays 198712
#define L_SET SEEK_SET
#define RE_NO_POSIX_BACKTRACKING (RE_UNMATCHED_RIGHT_PAREN_ORD << 1)
#define __UINT64_TYPE__ long unsigned int
#define __USE_XOPEN2K8XSI 1
#define __UINT32_C(c) c ## U
#define __FD_ELT(d) ((d) / __NFDBITS)
#define PTHREAD_SCOPE_SYSTEM PTHREAD_SCOPE_SYSTEM
#define EXPECT_PRED_FORMAT5(pred_format,v1,v2,v3,v4,v5) GTEST_PRED_FORMAT5_(pred_format, v1, v2, v3, v4, v5, GTEST_NONFATAL_FAILURE_)
#define __INTMAX_MAX__ 0x7fffffffffffffffL
#define __cpp_alias_templates 200704
#define EISCONN 106
#define __BYTE_ORDER__ __ORDER_LITTLE_ENDIAN__
#define __GTHREAD_MUTEX_INIT_FUNCTION __gthread_mutex_init_function
#define _STL_ITERATOR_H 1
#define _FUNCTIONAL_HASH_H 1
#define S_ISLNK(mode) __S_ISTYPE((mode), __S_IFLNK)
#define __size_t__ 
#define _SYS_SELECT_H 1
#define BUS_MCEERR_AO BUS_MCEERR_AO
#define _SC_GETGR_R_SIZE_MAX _SC_GETGR_R_SIZE_MAX
#define ASSERT_FALSE(condition) GTEST_TEST_BOOLEAN_(!(condition), #condition, true, false, GTEST_FATAL_FAILURE_)
#define _IO_BUFSIZ _G_BUFSIZ
#define __FLT_DENORM_MIN__ 1.40129846432481707092372958328991613e-45F
#define SIGEV_THREAD SIGEV_THREAD
#define CPU_ALLOC_SIZE(count) __CPU_ALLOC_SIZE (count)
#define __cpp_lib_tuple_element_t 201402
#define _ENDIAN_H 1
#define _SC_INT_MAX _SC_INT_MAX
#define GTEST_ASSERT_EQ(val1,val2) ASSERT_PRED_FORMAT2(::testing::internal:: EqHelper<GTEST_IS_NULL_LITERAL_(val1)>::Compare, val1, val2)
#define NGREG __NGREG
#define sigev_notify_attributes _sigev_un._sigev_thread._attribute
#define _SC_SYNCHRONIZED_IO _SC_SYNCHRONIZED_IO
#define INTMAX_MIN (-__INT64_C(9223372036854775807)-1)
#define CPU_OR(destset,srcset1,srcset2) __CPU_OP_S (sizeof (cpu_set_t), destset, srcset1, srcset2, |)
#define __LC_CTYPE 0
#define _GLIBCXX_HAVE_FINITE 1
#define EUSERS 87
#define SI_ASYNCNL SI_ASYNCNL
#define GTEST_HAS_RTTI 1
#define __INT8_MAX__ 0x7f
#define _SC_LEVEL4_CACHE_ASSOC _SC_LEVEL4_CACHE_ASSOC
#define __LONG_WIDTH__ 64
#define CPU_EQUAL_S(setsize,cpusetp1,cpusetp2) __CPU_EQUAL_S (setsize, cpusetp1, cpusetp2)
#define __PIC__ 2
#define F_LOCK 1
#define _GLIBCXX_HAVE_EOVERFLOW 1
#define SIG_ERR ((__sighandler_t) -1)
#define __UINT_FAST32_TYPE__ long unsigned int
#define FD_ISSET(fd,fdsetp) __FD_ISSET (fd, fdsetp)
#define _SC_XOPEN_STREAMS _SC_XOPEN_STREAMS
#define GTEST_NAME_ "Google Test"
#define _GLIBCXX_HAVE_EXCEPTION_PTR_SINCE_GCC46 1
#define RE_CHAR_CLASSES (RE_BK_PLUS_QM << 1)
#define POLL_ERR POLL_ERR
#define PTHREAD_EXPLICIT_SCHED PTHREAD_EXPLICIT_SCHED
#define __INO_T_MATCHES_INO64_T 1
#define _GLIBCXX_HAVE_FLOAT_H 1
#define __CHAR32_TYPE__ unsigned int
#define ENOPROTOOPT 92
#define _SC_PII_INTERNET_STREAM _SC_PII_INTERNET_STREAM
#define _IO_uid_t __uid_t
#define _CS_POSIX_V7_LP64_OFF64_CFLAGS _CS_POSIX_V7_LP64_OFF64_CFLAGS
#define _CONCEPT_CHECK_H 1
#define EXPECT_DEBUG_DEATH(statement,regex) EXPECT_DEATH(statement, regex)
#define __FLT_MAX__ 3.40282346638528859811704183484516925e+38F
#define POLL_IN POLL_IN
#define GTEST_BIND_(TmplSel,T) TmplSel::template Bind<T>::type
#define _GLIBCXX_HAVE_VFWSCANF 1
#define __REPB_PREFIX(name) name
#define _GLIBCXX_USE_ALLOCATOR_NEW 1
#define WINT_MIN (0u)
#define __fsfilcnt_t_defined 
#define __blkcnt_t_defined 
#define __HAVE_FLOAT128 1
#define GTEST_INCLUDE_GTEST_INTERNAL_GTEST_FILEPATH_H_ 
#define SCOPED_TRACE(message) ::testing::internal::ScopedTrace GTEST_CONCAT_TOKEN_(gtest_trace_, __LINE__)( __FILE__, __LINE__, ::testing::Message() << (message))
#define _POSIX_SPAWN 200809L
#define ECOMM 70
#define UINT_LEAST32_MAX (4294967295U)
#define CPU_XOR(destset,srcset1,srcset2) __CPU_OP_S (sizeof (cpu_set_t), destset, srcset1, srcset2, ^)
#define __cpp_constexpr 201304
#define _GLIBCXX98_USE_C99_STDIO 1
#define SA_NOCLDWAIT 2
#define __attribute_format_arg__(x) __attribute__ ((__format_arg__ (x)))
#define _IO_SKIPWS 01
#define minor(dev) __SYSMACROS_DM (minor) gnu_dev_minor (dev)
#define POLL_PRI POLL_PRI
#define _ISbit(bit) ((bit) < 8 ? ((1 << (bit)) << 8) : ((1 << (bit)) >> 8))
#define _GLIBCXX_HAVE_SYS_TYPES_H 1
#define _SC_PII_INTERNET _SC_PII_INTERNET
#define ERFKILL 132
#define UINT_LEAST32_WIDTH 32
#define S_TYPEISSEM(buf) __S_TYPEISSEM(buf)
#define __INT32_TYPE__ int
#define DBL_DIG __DBL_DIG__
#define _SC_2_SW_DEV _SC_2_SW_DEV
#define __SIZEOF_DOUBLE__ 8
#define __cpp_exceptions 199711
#define __FLT_MIN_10_EXP__ (-37)
#define _SYS_SYSMACROS_H 1
#define _SC_INT_MIN _SC_INT_MIN
#define GTEST_INTENTIONAL_CONST_COND_POP_() GTEST_DISABLE_MSC_WARNINGS_POP_()
#define __FLT64_MIN__ 2.22507385850720138309023271733240406e-308F64
#define S_ISSOCK(mode) __S_ISTYPE((mode), __S_IFSOCK)
#define _IO_BE(expr,res) __builtin_expect ((expr), res)
#define __INT_LEAST32_WIDTH__ 32
#define __SWORD_TYPE long int
#define ENOTEMPTY 39
#define __INTMAX_TYPE__ long int
#define _GLIBCXX11_USE_C99_MATH 1
#define be16toh(x) __bswap_16 (x)
#define GTEST_INCLUDE_GTEST_GTEST_H_ 
#define SIGINT 2
#define __PTHREAD_SPINS 0, 0
#define FRIEND_TEST(test_case_name,test_name) friend class test_case_name ##_ ##test_name ##_Test
#define __DEC128_MAX_EXP__ 6145
#define __CPU_FREE(cpuset) __sched_cpufree (cpuset)
#define __RE_TRANSLATE_TYPE unsigned char *
#define _T_SIZE 
#define _POSIX2_LOCALEDEF __POSIX2_THIS_VERSION
#define __va_arg_pack() __builtin_va_arg_pack ()
#define EWOULDBLOCK EAGAIN
#define GTEST_INCLUDE_GTEST_INTERNAL_GTEST_PORT_H_ 
#define S_IRWXG (S_IRWXU >> 3)
#define _GLIBCXX_HAVE_TLS 1
#define _GLIBCXX_HAVE_ACOSF 1
#define _GLIBCXX_HAVE_ACOSL 1
#define RE_DOT_NEWLINE (RE_CONTEXT_INVALID_OPS << 1)
#define __FLT32X_HAS_QUIET_NAN__ 1
#define __ATOMIC_CONSUME 1
#define _GLIBCXX_CSTDINT 1
#define FP_XSTATE_MAGIC2 0x46505845U
#define __GNUC_MINOR__ 3
#define __GLIBCXX_TYPE_INT_N_0 __int128
#define __INT_FAST16_WIDTH__ 64
#define __UINTMAX_MAX__ 0xffffffffffffffffUL
#define _SC_LEVEL1_ICACHE_LINESIZE _SC_LEVEL1_ICACHE_LINESIZE
#define _SC_TRACE_SYS_MAX _SC_TRACE_SYS_MAX
#define CPU_CLR_S(cpu,setsize,cpusetp) __CPU_CLR_S (cpu, setsize, cpusetp)
#define __DEC32_MANT_DIG__ 7
#define LITTLE_ENDIAN __LITTLE_ENDIAN
#define __FLT32X_DENORM_MIN__ 4.94065645841246544176568792868221372e-324F32x
#define _GLIBCXX_MAKE_MOVE_ITERATOR(_Iter) std::make_move_iterator(_Iter)
#define _STL_VECTOR_H 1
#define ELIBBAD 80
#define INTMAX_C(c) c ## L
#define __key_t_defined 
#define __glibc_clang_prereq(maj,min) 0
#define ILL_PRVOPC ILL_PRVOPC
#define __LP64_OFF64_CFLAGS "-m64"
#define ECONNREFUSED 111
#define RE_DOT_NOT_NULL (RE_DOT_NEWLINE << 1)
#define si_call_addr _sifields._sigsys._call_addr
#define __LP64_OFF64_LDFLAGS "-m64"
#define INT_FAST64_MIN (-__INT64_C(9223372036854775807)-1)
#define __CPU_ISSET_S(cpu,setsize,cpusetp) (__extension__ ({ size_t __cpu = (cpu); __cpu / 8 < (setsize) ? ((((const __cpu_mask *) ((cpusetp)->__bits))[__CPUELT (__cpu)] & __CPUMASK (__cpu))) != 0 : 0; }))
#define _GTHREAD_USE_MUTEX_TIMEDLOCK 1
#define REG_R13 REG_R13
#define _STDINT_H 1
#define UINT_FAST8_WIDTH 8
#define __DBL_MAX_10_EXP__ 308
#define __FILE_defined 1
#define __LDBL_DENORM_MIN__ 3.64519953188247460252840593361941982e-4951L
#define _CS_V7_WIDTH_RESTRICTED_ENVS _CS_V7_WIDTH_RESTRICTED_ENVS
#define REG_R15 REG_R15
#define __SIZEOF_PTHREAD_RWLOCKATTR_T 8
#define _CS_LFS64_LINTFLAGS _CS_LFS64_LINTFLAGS
#define _SC_CHAR_MAX _SC_CHAR_MAX
#define S_ISUID __S_ISUID
#define _CS_XBS5_LPBIG_OFFBIG_LINTFLAGS _CS_XBS5_LPBIG_OFFBIG_LINTFLAGS
#define LC_NAME __LC_NAME
#define __USE_XOPEN_EXTENDED 1
#define EXPECT_PRED_FORMAT3(pred_format,v1,v2,v3) GTEST_PRED_FORMAT3_(pred_format, v1, v2, v3, GTEST_NONFATAL_FAILURE_)
#define __CPU_SETSIZE 1024
#define _POSIX_TRACE_LOG -1
#define _GLIBCXX_HAVE_SYS_RESOURCE_H 1
#define __INT16_C(c) c
#define UINT8_WIDTH 8
#define _GLIBCXX_USE_REALPATH 1
#define __U32_TYPE unsigned int
#define _CS_POSIX_V7_ILP32_OFFBIG_LDFLAGS _CS_POSIX_V7_ILP32_OFFBIG_LDFLAGS
#define __SI_CLOCK_T __clock_t
#define __cpp_generic_lambdas 201304
#define _GLIBCXX_HAVE_SYS_IOCTL_H 1
#define FD_CLR(fd,fdsetp) __FD_CLR (fd, fdsetp)
#define SIG_IGN ((__sighandler_t) 1)
#define _CS_POSIX_V7_LP64_OFF64_LIBS _CS_POSIX_V7_LP64_OFF64_LIBS
#define GTEST_HAS_GLOBAL_WSTRING (GTEST_HAS_STD_WSTRING && GTEST_HAS_GLOBAL_STRING)
#define __STDC__ 1
#define __LC_MONETARY 4
#define _GLIBCXX_HAVE_VWSCANF 1
#define __attribute_malloc__ __attribute__ ((__malloc__))
#define __FLT32X_DIG__ 15
#define ELIBMAX 82
#define GTEST_SNPRINTF_ snprintf
#define _GLIBCXX_USE_C99_CTYPE_TR1 1
#define _SC_THREADS _SC_THREADS
#define __PTRDIFF_TYPE__ long int
#define _IO_off_t __off_t
#define _SC_FILE_ATTRIBUTES _SC_FILE_ATTRIBUTES
#define _GLIBCXX_SSTREAM 1
#define _GLIBCXX_END_NAMESPACE_CONTAINER _GLIBCXX_END_NAMESPACE_VERSION
#define __LC_IDENTIFICATION 12
#define stdin stdin
#define REG_NEWLINE (REG_ICASE << 1)
#define STA_UNSYNC 0x0040
#define __clockid_t_defined 1
#define _GLIBCXX_NOEXCEPT_IF(_COND) noexcept(_COND)
#define __attribute_noinline__ __attribute__ ((__noinline__))
#define _GLIBCXX_BEGIN_NAMESPACE_LDBL 
#define SIGRTMIN (__libc_current_sigrtmin ())
#define _BITS_STAT_H 1
#define CPU_AND_S(setsize,destset,srcset1,srcset2) __CPU_OP_S (setsize, destset, srcset1, srcset2, &)
#define _IO_HEX 0100
#define __ATOMIC_SEQ_CST 5
#define __tobody(c,f,a,args) (__extension__ ({ int __res; if (sizeof (c) > 1) { if (__builtin_constant_p (c)) { int __c = (c); __res = __c < -128 || __c > 255 ? __c : (a)[__c]; } else __res = f args; } else __res = (a)[(int) (c)]; __res; }))
#define __NCPUBITS (8 * sizeof (__cpu_mask))
#define ENOENT 2
#define EXPECT_GE(val1,val2) EXPECT_PRED_FORMAT2(::testing::internal::CmpHelperGE, val1, val2)
#define ENOPKG 65
#define __CLOCK_T_TYPE __SYSCALL_SLONG_TYPE
#define EXPECT_NEAR(val1,val2,abs_error) EXPECT_PRED_FORMAT3(::testing::internal::DoubleNearPredFormat, val1, val2, abs_error)
#define _BITS_FLOATN_H 
#define REG_ICASE (REG_EXTENDED << 1)
#define _IO_fpos64_t _G_fpos64_t
#define __throw_exception_again throw
#define __UINT32_TYPE__ unsigned int
#define REG_NOTEOL (1 << 1)
#define __FLT32X_MIN_10_EXP__ (-307)
#define __STDC_LIMIT_MACROS 
#define ASSERT_DEATH_IF_SUPPORTED(statement,regex) ASSERT_DEATH(statement, regex)
#define __UINTPTR_TYPE__ long unsigned int
#define _GLIBCXX_USE_CLOCK_MONOTONIC 1
#define __SIGEV_PAD_SIZE ((__SIGEV_MAX_SIZE / sizeof (int)) - 4)
#define _GLIBCXX_INLINE_VERSION 0
#define CLD_KILLED CLD_KILLED
#define SIGTERM 15
#define __DEC64_SUBNORMAL_MIN__ 0.000000000000001E-383DD
#define __FSFILCNT64_T_TYPE __UQUAD_TYPE
#define EBADSLT 57
#define GTEST_TEST_BOOLEAN_(expression,text,actual,expected,fail) GTEST_AMBIGUOUS_ELSE_BLOCKER_ if (const ::testing::AssertionResult gtest_ar_ = ::testing::AssertionResult(expression)) ; else fail(::testing::internal::GetBoolAssertionFailureMessage( gtest_ar_, text, #actual, #expected).c_str())
#define __DEC128_MANT_DIG__ 34
#define __LDBL_MIN_10_EXP__ (-4931)
#define EMEDIUMTYPE 124
#define EKEYREVOKED 128
#define _GLIBCXX_THROW_OR_ABORT(_EXC) (throw (_EXC))
#define __useconds_t_defined 
#define _GLIBCXX_USE_SCHED_YIELD 1
#define _SC_V7_LPBIG_OFFBIG _SC_V7_LPBIG_OFFBIG
#define __attribute_deprecated__ __attribute__ ((__deprecated__))
#define CLOCK_BOOTTIME_ALARM 9
#define __FLT128_EPSILON__ 1.92592994438723585305597794258492732e-34F128
#define _CS_POSIX_V7_WIDTH_RESTRICTED_ENVS _CS_V7_WIDTH_RESTRICTED_ENVS
#define __SI_BAND_TYPE long int
#define _SC_AIO_MAX _SC_AIO_MAX
#define __LC_TELEPHONE 10
#define __HAVE_DISTINCT_FLOAT64X 0
#define __SSE_MATH__ 1
#define __SQUAD_TYPE long int
#define X_OK 1
#define __SIZEOF_LONG_LONG__ 8
#define BUS_ADRALN BUS_ADRALN
#define _GLIBCXX_VERBOSE 1
#define _GLIBCXX_HAVE_ISINFF 1
#define _GLIBCXX_HAVE_ASINF 1
#define __cpp_user_defined_literals 200809
#define ELOOP 40
#define _GLIBCXX_HAVE_ISINFL 1
#define _GLIBCXX_HAVE_ASINL 1
#define __USE_ATFILE 1
#define __sigval_t_defined 
#define _GCC_PTRDIFF_T 
#define GTEST_SUCCESS_(message) GTEST_MESSAGE_(message, ::testing::TestPartResult::kSuccess)
#define __FLT128_DECIMAL_DIG__ 36
#define _ASSERT_H 1
#define __GCC_ATOMIC_LLONG_LOCK_FREE 2
#define __FLT32X_MIN__ 2.22507385850720138309023271733240406e-308F32x
#define SEGV_ACCERR SEGV_ACCERR
#define _G_IO_IO_FILE_VERSION 0x20001
#define __LDBL_DIG__ 18
#define ENOTUNIQ 76
#define GTEST_MUST_USE_RESULT_ __attribute__ ((warn_unused_result))
#define __exctype(name) extern int name (int) __THROW
#define _STL_MAP_H 1
#define RE_SYNTAX_EGREP (RE_CHAR_CLASSES | RE_CONTEXT_INDEP_ANCHORS | RE_CONTEXT_INDEP_OPS | RE_HAT_LISTS_NOT_NEWLINE | RE_NEWLINE_ALT | RE_NO_BK_PARENS | RE_NO_BK_VBAR)
#define si_upper _sifields._sigfault._bounds._addr_bnd._upper
#define PTRDIFF_MAX (9223372036854775807L)
#define _SC_NZERO _SC_NZERO
#define _GETOPT_CORE_H 1
#define __FLT_DECIMAL_DIG__ 9
#define __UINT_FAST16_MAX__ 0xffffffffffffffffUL
#define _CXXABI_INIT_EXCEPTION_H 1
#define S_IROTH (S_IRGRP >> 3)
#define GTEST_TYPE_PARAMS_(TestCaseName) gtest_type_params_ ##TestCaseName ##_
#define _GLIBCXX_PACKAGE_STRING "package-unused version-unused"
#define _SC_STREAMS _SC_STREAMS
#define __POSIX2_THIS_VERSION 200809L
#define __glibcxx_requires_nonempty() 
#define INTMAX_WIDTH 64
#define __glibcxx_requires_irreflexive_pred(_First,_Last,_Pred) 
#define ___int_ptrdiff_t_h 
#define _SC_BC_DIM_MAX _SC_BC_DIM_MAX
#define __GCC_ATOMIC_SHORT_LOCK_FREE 2
#define _IO_UNITBUF 020000
#define _GLIBCXX_USE_NANOSLEEP 1
#define FLT_MAX __FLT_MAX__
#define _SC_CHAR_BIT _SC_CHAR_BIT
#define GTEST_HAS_STD_WSTRING (!(GTEST_OS_LINUX_ANDROID || GTEST_OS_CYGWIN || GTEST_OS_SOLARIS))
#define __glibcxx_requires_partitioned_lower_pred(_First,_Last,_Value,_Pred) 
#define _GLIBCXX11_USE_C99_WCHAR 1
#define _SC_MONOTONIC_CLOCK _SC_MONOTONIC_CLOCK
#define _IO_peekc(_fp) _IO_peekc_unlocked (_fp)
#define __INT_LEAST64_WIDTH__ 64
#define _GLIBCXX_HAVE_ECHILD 1
#define INT_LEAST16_MAX (32767)
#define _IO_ftrylockfile(_fp) 
#define _GLIBCXX_HAVE_UNISTD_H 1
#define __glibc_likely(cond) __builtin_expect ((cond), 1)
#define __UINT_FAST8_TYPE__ unsigned char
#define _GNU_SOURCE 1
#define __S_ISGID 02000
#define _POSIX_SAVED_IDS 1
#define INT_FAST32_WIDTH __WORDSIZE
#define _BITS_CPU_SET_H 1
#define __N(msgid) (msgid)
#define __P(args) args
#define RE_CONTEXT_INVALID_DUP (RE_CARET_ANCHORS_HERE << 1)
#define _POSIX_BARRIERS 200809L
#define __cpp_init_captures 201304
#define _XOPEN_XCU_VERSION 4
#define __ATOMIC_ACQ_REL 4
#define S_IFIFO __S_IFIFO
#define __WCHAR_T 
#define __ATOMIC_RELEASE 3
#define _GLIBCXX_HAVE_EXECINFO_H 1
#define __fsblkcnt_t_defined 
#define _ALLOC_TRAITS_H 1
#define FPE_FLTUND FPE_FLTUND
#define _GLIBCXX_USE_INT128 1
#define GTEST_DEFINE_bool_(name,default_val,doc) GTEST_API_ bool GTEST_FLAG(name) = (default_val)
#define _STDLIB_H 1
