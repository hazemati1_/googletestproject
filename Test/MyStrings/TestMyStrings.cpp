#include <gtest/gtest.h>
#include <string.h>
#include "MyStrings.h"

/**************************Does String Contain Digits*************************/
TEST(TestDoesStringContainDigits, StringContainsDigits_ReturnTrue) {
	const char* string = "1234";
	EXPECT_TRUE(does_string_contain_digits(string));
}

TEST(TestDoesStringContainDigits, StringDoesNotContainDigits_ReturnFalse) {
	const char* string = "abcd";
	EXPECT_FALSE(does_string_contain_digits(string));
}

TEST(TestDoesStringContainDigits, StringContainsNull_ReturnFalse) {
	const char* string = NULL;
	EXPECT_FALSE(does_string_contain_digits(string));
}
/***************************************************************************/
