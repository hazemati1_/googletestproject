#include <string.h>
#include <stdlib.h>

bool does_string_contain_digits(const char* input) {
    if(input == NULL) {
        return false;
    } 
    for (size_t x = 0; input[x] != '\0'; x++) { 
        if(input[x] >= '0' && input[x] <= '9')
            return true ;
    }
    return false;
}
