#ifndef MY_STRINGS_H
#define MY_STRINGS_H

#include <stdbool.h>

/*
	check to see if string contains digits only
	@param: input
	@return: true -- if it contains digits
			 false -- if input does not contain digits
*/
bool does_string_contain_digits(const char* input);

#endif